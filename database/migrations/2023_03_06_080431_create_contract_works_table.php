<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_works', function (Blueprint $table) {
            $table->id();
             $table->tinyInteger('type');
            /*
             * Scope Of Work
             */
            $table->date('date_of_template');
            $table->date('date_of_installation');
            $table->string('kitchen_color');
            $table->string('kitchen_finish');
            $table->string('kitchen_size');

            $table->string('bathroom_color');
            $table->string('bathroom_finish');
            $table->string('bathroom_size');

            $table->string('sink_kitchen');
            $table->string('sink_kitchen_bathroom');

            $table->string('edge_kitchen');
            $table->string('edge_kitchen_bathroom');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_works');
    }
}
