<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToScopeOfWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_works', function (Blueprint $table) {
            $table->string('kitchen_installation',100)->nullable();
            $table->string('kitchen_brackets',100)->nullable();
            $table->string('kitchen_dishwasher_mount',100)->nullable();
            $table->string('bathroom_installation',100)->nullable();
            $table->string('bathroom_sink',100)->nullable();
            $table->string('nitch_window_seal',100)->nullable();

            $table->string('cabinet_color',100)->nullable();
            $table->string('cabinet_brand',100)->nullable();
            $table->string('cabinet_island_color',100)->nullable();
            $table->string('cabinet_installation',100)->nullable();
            $table->string('cabinet_knobs',100)->nullable();
            $table->string('cabinet_special_parts',100)->nullable();
            $table->string('cabinet_other',100)->nullable();

            $table->string('flooring_color',100)->nullable();
            $table->string('flooring_material',100)->nullable();
            $table->string('flooring_size',100)->nullable();
            $table->string('flooring_installation',100)->nullable();
            $table->string('flooring_trim_work',100)->nullable();
            $table->string('flooring_grout_color',100)->nullable();
            $table->string('flooring_shower_remodel',100)->nullable();
            $table->string('flooring_toilet_tub_remover',100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_works', function (Blueprint $table) {
            
        });
    }
}
