<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePropertiesTableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('alarm_code');
            $table->dropColumn('access_information');
            $table->dropColumn('gate_code');
            $table->dropColumn('garage_code');
            $table->dropColumn('contract_file');

            $table->string('address_one',255)->nullable();
            $table->string('address_two',255)->nullable();
            $table->string('city',255)->nullable();
            $table->string('state',255)->nullable();
            $table->unsignedBigInteger('zip'); 
            $table->tinyInteger('contact_info_status')->default(0);
            $table->string('contact_first_name',200)->nullable();
            $table->string('contact_last_name',100)->nullable();
            $table->string('contact_email',100)->nullable();
            $table->string('phone_one',20)->nullable();
            $table->string('phone_two',20)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
