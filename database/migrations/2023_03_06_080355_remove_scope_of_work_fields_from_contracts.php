<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveScopeOfWorkFieldsFromContracts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('type');
            /*
             * Scope Of Work
             */
            $table->dropColumn('date_of_template');
            $table->dropColumn('date_of_installation');
            $table->dropColumn('kitchen_color');
            $table->dropColumn('kitchen_finish');
            $table->dropColumn('kitchen_size');

            $table->dropColumn('bathroom_color');
            $table->dropColumn('bathroom_finish');
            $table->dropColumn('bathroom_size');

            $table->dropColumn('sink_kitchen');
            $table->dropColumn('sink_kitchen_bathroom');

            $table->dropColumn('edge_kitchen');
            $table->dropColumn('edge_kitchen_bathroom');


            /*
             * COUNTER TOPS SALES CONTRACT
             */
            $table->dropColumn('kitchen_backsplash');
            $table->dropColumn('bathroom_backsplash');

            $table->dropColumn('backsplash_install');

            $table->dropColumn('apron_cast_iron');

            $table->float('deposit_percent', 6, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('contracts', function (Blueprint $table) {
            
        });
    }

}
