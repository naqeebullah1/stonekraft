<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_orders', function (Blueprint $table) {
           $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('issue_id');
            $table->unsignedBigInteger('property_id');
            
            $table->string('type', 60)->nullable();
            $table->string('recurring_type', 100)->nullable();
            $table->string('escorted', 100)->nullable();
            $table->string('or_each_day', 30)->nullable();
            $table->string('week_day', 30)->nullable();
            $table->string('every_days', 30)->nullable();
            $table->string('date_month', 100)->nullable();
            $table->date('recurring_end_date')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('issue_id')->references('id')->on('issues');
            $table->foreign('property_id')->references('id')->on('properties');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_orders');
    }
}
