<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveDateFieldsFromContractWorksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('contract_works', function (Blueprint $table) {
            $table->dropColumn('date_of_template');
            $table->dropColumn('date_of_installation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('contract_works', function (Blueprint $table) {
            $table->date('date_of_template');
            $table->date('date_of_installation');
        });
    }

}
