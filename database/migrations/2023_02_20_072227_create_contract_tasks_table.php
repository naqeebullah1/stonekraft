<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractTasksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contract_tasks', function (Blueprint $table) {
            $table->id();
            $table->integer('contract_id');
            $table->integer('issue_id')->nullable();
            $table->integer('issue_list_id')->nullable();
            $table->string('task_name');
            $table->string('task_description')->nullable();
//            $table->tinyInteger('is_scheduled')->default(0);
            $table->date('task_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('contract_tasks');
    }

}
