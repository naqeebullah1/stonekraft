<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('task_name', 120);
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('task_type_id');
            $table->string('type', 60)->nullable();
            $table->string('recurring_type', 100)->nullable();
            $table->string('escorted', 100)->nullable();
            $table->string('or_each_day', 30)->nullable();
            $table->string('week_day', 30)->nullable();
            $table->string('every_days', 30)->nullable();
            $table->string('schedule', 30)->nullable();
            $table->text('description');
            $table->string('date_month', 100)->nullable();
            $table->date('recurring_end_date')->nullable();
            $table->date('scheduled_task')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->foreign('property_id')->references('id')->on('properties');
            $table->foreign('task_type_id')->references('id')->on('task_type');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
