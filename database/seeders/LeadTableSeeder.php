<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class LeadTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $permissions = [
            /*
             * Leads Setting
             */
                ['name' => 'lead-list', 'page' => 'leads', 'title' => 'List'],
                ['name' => 'lead-create', 'page' => 'leads', 'title' => 'Create'],
                ['name' => 'lead-edit', 'page' => 'leads', 'title' => 'Edit'],
                ['name' => 'lead-delete', 'page' => 'leads', 'title' => 'Delete'],
        ];


        foreach ($permissions as $permission) {
            Permission::create($permission);
        }
    }

}
