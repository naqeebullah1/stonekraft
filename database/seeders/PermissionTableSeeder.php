<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
  
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           ['name' => 'user-list','page'=>'Users','title'=>'List'],
           ['name' => 'user-create','page'=>'Users','title'=>'Create'],
           ['name' => 'user-edit','page'=>'Users','title'=>'Edit'],
           ['name' => 'user-delete','page'=>'Users','title'=>'Delete'],
           ['name' => 'role-list','page'=>'Roles','title'=>'List'],
           ['name' => 'role-create','page'=>'Roles','title'=>'Create'],
           ['name' => 'role-edit','page'=>'Roles','title'=>'Edit'],
           ['name' => 'role-delete','page'=>'Roles','title'=>'Delete'],
           ['name' => 'property-list','page'=>'properties','title'=>'List'],
           ['name' => 'property-create','page'=>'properties','title'=>'Create'],
           ['name' => 'property-edit','page'=>'properties','title'=>'Edit'],
           ['name' => 'property-delete','page'=>'properties','title'=>'Delete'],
           ['name' => 'client-list','page'=>'clients','title'=>'List'],
           ['name' => 'client-create','page'=>'clients','title'=>'Create'],
           ['name' => 'client-edit','page'=>'clients','title'=>'Edit'],
           ['name' => 'client-delete','page'=>'clients','title'=>'Delete'],
           ['name' => 'client-property-list','page'=>'client-properties','title'=>'List'],
           ['name' => 'client-property-create','page'=>'client-properties','title'=>'Create'],
           ['name' => 'client-property-edit','page'=>'client-properties','title'=>'Edit'],
           ['name' => 'client-property-delete','page'=>'client-properties','title'=>'Delete'],
           ['name' => 'client-login-list','page'=>'client-login','title'=>'List'],
           ['name' => 'client-login-create','page'=>'client-login','title'=>'Create'],
           ['name' => 'client-login-edit','page'=>'client-login','title'=>'Edit'],
           ['name' => 'client-login-delete','page'=>'client-login','title'=>'Delete'],
           ['name' => 'workOrder-list','page'=>'tasks','title'=>'List'],
           ['name' => 'workOrder-create','page'=>'tasks','title'=>'Create'],
           ['name' => 'workOrder-edit','page'=>'tasks','title'=>'Edit'],
           ['name' => 'workOrder-delete','page'=>'tasks','title'=>'Delete'],
           ['name' => 'task-type-list','page'=>'task-type','title'=>'List'],
           ['name' => 'task-type-create','page'=>'task-type','title'=>'Create'],
           ['name' => 'task-type-edit','page'=>'task-type','title'=>'Edit'],
           ['name' => 'task-type-delete','page'=>'task-type','title'=>'Delete'],
        ];
       
     
        foreach ($permissions as $permission) {
             Permission::create($permission);
        }
    }
}