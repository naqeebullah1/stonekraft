<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>{{ config('app.name', 'Content Management System') }} - @yield('title')</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="{{ asset('pages/ico/60.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('pages/ico/76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('pages/ico/120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('pages/ico/152.png') }}">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">

    <meta name="base-url" content="{{ asset('/') }}">

    <link href="{{ asset('plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css"
        media="screen" />
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css"
        media="screen" />
    <link href="{{ asset('plugins/mapplic/css/mapplic.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css"
        media="screen">
    <link href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet"
        type="text/css" media="screen">
    <link href="{{ asset('plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/datatables-responsive/css/datatables.responsive.css') }}" rel="stylesheet"
        type="text/css" media="screen" />
    <link href="{{ asset('pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{ asset('pages/css/themes/modern.css') }}" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/jquery-confirm.css') }}">
    <link rel="stylesheet" href="{{ asset('validate-password/css/jquery.passwordRequirements.css') }}" />
    <link href="{{ asset('plugins/bootstrap4-glyphicons/css/bootstrap-glyphicons.css') }}" rel="stylesheet" />
    {{-- inline Edit --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap3-editable/css/bootstrap-editable.css') }}">

    <link rel="stylesheet" href="{{ asset('intl-tel/build/css/intlTelInput.css') }}">
    <script src="{{ asset('js/jquery.js') }}"></script>
    <!--<script src="{{ asset('js/sorttable.js') }}"></script>-->
    <style>
        @media only screen and (max-width: 800px) {
            .draggable-icon {
                display: none !important;
            }

            /* Force table to not be like tables anymore */
            #no-more-tables table,
            #no-more-tables thead,
            #no-more-tables tbody,
            #no-more-tables tfoot,
            #no-more-tables th,
            #no-more-tables td,
            #no-more-tables tr {
                display: block;
            }

            /* Hide table headers (but not display: none;, for accessibility) */
            #no-more-tables thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            #no-more-tables tr {
                border: 1px solid #ccc;
            }

            #no-more-tables input select {
                border: 1px solid #ccc !important;
            }

            #no-more-tables td {
                /* Behave  like a "row" */
                border: none !important;
                border-bottom: 1px solid #eee !important;
                position: relative !important;
                padding-left: 50% !important;
                white-space: normal !important;
                text-align: left !important;
            }

            #no-more-tables td:before {
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
                text-align: left;
                font-weight: bold;
            }

            /*
                Label the data
                */
            #no-more-tables td:before {
                content: attr(data-title);
            }
        }

        .no-sorting img {
            display: none !important;
        }

        .suggestion-list {
            position: absolute;
            background-color: #fff;
            list-style-type: none;
            padding: 0;
            box-shadow: 0 4px 5px rgba(0, 0, 0, 0.15);
            width: 100%;
            z-index: 111;
            max-height: 200px;
            overflow: auto
        }

        .suggested-item,
        .suggested-job-item {
            padding: 5px 5px;
            cursor: pointer;

        }

        .display_box_hover,
        .suggested-item:hover,
        .suggested-job-item:hover {
            background-color: #e8e9ea !important;
        }

        td input.qty-field,
        td input.qty-ord {
            text-align: center;
        }

        td input.quoted-field {
            text-align: right;
        }

        .form-control::placeholder {
            font-size: 14px;
        }

        form .row [class*="col-"]:first-child {
            padding-left: 7px !important;
        }


        th span {
            float: right;
        }

        th {
            cursor: pointer;
        }

        th:hover {
            outline: 1px solid #ccc;
        }

        .card-header a:not(.btn) {
            opacity: 10 !important;
        }

        /* radio button */
        /* The container */
        .radio-container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default radio button */
        .radio-container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* Create a custom radio button */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-radius: 50%;
        }

        /* On mouse-over, add a grey background color */
        .radio-container:hover input~.checkmark {
            background-color: #ccc;
        }

        /* When the radio button is checked, add a blue background */
        .radio-container input:checked~.checkmark {
            background-color: #2196F3;
        }

        /* Create the indicator (the dot/circle - hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the indicator (dot/circle) when checked */
        .radio-container input:checked~.checkmark:after {
            display: block;
        }

        /* Style the indicator (dot/circle) */
        .radio-container .checkmark:after {
            top: 9px;
            left: 9px;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            background: white;
        }

        .datepicker.datepicker-dropdown.datepicker-orient-bottom:before {
            border-color: transparent !important;
        }

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            background-color: #f5f6f7 !important;
        }

        .nav-pills .nav-item {
            border: 2px solid black !important;
            margin-left: 5px !important;
            border-radius: 5px !important;
        }

        footer {
            position: fixed;
            height: 100px;
            bottom: 0;
            width: 100%;
        }

        .form-group-default.focused {
            background-color: #E7F6F2 !important;
        }
    </style>
</head>

<body class="horizontal-menu horizontal-app-menu">
    @include('pages.client_ticket.header')
    {{ View::make('footer') }}
    @yield('content')
    {{ View::make('footer') }}
    <!-- BEGIN VENDOR JS -->
    <script src="{{ asset('plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/modernizr.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/popper/umd/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-actual/jquery.actual.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/classie/classie.js') }}"></script>
    <script src="{{ asset('plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp" type="text/javascript"></script>
    <script src="{{ asset('plugins/mapplic/js/hammer.min.js') }}"></script>
    <script src="{{ asset('plugins/mapplic/js/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('plugins/mapplic/js/mapplic.js') }}"></script>
    <script src="{{ asset('plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/skycons/skycons.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}" type="text/javascript">
    </script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-datatable/media/js/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"
        type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('plugins/datatables-responsive/js/datatables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/datatables-responsive/js/lodash.min.js') }}"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="{{ asset('pages/js/pages.min.js') }}"></script>
    <script src="{{ asset('js/jquery.toaster.js') }}"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="{{ asset('js/scripts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-confirm.js') }}"></script>
    <script src="{{ asset('plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <!--<script src="{{ asset('js/form_elements.js') }}" type="text/javascript"></script>-->
    <!-- END PAGE LEVEL JS -->
    <script src="{{ asset('validate-password/js/jquery.passwordRequirements.js') }}"></script>
    <script src="{{ asset('intl-tel/build/js/intlTelInput.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

    <script src="{{ asset('plugins/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>



    @yield('script')

</body>
</html>
