@php
$task_label1 = 'Work Order';
$task_label2 = 'Schedule';
$task_label3 = 'Item';
$task_label4 = 'Occurrence';
$task_label5 = 'Property';
$serial = 0;
@endphp
<div class="container grid-wrapper p-3">
    <div class="grid-header">
        <div class="row d-none d-md-flex mb-md-2 p-md-1">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label1 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label2 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label3 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label4 }}</h4>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label5 }}</h4>
            </div>
        </div>
    </div>
    <div class="grid-body">
        @foreach ($tickets as $ticket)
            <div class="row mb-md-0 p-md-1 mb-3 p-1">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label1 }}</h4>
                    <p class="m-0 p-1">{{ $ticket['work_order'] }}</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label2 }}</h4>
                    @if ($ticket['schedule'] == 'COMPLETED')
                        <p class="m-0 p-1">Completed</p>
                    @endif
                    @if ($ticket['schedule'] == 'NOT_SCHEDULED')
                        <p class="m-0 p-1">Not Scheduled</p>
                    @endif
                    @if ($ticket['schedule'] == 'PENDING')
                        <p class="m-0 p-1">Pending</p>
                    @endif
                    @if ($ticket['schedule'] == 'REJECTED')
                        <p class="m-0 p-1 text-danger">Rejected</p>
                    @endif
                    @if ($ticket['schedule'] == 'SCHEDULED')
                    <?php
                    $date = explode('-', $ticket['date_month']);
                    $date = $date[1].'-'.$date[0];
                ?>
                <p class="m-0 p-1">
                    {{ $date }}
                    </p>
                    @endif
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label3 }}</h4>
                    <p class="m-0 p-1">{{ $ticket['issue'] }}</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label4 }}</h4>
                    @if ($ticket['type'] == 'one_time')
                        <p class="m-0 p-1">One Time</p>
                    @endif
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label5 }}</h4>
                    <p class="m-0 p-1">{{ $ticket['property'] }}</p>
                </div>
            </div>
        @endforeach
    </div> <!-- End of ROW -->
</div>
