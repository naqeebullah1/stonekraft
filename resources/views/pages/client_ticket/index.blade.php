<div class="container">
    <div class="container">
        @if (Session::has('message'))
            <div class="row">
                <div class="container text-center">
                    <div class="alert alert-success message">
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>
        @endif
        @if (Session::has('success'))
            <div class="row">
                <div class="container text-center">
                    <div class="alert alert-success message">
                        {{ Session::get('success') }}
                    </div>
                </div>
            </div>
        @endif
        <div class="row justify-content-center pt-2">
            <div class="col-md-8">
                <div class="card">
                    <h4 class="semi-bold text-center light-heading">{{ __('Create Client Work Order') }}</h4>

                    <div class="card-body">
                        <form method="POST" id="client-property-form" action="{{ route('store.ticket') }}">
                            @include('pages.client_ticket.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h1 class="float-left">List Client Work Order</h1>
    <div class="clearfix"></div>
    @include('pages.client_ticket.list')
    <script>
        $(document).ready(function() {

            setTimeout(() => {
                $('.message').fadeOut('slow');
            }, 3000);

            $('#description').keyup(function() {
                var characterCount = $(this).val().length;
                current = $('#current');
                maximum = $('#maximum');
                theCount = $('#the-count');

                current.text(characterCount);
                if (characterCount < 70) {
                    current.css('color', '#666');
                }
                if (characterCount > 70 && characterCount < 90) {
                    current.css('color', '#6d5555');
                }
                if (characterCount > 90 && characterCount < 100) {
                    current.css('color', '#793535');
                }
                if (characterCount > 100 && characterCount < 120) {
                    current.css('color', '#841c1c');
                }
                if (characterCount > 120 && characterCount < 139) {
                    current.css('color', '#8f0001');
                }

                if (characterCount >= 140) {
                    maximum.css('color', '#8f0001');
                    current.css('color', '#8f0001');
                    theCount.css('font-weight', 'bold');
                } else {
                    maximum.css('color', '#666');
                    theCount.css('font-weight', 'normal');
                }
            });

        });

        $(document).ready(function(){
            $('#property_id').change(function() {
                var propertyId = $(this).val();

                if (propertyId != '') {

                    $.ajax({
                        url: "{{ route('client.generate.work.order') }}",
                        type: "GET",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "property_id": propertyId,
                        },
                        cache: false,
                        async: false,
                        success: function(data) {
                            
                            var result = JSON.parse(data);
                            if (result == 'Not-Assigned-client') {
                                $('#work_order').val('Please! Assign property to a client.');
                                $('#save-form').attr('disabled', true);
                            } else {
                                $('#save-form').attr('disabled', false);
                                $('#work_order').val(result);
                            }
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                }
            });
        });
    </script>
</div>
