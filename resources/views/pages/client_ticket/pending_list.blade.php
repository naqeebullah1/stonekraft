@extends('pages.client_ticket.layout')
@section('title', 'Dashboard')
@section('content')
    <div class="container">
        <h1 class="float-left">List Client Ticket</h1>
    </div>
    @php
    $task_label1 = 'ID';
    $task_label2 = 'Item';
    $task_label3 = 'Task Name';
    $task_label4 = 'Property';
    $task_label5 = 'Description';
    $task_label6 = 'Created Date';
    $serial = 0;
    @endphp
    <div class="container grid-wrapper p-3">
        <div class="grid-header">
            <div class="row d-none d-md-flex mb-md-2 p-md-1">
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                    <h4 class="small bold m-0">{{ $task_label1 }}</h4>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="small bold m-0">{{ $task_label2 }}</h4>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="small bold m-0">{{ $task_label3 }}</h4>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="small bold m-0">{{ $task_label4 }}</h4>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <h4 class="small bold m-0">{{ $task_label5 }}</h4>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="small bold m-0">{{ $task_label6 }}</h4>
                </div>
            </div>
        </div>
        <div class="grid-body">
            @foreach ($pendingTickets as $pendingTicket)
                <div class="row mb-md-0 p-md-1 mb-3 p-1">
                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label1 }}</h4>
                        <p class="m-0 p-1">{{ ++$serial }}</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label2 }}</h4>
                        <p class="m-0 p-1">{{ $pendingTicket->taskType->task_type }}</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label3 }}</h4>
                        <p class="m-0 p-1">{{ $pendingTicket->task_name }}</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label4 }}</h4>
                        <p class="m-0 p-1">{{ $pendingTicket->property->property_name }}</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label5 }}</h4>
                        <p class="m-0 p-1">{{ $pendingTicket->description }}</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label6 }}</h4>
                        <p class="m-0 p-1">{{ date('m-d-Y', strtotime($pendingTicket->created_at)) }}</p>
                        
                    </div>
                   
                </div>
            @endforeach
        </div> <!-- End of ROW -->
    </div>
@endsection
