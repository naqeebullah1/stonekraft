<div class="header p-r-0 bg-primary">
    <div class="header-inner header-md-height">
        <a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-menu text-white" data-toggle="horizontal-menu"></a>
        <div class="">
            <div class="brand inline no-border d-sm-inline-block">
                <h3 class="text-white">Stonecraft</h3>
                <!--<img style="width: 175px;" src="{{ asset('img/logo.png') }}" alt="{{ config('app.name') }}"-->
                <!--    data-src="{{ asset('img/logo.png') }}" data-src-retina="{{ asset('img/logo.png') }}"-->
                <!--    class="img-fluid">-->
            </div>
        </div>
        <div class="d-flex align-items-center">
            <!-- START User Info-->

            <div class="dropdown pull-right">
                <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="pull-left p-r-10 fs-14 font-heading d-lg-inline-block text-white">
                        <span class="semi-bold">Logged in as {{ Session::get('client_name') }}</span>
                    </div>
                </button>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                    <a href="{{ url('/') }}/change-password" class="dropdown-item"><i
                            class="pg-settings_small"></i> Change Password</a>
                    <a href="javascript:"
                        onClick="event.preventDefault();document.getElementById('logout-form').submit()"
                        class="clearfix bg-master-lighter dropdown-item">
                        <form action="{{ route('client.logout') }}" method="POST" style="display:none;"
                            id="logout-form">
                            @csrf
                            @method('POST')
                        </form>
                        <span class="pull-left pr-2"><i class="pg-power"></i></span>
                        <span class="pull-left">Logout</span>
                    </a>
                </div>
            </div>
            <!-- END User Info-->
        </div>
    </div>
</div>
<div class="bg-white">
    <div class="container">
        <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="">
           
        </div>
    </div>
</div>
