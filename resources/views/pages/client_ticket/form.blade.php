@csrf
<style>
    #the-count {
        float: right;
        padding: 0.1rem 0 0 0;
        font-size: 0.875rem;
    }

    .form-group-default textarea.form-control {
        height: 80px !important;
    }

    .work_order {
        background-color: #CCCCCC !important;
    }
</style>
<div class="row clearfix">
    <div class="col-md-12">
        <input type="hidden" name="client_id" value="{{ Session::get('client_id') }}">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="" class="col-md-12 col-form-label text-md-left">{{ __('Property') }}</label>
            <select id="property_id" class="form-control @error('property_id') is-invalid @enderror" name="property_id"
                value="{{ old('property_id') }}" required autocomplete="property_id" autofocus>
                <option value="">Select Property</option>
                @foreach ($clientProperty as $property)
                    <option value="{{ $property->property_id }}">{{ $property->property->property_name }}</option>
                @endforeach
            </select>
            @error('property_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required work_order" aria-required="true">
            <label for="work_order" class="col-md-12 col-form-label text-md-left">{{ __('Work Order') }}</label>
            <input id="work_order" class="form-control @error('work_order') is-invalid @enderror work_order"
                name="work_order" autocomplete="work_order" value="{{ old('work_order') }}" required
                autocomplete="work_order" readonly style="color: black;" />
            @error('work_order')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="" class="col-md-12 col-form-label text-md-left">{{ __('Item') }}</label>
            <select id="issue_id" class="form-control @error('issue_id') is-invalid @enderror" name="issue_id"
                value="{{ old('issue_id') }}" required autocomplete="issue_id" autofocus>
                <option value="">Select Issue</option>
                @foreach ($issue as $tt)
                    <option value="{{ $tt->id }}">{{ $tt->issue }}</option>
                @endforeach
            </select>
            @error('issue_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="description" class="col-md-12 col-form-label text-md-left">{{ __('Notes') }}</label>
            <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" required
                autocomplete="description" autofocus maxlength="250" rows="10" style="resize: none;">{{ old('description') }}</textarea>
            <div id="the-count">
                <span id="current">0</span>
                <span id="maximum">/ 250</span>
            </div>
            @error('description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<button id="save-form" type="submit" class="btn btn-primary float-right">
    {{ __('Create') }}
</button>

