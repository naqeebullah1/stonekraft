@extends('master')
@section('title', 'Client Lead')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h4 class="semi-bold text-center light-heading">{{ __('Create Lead') }}</h4>

                    <div class="card-body">
                        <form method="POST" id="client-form" action="{{ route('admin.leads.store') }}">
                            @include('pages.lead.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document.body).on('click', '#save-form', function() {
            $('#client-form').validate({
                rules: {
                    first_name: {
                        lettersonly: true,
                        required: true,
                        maxlength: 45,
                        minlength: 2

                    },
                    last_name: {
                        lettersonly: true,
                        required: true,
                        maxlength: 45,
                        minlength: 2

                    },
                    email: {
                        required: true,
                        email: true
                    },
                    email_confirmation: {
                        required: true,
                        equalTo: "#email"
                    },
                    phone_one: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    phone_two: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    address_one: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    address_two: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    city: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 20,
                        minlength: 2
                    },
                    state: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 20,
                        minlength: 2
                    },
                    zip: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 5,
                        minlength: 2
                    },
                },
                message: {
                    first_name: {
                        alphanumeric: "please enter characters only",
                        required: "Enter your first name, please.",
                        maxlength: "first Name too long.",
                        minlength: "Min 2 Char"
                    },
                    last_name: {
                        alphanumeric: "please enter characters only",
                        required: "Enter your first name, please.",
                        maxlength: "first Name too long.",
                        minlength: "Min 2 Char"
                    },
                },
                submitHandler: function(form) {
                    $("#submitForm").html("Please wait...").attr('disabled', true);
                    form.submit();
                }

            });

        });
    </script>
@endsection
