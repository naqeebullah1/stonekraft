@extends('master')
@section('title','Client')
@section('content')
<div class="container-fluid">
        @if (Session::has('message'))
            <div class="row">
                <div class="container text-center">
                    <div class="alert alert-success message">
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>
        @endif
    <h1 class="float-left">Leads List</h1>
    @include('pages.lead.client_search_form')   
    @can('client-create')
    <a href="{{ route('admin.leads.create') }}" class="btn btn-primary float-right" role="button">Create Lead</a>
    @endcan 
    <div class="clearfix"></div>
</div>
@include('pages.lead.list')    

@include('partials.loadmorejs')
<script>
    $(document).ready(function() {
        setTimeout(() => {
            $('.message').fadeOut('slow');
        }, 3000);
    });
</script>
@endsection
