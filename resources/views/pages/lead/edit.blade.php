@extends('master')
@section('title', 'Lead Edit')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h4 class="semi-bold text-center light-heading">{{ __('Edit Lead') }}</h4>

                    <div class="card-body">
                        <form id="client-form" method="POST" action="{{ route('admin.leads.update', $client->id) }}">
                            @method('PATCH')
                            @include('pages.lead.form')
                        </form>
                        @can('client-delete')
                            <button class="btn btn-danger float-left"
                                onClick="event.preventDefault();deleteConfirm('client-delete-form-{{ $client->id }}', 'You are about to delete a client, Are you sure?')">
                                Delete
                            </button>
                            <form class="client-form" id="client-delete-form-{{ $client->id }}" style="display:none;"
                                action="{{ route('admin.clients.destroy', $client->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                            </form>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document.body).on('click', '#save-form', function() {
            $('#client-form').validate({
                rules:{
                    first_name: {
//                        lettersonly: true,
                        required: true,
                        maxlength: 45,
                        minlength: 2

                    },
                    last_name: {
//                        lettersonly: true,
                        required: true,
                        maxlength: 45,
                        minlength: 2

                    },
                    email: {
                        required: true,
                        email: true
                    },
                    email_confirmation: {
                        required: true,
                        equalTo: "#email"
                    },
                    phone_one: {
//                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                     phone_two: {
//                        alphanumeric: true,
                        required: false,
                        maxlength: 120,
                        minlength: 5
                    },
                    address_one: {
//                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    address_two: {
//                        alphanumeric: true,
                        required: false,
                        maxlength: 120,
                        minlength: 5
                    },
                    city: {
//                        alphanumeric: true,
                        required: true,
                        maxlength: 20,
                        minlength: 2
                    },
                    state: {
//                        alphanumeric: true,
                        required: true,
                        maxlength: 20,
                        minlength: 2
                    },
                    zip: {
//                        alphanumeric: true,
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    },
                    
                },
/*                rules: {
                    first_name: {
                        lettersonly: true,
                        required: true,
                        maxlength: 45,
                        minlength: 2

                    },
                    last_name: {
                        lettersonly: true,
                        required: true,
                        maxlength: 45,
                        minlength: 2

                    },
                    email: {
                        required: true,
                        email: true
                    },
                    email_confirmation: {
                        required: true,
                        equalTo: "#email"
                    },
                    phone_one: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    phone_two: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    address_one: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    address_two: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    city: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 20,
                        minlength: 2
                    },
                    state: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 20,
                        minlength: 2
                    },
                    zip: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    },
                },*/
                message: {
                    first_name: {
                        alphanumeric: "please enter characters only",
                        required: "Enter your first name, please.",
                        maxlength: "first Name too long.",
                        minlength: "Min 2 Char"
                    },
                    last_name: {
                        alphanumeric: "please enter characters only",
                        required: "Enter your first name, please.",
                        maxlength: "first Name too long.",
                        minlength: "Min 2 Char"
                    },
                },
                submitHandler: function(form) {
                    $("#submitForm").html("Please wait...").attr('disabled', true);
                    form.submit();
                }

            });

        });
    </script>
@endsection
