<div class="row clearfix">
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label for="address_one" class="col-md-12 col-form-label text-md-left">{{ __('Address One') }}<span
                            class="text-danger">*</span></label>
                    <input id="address_one" type="text"
                           class="form-control @error('address_one') is-invalid @enderror" name="address_one"
                           value="{{ old('address_one') ?? $client->address_one }}" autofocus>
                    @error('address_one')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label for="address_two"
                           class="col-md-12 col-form-label text-md-left">{{ __('Address Two') }}</label>
                    <input id="address_two" type="text"
                           class="form-control @error('address_two') is-invalid @enderror" name="address_two"
                           value="{{ old('address_two') ?? $client->address_two }}" autofocus>
                    @error('address_two')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-4">
                <div class="form-group form-group-default">
                    <label for="city"
                           class="col-md-12 col-form-label text-md-left">{{ __('City ') }}<span
                            class="text-danger">*</span></label>
                    <input id="city" type="text" class="form-control @error('city') is-invalid @enderror"
                           name="city" value="{{ old('city') ?? $client->city }}" autofocus>
                    @error('city_one')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-group-default">
                    <label for="state"
                           class="col-md-12 col-form-label text-md-left">{{ __('State/Province') }}<span
                            class="text-danger">*</span></label>
                    @include('pages.client.state_partial')
           <!--<input id="state" type="text" class="form-control @error('state') is-invalid @enderror"-->
                    <!--    name="state" value="{{ old('state') ?? $client->state }}" autofocus>-->
                    <!--@error('state')-->
                    <!--    <span class="invalid-feedback" role="alert">-->
                    <!--        <strong>{{ $message }}</strong>-->
                    <!--    </span>-->
                    <!--@enderror-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-group-default">
                    <label for="zip" class="col-md-12 col-form-label text-md-left">{{ __('Zip/ Postal Code') }}<span
                            class="text-danger">*</span></label>
                    <input id="zip" type="number" min="0" class="form-control zip @error('zip') is-invalid @enderror"
                           name="zip" value="{{ old('zip') ?? $client->zip }}" autofocus maxlength="5">
                    @error('zip')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row clearfix">

            <div class="col-md-4">
                <label class="ml-1"><b>Contact Information</b> &nbsp;&nbsp;
                </label>
            </div>
            <div class="col-md-6">
                <input type="checkbox" name="contact_info_status" id="contact_info_status" value="0"> Same as client information
            </div>
        </div> 
        <div id="property_contact_block">       
        <div class="row clearfix">
            <div class="col-md-6">
                <div class="form-group form-group-default required" aria-required="true">
                    <label for="contact_first_name" class="col-md-12 col-form-label text-md-left">{{ __('First Name') }}</label>
                    <input id="contact_first_name" type="text" class="form-control @error('contact_first_name') is-invalid @enderror"
                           name="contact_first_name" value="{{ old('contact_first_name') ?? $client->first_name }}" required
                           autocomplete="contact_first_name" autofocus>
                    @error('contact_first_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default required">
                    <label for="contact_last_name" class="col-md-12 col-form-label text-md-left">{{ __('Last Name') }}</label>
                    <input id="contact_last_name" type="text" class="form-control @error('contact_last_name') is-invalid @enderror"
                           name="contact_last_name" value="{{ old('contact_last_name') ?? $client->last_name }}" required autocomplete="contact_last_name"
                           autofocus>
                    @error('contact_last_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

            </div>
        </div>

        <div class="row clearfix">
            <div class="@if ($client->id) col-md
                 @else
                 col-md-6 @endif">
                <div class="form-group form-group-default required">
                    <label for="contact_email" class="col-md-12 col-form-label text-md-left">{{ __('Email') }}</label>
                    <input id="contact_email" type="text" class="form-control @error('contact_email') is-invalid @enderror"
                           name="contact_email" value="{{ old('contact_email') ?? $client->email }}" required autocomplete="email" autofocus>
                    @error('contact_email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md">
                <div class="form-group form-group-default required">
                    <label for="contact_email_confirmation"
                           class="col-md-12 col-form-label text-md-left">{{ __('Confirm Email') }}</label>
                    <input id="contact_email_confirmation" type="text"
                           class="form-control @error('contact_email_confirmation') is-invalid @enderror" name="contact_email_confirmation"
                           required autocomplete="contact_email_confirmation" autofocus value="{{ old('contact_email') ?? $client->email }}">
                    @error('contact_email_confirmation')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label for="contact_phone_one" class="col-md-12 col-form-label text-md-left">{{ __('Phone Number') }}<span
                            class="text-danger">*</span></label>
                    <input id="contact_phone_one" type="text" class="phonemask form-control @error('contact_phone_one') is-invalid @enderror"
                           name="contact_phone_one" required value="{{ old('contact_phone_one') ?? $client->phone_one }}" autofocus>
                    @error('contact_phone_one')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label for="contact_phone_two" class="col-md-12 col-form-label text-md-left">{{ __('Alt. Phone Number') }}</label>
                    <input id="contact_phone_two" type="text" class="phonemask form-control @error('contact_phone_two') is-invalid @enderror"
                           name="contact_phone_two" value="{{ old('contact_phone_two') ?? $client->phone_two }}" autofocus>
                    @error('contact_phone_two')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>        
        </div>

<script type="text/javascript">
    $('#contact_info_status').change(function(){
        if ( $(this).is(":checked") ) {
            $('#property_contact_block').css('display','none');
            $(this).val(1);
        }else{
            $('#property_contact_block').css('display','');
            $(this).val(0);
        }
    });
</script>