@extends('master')
@section('title', 'Issue Edit')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <h4 class="semi-bold text-center light-heading">{{ __('Edit Item') }}</h4>
                    <div class="card-body">
                        <form id="issue-form" method="POST" action="{{ route('admin.items.update', $issue->id) }}">
                            @method('PATCH')
                            @include('pages.issue.form')
                        </form>
                        @can('task-type-delete')
                            @if ($issue->id)
                                <button class="btn btn-danger float-left"
                                    onClick="event.preventDefault();deleteConfirm('issue-delete-form-{{ $issue->id }}', 'You are about to delete a issue, Are you sure?')">
Delete                                
                                </button>
                                <form class="task_type-form" id="issue-delete-form-{{ $issue->id }}" style="display:none;"
                                    action="{{ route('admin.items.destroy', $issue->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endif
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document.body).on('click', '#save-form', function() {
            $('#issue-form').validate({
                rules: {
                    issue: {
                        lettersonly: true,
                        required: true,
                        maxlength: 45,
                        minlength: 2

                    },
                },
                message: {},
                submitHandler: function(form) {
                    $("#submitForm").html("Please wait...").attr('disabled', true);
                    form.submit();
                }

            });

        });
    </script>
@endsection
