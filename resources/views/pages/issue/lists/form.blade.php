@csrf
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="title" class="col-md-12 col-form-label text-md-left">{{ __('Title') }}</label>
            <input id="title" class="form-control @error('title') is-invalid @enderror" name="title"
                   value="{{ old('title') ?? $issue->title }}" required autocomplete="title" style="text-transform:uppercase">
            @error('title')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="description" class="col-md-12 col-form-label text-md-left">{{ __('Description') }}</label>
            <input id="description" class="form-control @error('description') is-invalid @enderror" name="description"
                   value="{{ old('description') ?? $issue->description }}" 
                   required autocomplete="description">
            @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required">
            <label for="is_active" class="col-md-12 col-form-label text-md-left">{{ __('Status') }}</label>
            <select name="is_active" class="full-width" data-init-plugin="select2">
                <option value="">Select Status</option>
                <option value="1" @if ($issue->is_active == 1) {{ 'selected' }} @endif>Active</option>
                <option value="0" @if (isset($issue->is_active) && $issue->is_active == 0) {{ 'selected' }} @endif>Inactive</option>
            </select>
            @error('is_active')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<button id="save-form" type="submit" class="btn btn-primary float-right">
    @isset($issue->id)
    {{ __('Update') }}
    @else
    {{ __('Create') }}
    @endisset
</button>
