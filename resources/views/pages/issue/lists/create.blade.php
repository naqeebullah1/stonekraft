@extends('master')
@section('title', 'Create Item')
@section('content')
    <div class="container">
        @if (Session::has('message'))
            <div class="row">
                <div class="container text-center">
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h4 class="semi-bold text-center light-heading">{{ __('Create Item List') }}</h4>

                    <div class="card-body">
                        <form method="POST" id="issue-form" action="{{ route('admin.lists.store',request()->issue_id) }}">
                            @include('pages.issue.lists.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document.body).on('click', '#save-form', function() {
            $('#issue-form').validate({
                rules: {
                    issue: {
                        required: true,
                    },
                    is_active: {
                        required: true,
                    },

                },
                message: {

                },
                submitHandler: function(form) {
                    $("#submitForm").html("Please wait...").attr('disabled', true);
                    form.submit();
                }

            });

        });
    </script>
@endsection
