@extends('master')
@section('title', 'Issue')
@section('content')
    <div>
        @if (Session::has('message'))
            <div class="row">
                <div class="container text-center">
                    <div class="alert alert-success message">
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>
        @endif
        <h1 class="float-left">Item Types List</h1>
        <div class="pull-left container">
            
            <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title=""
            class="rounded-circle bg-primary ml-2 mr-2" data-original-title="Active"></i>
            <b>Active </b>
            
            <i style="padding: 1px 9px;" class="rounded-circle bg-danger ml-2 mr-2" data-toggle="tooltip" data-placement="top"
            title="" data-original-title="Inactive"></i>
            <b>Inactive </b>
            @can('client-create')
                <a href="{{ route('admin.items.create') }}" class="btn btn-primary float-right" role="button">Create Item</a>
            @endcan
        </div>
        <div class="clearfix"></div>
    </div>
    @include('pages.issue.list')

    @include('partials.loadmorejs')
    
   <script>
    $(document).ready(function() {
        setTimeout(() => {
            $('.message').fadeOut('slow');
        }, 3000);
    });
</script>
@endsection
