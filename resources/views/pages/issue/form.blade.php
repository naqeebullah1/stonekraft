@csrf
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="issue" class="col-md-12 col-form-label text-md-left">{{ __('Item') }}</label>
            <input id="issue" class="form-control @error('issue') is-invalid @enderror" name="issue"
                value="{{ old('issue') ?? $issue->issue }}" required autocomplete="issue" style="text-transform:uppercase">
            @error('issue')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required">
            <label for="is_active" class="col-md-12 col-form-label text-md-left">{{ __('Status') }}</label>
            <select name="is_active" class="full-width" data-init-plugin="select2">
                <option value="">Select Status</option>
                <option value="1" @if ($issue->is_active == 1) {{ 'selected' }} @endif>Active</option>
                <option value="0" @if ($issue->is_active == 0) {{ 'selected' }} @endif>Inactive</option>
            </select>
            @error('is_active')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<button id="save-form" type="submit" class="btn btn-primary float-right">
    @isset($issue->id)
        {{ __('Update') }}
    @else
        {{ __('Create') }}
    @endisset
</button>
