@php
$issue_label1 = 'SR. No';
$issue_label2 = 'Item';
$issue_label3 = 'Status';
$issue_label4 = 'Action';
$serial = 0;
@endphp
<div class="container grid-wrapper p-3 mt-2">
    <div class="grid-header">
        <div class="row d-none d-md-flex mb-md-2 p-md-1">
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $issue_label1 }}</h4>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $issue_label2 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $issue_label3 }}</h4>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $issue_label4 }}</h4>
            </div>
        </div>
    </div>
    <div class="grid-body">
        @foreach ($issues as $issue)
            @php
                $active = '<i style="" class="rounded-circle bg-primary" data-toggle="tooltip" data-placement="top" title=""></i>';
                $inactive = '<i style="" class="rounded-circle bg-danger" data-toggle="tooltip" data-placement="top" title=""></i>';
            @endphp
            <div class="row mb-md-0 p-md-1 mb-3 p-1">
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $issue_label1 }}</h4>
                    <p class="m-0 p-1">{{ ++$serial }}</p>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $issue_label2 }}</h4>
                    <p class="m-0 p-1 text-uppercase">{{ $issue->issue }}</p>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $issue_label3 }}</h4>
                    <p class="m-0 p-1">
                        @if ($issue->is_active == 1)
                            @php echo $active @endphp
                        @else
                            @php echo $inactive @endphp
                        @endif
                    </p>
                </div>
                <div class="col-3 col-sm-3 col-md-2">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $issue_label4 }}</h4>
                    <br class="d-none d-md-none d-sm-block " />
                    @can('task-type-edit')
                        <a href="{{ route('admin.lists.index', $issue->id) }}"
                            class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button">
                            Item Lists
                        </a>
                        <a href="{{ route('admin.items.edit', $issue->id) }}"
                            class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button">
                            Edit
                        </a>
                    @endcan
                    
                </div>
            </div> <!-- End of ROW -->
        @endforeach
    </div>
</div>
{{ $issues->links() }}

