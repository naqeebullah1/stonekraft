<div class="clearfix"></div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <h4 class="bold" style="padding:0px 10px;font-size:18px;"><a class="toggler" href="#"
                    onClick="toggleSearch('searchfields')">{{ __('Advanced Search') }}<span class="pull-right"><i
                            class="fa fa-chevron-down"></i></span></a></h4>
            
            <div class="card-body" id="searchfields">
                <form method="get" id="searchform" accept-charset="utf-8" action="{{ route('admin.task.index',$type) }}">
                    <!--csrf-->
                    <div class="row">
                        <input type="hidden" name="filtered" value="1">
                        <input type="hidden" name="sort_field" class="sort_field" value="">
                        <input type="hidden" name="sort_order" class="sort_order" value="">
                        <div class="col-lg col-sm-12 col-xs-12">
                            <label for="client_id" class="font-weight-bold">Client</label>
                            <select name="client_id" id="client_id" class="form-control client">
                                <option value="">Select Client</option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}" 
                                            @if(request()->client_id==$client->id)
                                            selected
                                            @endif
                                            >{{ $client->first_name }},{{ $client->last_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg col-sm-12 col-xs-12">
                            <label for="" class="font-weight-bold">From Date</label>
                            
                            <?php
//                            dump($fromDate);
                            ?>
                            <input type="text" placeholder="mm-dd-yyyy" value="<?= formatDate($fromDate) ?>" name="from_date" id="from_date" class="form-control datepicker"
                                autocomplete="off" />
                        </div>
                        <div class="col-lg col-sm-12 col-xs-12">
                            <label for="" class="font-weight-bold">To Date</label>
                            <input type="text" placeholder="mm-dd-yyyy" value="<?=formatDate($toDate) ?>"  name="to_date" id="to_date" class="form-control datepicker" autocomplete="off" />
                        </div>
                        
                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <span class="font-weight-bold contact_name_label" style="display: none">Client Name:</span>
                            <span class="contact_name"></span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <span class="font-weight-bold phone_label" style="display: none">Phone:</span>
                            <span class="phone"></span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <span class="font-weight-bold email_label" style="display: none">Email:</span>
                            <span class="email"></span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <span class="font-weight-bold address_label" style="display: none">Address:</span>
                            <span class="address"></span>
                        </div>
                        <div class="col-md-12 text-right">
                            <a class="btn btn-primary" style="margin-top:25px;" href="{{ url('backend/task-list',$type) }}">Reset</a>
                            <input type="submit" name="search" class="btn btn-primary" style="margin-top:25px;"
                                value="Search" id="searchBnt">
                            <br>
                        </div>
                    </div>
                    
                </form>
            </div> 
        </div>
    </div>
</div>
