<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"
      rel="stylesheet">
@csrf
<div class="row clearfix">
    <input type="hidden" name="type" value="{{ $task->type }}">
    <div class="col-md-12">
        <div class="form-group form-group-default required disabled" aria-required="true">
            <label for="work_order" class="col-md-12 col-form-label text-md-left">{{ __('Work Order') }}</label>
            <input id="work_order" type="text" class="form-control @error('work_order') is-invalid @enderror"
                   name="work_order" value="{{ old('work_order') ?? $task->work_order }}" required
                   autocomplete="work_order" autofocus readonly />
            @error('work_order')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group form-group-default required disabled" aria-required="true">
            <label for="property_id" class="col-md-12  col-form-label text-md-left">{{ __('Property') }}</label>
            <select id="property_id" class="form-control @error('property_id') is-invalid @enderror" name="property_id"
                    value="{{ old('property_id') }}" required autocomplete="property_id" autofocus readonly>
                <option value="">Select Property</option>
                @foreach ($properties as $property)
                <option value="{{ $property->id }}" @if ($task->property_id == $property->id) {{ 'selected' }} @endif>
                        {{ $property->property_name }}</option>
                @endforeach
            </select>
            @error('property_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

</div>

<div class="row light-heading clearfix mb-2">
    <table class="table table-bordered table-disabled">
        <thead>
            <tr>
                <th>Recurring/ One Time</th>
                <th>Repeat</th>
                <th>End Date</th>
                <th>Schedule Month</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ ucwords(str_replace('_',' ',$task->type)) }}</td>
                <td>
                    @if ($task->type != 'one_time')
                    <label for="">{{ ucfirst(strtolower($task->recurring_type)) }}
                   @if($task->recurring_type=="DAILY")
                        ({{ $task->every_days }})
                   @endif
                    </label>
                    @else
                    <label for="">None</label>
                    @endif
                </td>
                <td>
                    @if ($task->type != 'one_time')
                    @php
                    if ($task->recurring_end_date != '') {
                    $recurringDate = explode('-', $task->recurring_end_date);
                    $recurringDate = $recurringDate['1'] . '-' . $recurringDate['2'] . '-' . $recurringDate['0'];
                    } else {
                    $recurringDate = '';
                    }
                    @endphp
                    <label for="">
                        {{ $recurringDate }}
                        
                        
                    </label>
                    @else
                    <label for="">None</label>
                    @endif
                </td>
                <td>
                    {{ yymm($task->date_month) }}
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="row clearfix mt-3">
    <div class="col pr-0 pl-0" >
        <div class="form-group form-group-default required bdr">
            <label for="schedule_time" class="col-md-12 col-form-label text-md-left">Schedule Date</label>
            <input id="scheduled_task"
                   class="form-control @error('scheduled_task') is-invalid @enderror recurring_end_date"
                   name="scheduled_task" value="{{ ($task->scheduled_task)?date('m-d-Y', strtotime($task->scheduled_task)):'' }}" required autocomplete="scheduled_task" />
        </div>
    </div>
    <div class="col pr-0 pl-1">
        <div class="form-group form-group-default required bdr" aria-required="true" style="overflow:visible !important;">
            <label for="schedule_time" class="col-md-12 col-form-label text-md-left">{{ __('Schedule Start Time') }}</label>
            <input id="schedule_time" type="text"
                   class="timepicker form-control @error('schedule_time') is-invalid @enderror" name="schedule_time"
                   value="{{ $task->schedule_time ? date('h:i a', strtotime($task->schedule_time)) : null }}" required
                   autocomplete="schedule_time" autofocus>
            @error('schedule_time')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col pr-0 pl-1">
        <div class="form-group form-group-default required bdr" aria-required="true" style="overflow:visible !important;">
            <label for="schedule_to_time" class="col-md-12 col-form-label text-md-left">{{ __('Schedule End Time') }}</label>
            <input id="schedule_to_time" type="text"
                   class="timepicker form-control @error('schedule_to_time') is-invalid @enderror" name="schedule_to_time"
                   value="{{ $task->schedule_to_time ? date('h:i a', strtotime($task->schedule_to_time)) : null }}" required
                   autocomplete="schedule_to_time" autofocus>
            @error('schedule_to_time')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-6 pr-0 pl-0" style="padding-left: 0 !important;">
        <div class="form-group form-group-default required bdr" >
            <label for="escorted" class="col-md-12 col-form-label text-md-left" style="padding-left: 0!important;">Escorted</label>
            <select onchange="toggleES(this.value)" id="is_escorted" name="escorted" class="form-control">
                <option value="No" @if ($task->escorted == 'NO') selected @endif >No</option>           
                <option value="Yes" @if ($task->escorted !='No') selected @endif >Yes</option>           
            </select>
        </div>
    </div>
    <!--if ($task->escorted != 'No')-->
    <div class="col pr-0 pl-1 es-name d-none">
        <div class="form-group form-group-default required bdr">
            <label class="col-md-12 col-form-label text-md-left">Escorted Name</label>
            <input required="" type="text" class="form-control" name="escorted_name"
                   value="@if ($task->escorted != 'Yes') {{ $task->escorted }} @endif">
            @error('escorted_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!--endif-->

</div>
@if(request()->completed==null)
<div class="d-flex">
    
<div class="flex-grow-1">
    @if($task->type=='recurring' && $task->schedule=="SCHEDULED")
    <a href="javascript:" onclick="event.preventDefault();revertForm()"  class="btn btn-danger">
        {{ __('Change Recurring Work Order') }}
    </a>
    @endif
</div>

    <div>
        
    <button id="save-form" type="submit" class="btn btn-primary ">
        {{ __('Schedule Task') }}
    </button>
</div>
</div>
@else
<div class="text-right">
    <a href="{{url('backend/task-list/COMPLETED')}}" class="btn btn-primary">Cancel</a>
    <a href="{{ url('backend/workOrder-revert/'.$task->id) }}" class="btn btn-complete ml-2">Revert to schedule</a>
</div>


@endif

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js">
</script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js">
</script>
@if(request()->completed)
<script>
    $(function () {
        $('#task-form input,#task-form select').attr('disabled', true).parents('div.form-group').addClass('disabled');

    })
</script>
@endif
<script type="text/javascript">

    $(function () {
        toggleES($('#is_escorted').val(), 1);
        $('.timepicker').datetimepicker({
            format: 'LT'
        });
    });
    function toggleES(val, $first = 0) {

        if ($first == 0) {
            $('.es-name input').val('');
        }
        if (val == "Yes") {
            $('.es-name').removeClass('d-none');
        } else {
            $('.es-name').addClass('d-none');

    }
    }
    function revertForm(message) {
            $.confirm({
                theme: 'bootstrap',
                title: 'Confirm!',
                content: 'All the <b>"Future"</b> Scheduled/Un Scheduled Records will be deleted by performing this action. Do you want to continue?',
                buttons: {
                    Confirm: function() {
                        window.location="{{ route('admin.tasks.edit',$task->id) }}";
                    },
                    Cancel: function() {

                    }
                }
            });
        }
</script>
