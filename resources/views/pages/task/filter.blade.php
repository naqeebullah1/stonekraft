<div class="container mb-5">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <label for="property_id" class="font-weight-bold">Property</label>
            <select name="property_id" id="property_id" class="form-control">
                <option value="">Select Property</option>
                @foreach ($properties as $property)
                    <option value="{{ $property->id }}">{{ $property->property_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <label for="" class="font-weight-bold">From Date</label>
            <input type="text" name="from_date" id="from_date" class="form-control" autocomplete="off"/>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <label for="" class="font-weight-bold">To Date</label>
            <input type="text" name="to_date" id="to_date" class="form-control" autocomplete="off"/>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <span class="font-weight-bold">Contact Name:</span>    
            <span class="contact_name"></span>    
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <span class="font-weight-bold">Phone:</span>
            <span class="phone"></span>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <span class="font-weight-bold">Email:</span>
            <span class="email"></span>
        </div>
    </div>
</div>
