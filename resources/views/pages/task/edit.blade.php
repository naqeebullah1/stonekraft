@extends('master')
@section('title', 'Task Update')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <h4 class="semi-bold text-center light-heading">{{ __('Update Work Order') }}</h4>
                    <div class="card-body">
                        <form method="POST" id="task-form" action="{{ route('admin.tasks.update', $task->id) }}">
                            @method('PUT')
                            @include('pages.task.form')
                        </form>
                        @can('workOrder-delete')
                            <button class="btn btn-danger float-left"
                                onClick="event.preventDefault();deleteConfirm('task-delete-form-{{ $task->id }}', 'You are about to delete a task, Are you sure?')">Delete</button>

                            <form class="task-form" id="task-delete-form-{{ $task->id }}" style="display:none;"
                                action="{{ route('admin.tasks.destroy', $task->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                            </form>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@if($completed)
    <script>
        $(function(){
            $('#task-form input,#task-form select,#task-form textarea').attr('disabled',true).parents('div.form-group').addClass('disabled');
            
        })
    </script>

@endif
    <script>
        $(function() {
            
            $('#task-form').validate({
                rules: {
                    property_id: {
                        required: true,
                    },
                    issue_id: {
                        required: true,
                    },
                    date_month: {
                        required: true,
                    },

                    recurring_type: {
                        required: true,
                    },
                    item_desc: {
                        required: true,
                    },

                    monthly_date_month: {
                        required: true,
                    },

                    or_each: {
                        required: false,
                    },
                    week_day: {
                        required: false,
                    },
                    recurring_end_date: {
                        required: true,
                    },
                    annually_date_month: {
                        required: true,
                    },

                    annually_recurring_end_date: {
                        required: true,
                    },

                    every_days: {
                        required: true,
                    },
                    daily_recurring_end_date: {
                        required: true,
                    },
                   
                },
                messages: {
                    red_box: {
                        maxlength: 'The white box must be empty'
                    }
                },
                submitHandler: function(form) {
                    form.submit();
                }

            });
        });

        $(document).ready(function() {

            $('.date_month').datepicker({
                autoclose: true,
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months"
            });

            $('.recurring_end_date').datepicker({
                autoclose: true,
                format: "mm-dd-yyyy",
            });

            var recurringType = $('#recurring_type').find(":selected").val();



            if (recurringType === 'MONTHLY') {
                $('#monthly').show();
            }

            if (recurringType === 'ANNUALLY') {
                $('#annually').show();
            }

            if (recurringType === 'DAILY') {
                $('#daily').show();
            }
            // show selected date end

            //check checked radio value button on load page
            var type = $("input[type=radio][name=type]:checked").val();
            if (type === 'one_time') {
                $('#oneTimeType').show();
                $('#recurringType').hide();
                $('#monthly').hide();
                $('#daily').hide();
                $('#annually').hide();
            }
            if (type === 'recurring') {
                $('#recurringType').show();
                $('#oneTimeType').hide();
            }
            if (type === undefined) {
                $('#oneTimeType').show();
                $('#recurringType').hide();
                $('#monthly').hide();
                $('#daily').hide();
                $('#annually').hide();
            }




            $('input[type=radio][name=type]').change(function() {
                if (this.value === 'one_time') {
                    $('#oneTimeType').show();
                    $('#recurringType').hide();
                    $('#monthly').hide();
                    $('#daily').hide();
                    $('#annually').hide();
                }

                if (this.value === 'recurring') {
                    $("select option").each(function() {
                        if ($(this).val() == "MONTHLY") {
                            $(this).attr("selected", "selected");
                        }
                    });
                    $('#oneTimeType').hide();
                    $('#recurringType').show();
                    $('#monthly').show();
                    $('#recurring_type').val(null).trigger('change');
                    $('#recurring_type').val('MONTHLY');
                    $('#recurring_type').trigger('change');
                }
            });

//            $('#recurring_type').change(function() {
//
//                if ($('#recurring_type').val() === 'MONTHLY') {
//                    $('#monthly').show();
//                    $('#daily').hide();
//                    $('#annually').hide();
//                }
//
//                if ($('#recurring_type').val() === 'ANNUALLY') {
//                    $('#monthly').hide();
//                    $('#daily').hide();
//                    $('#annually').show();
//                }
//
//                if ($('#recurring_type').val() === 'DAILY') {
//                    $('#monthly').hide();
//                    $('#daily').show();
//                    $('#annually').hide();
//                }
//            });

setRecurringFields();
            $('#recurring_type').change(function() {
                        setRecurringFields();
            });

function setRecurringFields(){
     if ($('#recurring_type').val() === 'MONTHLY') {
                    $('#monthly').show();
                    $('#daily').hide();
                    $('#annually').hide();
                }else if ($('#recurring_type').val() === 'DAILY') {
                    $('#monthly').hide();
                    $('#daily').show();
                    $('#annually').hide();
                }else {
                    $('#monthly').hide();
                    $('#daily').hide();
                    $('#annually').show();
                }
}


       isSpecific('#date_day');

        });
        $('.end-recurring-on').change(function(){
//            alert($(this).val());
            $('.end-recurring-on').val($(this).val());
        });
         function isSpecific($this){
            $(".is-specific-fields").addClass('d-none');
            $(".is-specific-fields-"+$($this).val()).removeClass('d-none');
        }
    </script>
@endsection
