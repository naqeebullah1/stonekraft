@extends('master')
@section('title', 'schedular')
@section('content')
    <div class="container">
        @include('pages.task.task_search')
        @if (Session::has('message'))
            <div class="row">
                <div class="container text-center">
                    <div class="alert alert-success message">
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>
        @endif
        <div class="pull-left">

            <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top"
                class="rounded-circle bg-danger ml-2 mr-2"></i><span>
                Overdue
            </span>
            <i style="padding: 1px 9px;" class="rounded-circle bg-warning mr-2" data-toggle="tooltip"
                data-placement="top"></i><span>Due Within 7 days</span>
            <i style="padding: 1px 9px;" class="rounded-circle bg-primary mr-2" data-toggle="tooltip"
                data-placement="top"></i><span>Due Within 30 days</span>
        </div>
        @can('workOrder-create')
            <a href="{{ route('admin.tasks.create') }}" class="btn btn-primary float-right mb-2" role="button">Create Work
                Order</a>
        @endcan
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card text-center">
                    <!-- navigation in .card-header -->
                    <div class="card-header">
                        <ul class="nav nav-tabs nav-fill">
                            <li class="nav-item">
                                <a class="nav-link pill1 @if ($type == 'SCHEDULED' || $type == null)
                                active
                                @endif  " data-toggle="tab"
                                    href="{{ route('admin.task.index', ['type'=> 'SCHEDULED']) }}">Maintenance Scheduled</a>
                            </li>
                    
                            <li class="nav-item">
                                <a class="nav-link @if ($type == 'NOT_SCHEDULED')
                                active
                                @endif pill2" data-toggle="tab"
                                    href="{{ route('admin.task.index', ['type'=> 'NOT_SCHEDULED']) }}">Maintenance Not Scheduled</a>
                            </li>
                        
                            <li class="nav-item">
                                <a class="nav-link @if ($type == 'COMPLETED')
                                active
                                @endif pill3" data-toggle="tab"
                                    href="{{ route('admin.task.index', ['type'=> 'COMPLETED']) }}">Maintenance Completed</a>
                            </li>
                           
                        </ul>
                    </div>
                    <!-- .card-body.tab-content  -->
                    <div class="card-body tab-content">
                        @if ($type == 'SCHEDULED' || $type == null)
                            <div class="tab-pane fade @if ($type == 'SCHEDULED' || $type == null)
                            show
                            active
                            @endif" id="pill1">
                                @include('pages.task.schedule_list')
                            </div>
                        @endif
                        @if ($type == 'NOT_SCHEDULED')
                        <div class="tab-pane fade @if ($type == 'NOT_SCHEDULED')
                        show
                        active
                        @endif" id="pill2">
                        
                            @include('pages.task.not_schedule_list')
                        </div>
                        @endif
                        @if ($type == 'COMPLETED')
                            
                        <div class="tab-pane fade @if ($type == 'COMPLETED')
                        show
                        active
                        @endif" id="pill3">
                            @include('pages.task.completed_list')
                        </div>
                        @endif
                    </div><!-- /.card-body -->
                </div><!-- /.card -->
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var hash = window.location.hash;
            if (hash != '') {
                hash = hash.split('#');
                $('.' + hash[1]).click();
                $('#' + hash[1]).show();
            }


            setTimeout(() => {
                $('.message').fadeOut('slow');
            }, 3000);

            $('.pill1').click(function() {
                // $(this).addClass("active");
                // $('.pill2').removeClass("active");
                // $('.pill3').removeClass("active");

                // $('#pill1').addClass("active");
                // $('#pill1').show();
                // $('#pill2').hide();
                // $('#pill3').hide();
                var url = "{{ route('admin.task.index', ['type'=> 'SCHEDULED']) }}"
                window.location.replace(url)
            });

            $('.pill2').click(function() {
                // $(this).addClass("active");
                // $('.pill1').removeClass("active");
                // $('.pill3').removeClass("active");
                // $('#pill2').addClass("active");
                // $('#pill1').removeClass("active");
                // $('#pill3').removeClass("active");

                // $('#pill1').hide();
                // $('#pill2').show();
                // $('#pill3').hide();
                var url = "{{ route('admin.task.index', ['type'=> 'NOT_SCHEDULED']) }}"
                window.location.replace(url)
            });

            $('.pill3').click(function() {
                // $(this).addClass("active");
                // $('.pill1').removeClass("active");
                // $('.pill2').removeClass("active");
                // $('#pill3').addClass("active");
                // $('#pill1').removeClass("active");
                // $('#pill2').removeClass("active");

                // $('#pill1').hide();
                // $('#pill2').hide();
                // $('#pill3').show();
                var url = "{{ route('admin.task.index', ['type'=> 'COMPLETED']) }}"
                window.location.replace(url)
            });

            $('#from_date, #to_date').datepicker({
                autoclose: true,
                format: "mm-dd-yyyy",
            });

            $('.property').change(function() {

                if ($(this).val() !== '') {
                    $('.contact_name_label').show();
                    $('.phone_label').show();
                    $('.email_label').show();
                } else {
                    $('.contact_name_label').hide();
                    $('.phone_label').hide();
                    $('.email_label').hide();
                }
            });


            $('#property_id').change(function() {
                var propertyId = $(this).val();

                if (propertyId !== '') {
                    $.ajax({
                        url: "{{ route('admin.get.client.info') }}",
                        type: "GET",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "property_id": propertyId
                        },
                        cache: false,
                        async: false,
                        success: function(data) {
                            var client = JSON.parse(data);


                            var fullName = client.first_name + ' ' + client
                                .last_name;
                            $('.contact_name').text(fullName);
                            $('.phone').text(client.phone_one);
                            $('.email').text(client.email);


                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                } else {
                    $('.contact_name').text('');
                    $('.phone').text('');
                    $('.email').text('');
                }
            });
        });
    </script>
@endsection
