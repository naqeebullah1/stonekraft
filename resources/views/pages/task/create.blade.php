@extends('master')
@section('title', 'Task Create')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h4 class="semi-bold text-center light-heading">{{ __('Create Work Order') }}</h4>
                    <div class="card-body">
                        <form method="POST" id="task-form" action="{{ route('admin.tasks.store') }}">
                            @include('pages.task.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
         $(function() {
            $('#task-form').validate({
                rules: {
                    property_id: {
                        required: true,
                    },
                    item: {
                        required: true,
                    },
                    date_month: {
                        required: true,
                    },
                    item_desc: {
                        required: true,
                    },

                    type: {
                        required: true,
                    },
                    recurring_type: {
                        required: true,
                    },

                    monthly_date_month: {
                        required: true,
                    },
                    week_day: {
                        required: false,
                    },
                    recurring_end_date: {
                        required: true,
                    },
                    annually_date_month: {
                        required: true,
                    },

                    annually_recurring_end_date: {
                        required: true,
                    },

                    every_days: {
                        required: true,
                    },
                    daily_recurring_end_date: {
                        required: true,
                    },
                    is_active: {
                        required: true,
                    },
                   
                },
                messages: {
                    red_box: {
                        maxlength: 'The white box must be empty'
                    }
                },
                submitHandler: function(form) {
                    form.submit();
                }

            });
        });
        $(document).ready(function() {

            $('.date_month').datepicker({
                autoclose: true,
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months"
            });

            $('.recurring_end_date').datepicker({
                autoclose: true,
                format: "mm-dd-yyyy",
            });


            $('input[type=radio][name=type]').change(function() {
                if (this.value === 'one_time') {
                    $('#oneTimeType').show();
                    $('#recurringType').hide();
                    $('#monthly').hide();
                    $('#daily').hide();
                    $('#annually').hide();
                }

                if (this.value === 'recurring') {
                    $("select option").each(function() {
                        if ($(this).val() == "MONTHLY") {
                            $(this).attr("selected", "selected");
                        }
                    });
                    $('#oneTimeType').hide();
                    $('#recurringType').show();
                    $('#monthly').show();
                    $('#recurring_type').val(null).trigger('change');
                    $('#recurring_type').val('MONTHLY');
                    $('#recurring_type').trigger('change');
                }
            });

            $('#recurring_type').change(function() {

                if ($('#recurring_type').val() === 'MONTHLY') {
                    $('#monthly').show();
                    $('#daily').hide();
                    $('#annually').hide();
                }else if ($('#recurring_type').val() === 'DAILY') {
                    $('#monthly').hide();
                    $('#daily').show();
                    $('#annually').hide();
                }else {
                    $('#monthly').hide();
                    $('#daily').hide();
                    $('#annually').show();
                }
            });

            /*$('#monthly_date_month').change(function() {
                var month = $(this).val();

                if (month !== '') {
                    $.ajax({
                        url: "{{ route('admin.get.month.numbers') }}",
                        type: "GET",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "month": month
                        },
                        cache: false,
                        async: false,
                        success: function(data) {
                            var month = JSON.parse(data);
                            var i = 0;
                            var option = '<option value="">Select Week</option>';
                            var str = '';

                            while (i < month.length) {
                                if (i === 0) {
                                    str = 'First';
                                }

                                if (i === 1) {
                                    str = 'Second';
                                }

                                if (i === 2) {
                                    str = 'Third';
                                }

                                if (i === 3) {
                                    str = 'Fourth';
                                }

                                if (i === 4) {
                                    str = 'Last';
                                }

                                option = option + '<option value="' + month[i] +
                                    '" @if ($task->or_each_day == "'+ month[i] +'") {{ 'selected' }} @endif>' +
                                    str + '</option>';

                                i++;
                            }
                            $('#or_each').html(option);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                }
            });*/

            $('#week_day').change(function() {
                var monthYear = $('#monthly_date_month').val();
                var week = $('#or_each').val();
                var weekDay = $(this).val();

                if (monthYear !== '' && week !== '' && weekDay !== '') {
                    $.ajax({
                        url: "{{ route('admin.get.days.date') }}",
                        type: "GET",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "monthYear": monthYear,
                            "week": week,
                            "weekDay": weekDay,
                        },
                        cache: false,
                        async: false,
                        success: function(data) {
                            var result = JSON.parse(data);

                            console.log(result);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                }
            });
            
            $('#property_id').change(function() {
                var propertyId = $(this).val();

                if (propertyId != '') {

                    $.ajax({
                        url: "{{ route('admin.generate.work.order') }}",
                        type: "GET",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "property_id": propertyId,

                        },
                        cache: false,
                        async: false,
                        dataType: 'JSON',
                        success: function(data) {
//                            alert(data.work_order);
                            var result = data.work_order;
                            if (result == 'Not-Assigned-client') {
                                $('#work_order').val('Please! Assign property to a client.');
                                $('#save-form').attr('disabled', true);
                            } else {
                                $('#save-form').attr('disabled', false);
                                $('#work_order').val(result);
                            }
                            $('#client_id').val(data.client_id);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                }
            });
         isSpecific('#date_day');

        });
         function isSpecific($this){
            $(".is-specific-fields").addClass('d-none');
            $(".is-specific-fields-"+$($this).val()).removeClass('d-none');
        }
    </script>
@endsection
