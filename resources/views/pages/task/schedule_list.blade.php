@php
$task_label1 = 'Status';
$task_label2 = 'Scheduled Date';
$task_label3 = 'R/O';
$task_label4 = 'Work Order #';
$task_label5 = 'Item';
$task_label6 = 'Action';
$serial = 0;
@endphp
<div class="container grid-wrapper p-3">
    <div class="grid-header">
        <div class="row d-none d-md-flex mb-md-2 p-md-1">
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label1 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label2 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label3 }}</h4>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label4 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label5 }}</h4>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label6 }}</h4>
            </div>
        </div>
    </div>
    <div class="grid-body">
        @foreach ($tasks as $task)
            @php
                $active = '<i style="" class="rounded-circle bg-success" data-toggle="tooltip" data-placement="top" title=""></i>';
                $inactive = '<i style="" class="rounded-circle bg-danger" data-toggle="tooltip" data-placement="top" title=""></i>';
            @endphp

            @if ($task->schedule == 'SCHEDULED')
                <div class="row mb-md-0 p-md-1 mb-3 p-1">
                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label1 }}</h4>
                          @if ($task->remaining_days >= 30 || $task->remaining_days == 0 || $task->remaining_days == null || $task->remaining_days == '')
                            <i style="padding: 1px 9px;" data-toggle="tooltip"
                                class="rounded-circle bg-danger"></i>
                        @endif
                        @if ($task->remaining_days <= 7 && $task->remaining_days >= 1)
                            <i style="padding: 1px 9px;" class="rounded-circle bg-warning" data-toggle="tooltip"
                                data-placement="top"></i>
                        @endif
                        @if ($task->remaining_days <= 30 && $task->remaining_days > 7)
                            <i style="padding: 1px 9px;" class="rounded-circle bg-primary" data-toggle="tooltip"
                                data-placement="top"></i>
                        @endif
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label2 }}</h4>
                            <p class="m-0 p-1">{{ date('m-d-Y', strtotime($task->scheduled_task)) }}</p>
                       
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label3 }}</h4>
                        @if ($task->type == 'one_time')
                        <p class="m-0 p-1">One Time</p>
                    @endif
                    @if ($task->type == 'recurring')
                        <p class="m-0 p-1">Recurring</p>
                    @endif
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label4 }}</h4>
                        <p class="m-0 p-1">{{ $task->work_order }}</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label5 }}</h4>
                        <p class="m-0 p-1">@isset($task->issue->issue){{ $task->issue->issue }}@endisset</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label6 }}</h4>
                        <br class="d-none d-md-none d-sm-block " />

                        <a href="{{ route('admin.task.complete', $task->id) }}" class="btn btn-primary"
                            >Completed</a>
                        @can('workOrder-edit')

                          <a href="{{ route('admin.schedule.edit', $task->id) }}"
                                class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button">Edit</a>
                       
                           
                        @endcan
                        {{-- @can('task-delete')
                            <button class="btn btn-danger pt-1 pb-1 pl-2 pr-2"
                                onClick="event.preventDefault();deleteConfirm('task-delete-form-{{ $task->id }}', 'You are about to delete a task, Are you sure?')">Delete</button>

                            <form class="task-form" id="task-delete-form-{{ $task->id }}" style="display:none;"
                                action="{{ route('admin.task.delete', $task->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                            </form>
                        @endcan --}}
                    </div>
                </div>
            @endif
        @endforeach
        <div class="row float-right pr-5 pt-3">
         {{ $tasks->fragment('pill1')->links() }}
                </div>
         
    </div> <!-- End of ROW -->
</div>

