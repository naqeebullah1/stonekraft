@csrf
<style>
    #the-count {
        float: right;
        padding: 0.1rem 0 0 0;
        font-size: 0.875rem;
    }

    .form-group-default textarea.form-control {
        height: 80px !important;
    }

    .work_order {
        /*background-color: #CCCCCC !important;*/
    }
</style>
<div class="row clearfix">
    <input type="hidden" name="schedule" value="{{ $task->schedule }}">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="property_id" class="col-md-12 col-form-label text-md-left">{{ __('Property') }}</label>
            <select id="property_id" class="form-control @error('property_id') is-invalid @enderror" name="property_id"
                value="{{ old('property_id') }}" autocomplete="property_id" autofocus>
                <option value="">Select Property</option>
                @foreach ($properties as $property)
                    <option value="{{ $property->id }}" @if ($task->property_id == $property->id) {{ 'selected' }} @endif>
                        {{ $property->property_name }}</option>
                @endforeach
            </select>
            @error('property_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<input type="hidden" name="client_id" id="client_id" value="{{$task->client_id}}">
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required work_order" aria-required="true">
            <label for="work_order" class="col-md-12 col-form-label text-md-left">{{ __('Work Order') }}</label>
            <input id="work_order" type="text"
                class="form-control @error('work_order') is-invalid @enderror text-dark  " name="work_order"
                value="{{ old('work_order') ?? $task->work_order }}" autocomplete="work_order" autofocus readonly>
            @error('work_order')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="" class="col-md-12 col-form-label text-md-left">{{ __('Item') }}</label>
            <select id="issue_id" class="form-control @error('item') is-invalid @enderror" name="item"
                value="{{ old('item') }}" autocomplete="issue_id" autofocus>
                <option value="">Select Item</option>
                @foreach ($issues as $issue)
                    <option value="{{ $issue->id }}" @if ($task->issue_id == $issue->id) {{ 'selected' }} @endif
                        class="text-uppercase">{{ $issue->issue }}</option>
                @endforeach
            </select>
            @error('item')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12 pr-0">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="item_desc" class="col-md-12 col-form-label text-md-left">{{ __('Item Description') }}</label>
            <textarea id="item_desc" class="form-control @error('item_desc') is-invalid @enderror" name="item_desc" 
                      autofocus rows="10" style="resize: none;">{{ old('item_desc') ?? $task->item_desc }}</textarea>
            @error('item_desc')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group form-group-default" aria-required="true">
            <label for="description" class="col-md-12 col-form-label text-md-left">{{ __('Work Order Notes') }}</label>
            <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description"
                autocomplete="description" autofocus rows="10" style="resize: none;">{{ old('description') ?? $task->description }}</textarea>
            @error('description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

<div class="row clearfix mt-3 mb-3">
    <div class="col-md-3">
    </div>
    <div class="col-md-3">
        <label class="radio-container">One Time
            <input type="radio" @if ($task->type == 'one_time') checked @endif name="type" id="one_time"
                value="one_time">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="col-md-3">
        <label class="radio-container">Recurring
            <input type="radio" name="type" @if ($task->type == 'recurring') checked @endif id="recurring"
                value="recurring">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="col-md-3">

    </div>
</div>
<div id="oneTimeType">
    <div class="row clearfix mt-3 mb-3">
        <div class="col-md-6">
            <div class="form-group form-group-default">
                <label for="date_month" class="col-md-12 col-form-label text-md-left">{{ __('Select Month') }}</label>
                <input type="text" name="date_month" value="{{ old('date_month') ?? yymm($task->date_month) }}"
                    class="form-control date_month @error('date_month') is-invalid @enderror" autocomplete="off" />
                @error('date_month')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-md-3">
            <div class="custom-control custom-checkbox mt-3">
                <input type="checkbox" name="escorted" @if ($task->escorted == 'Yes') checked @endif class="custom-control-input"
                     id="escorted">
                <label class="custom-control-label ml-1" for="escorted" style="font-size: 20px;">Escorted</label>
            </div>
        </div>
    </div>
</div>

<div style="display: none" id="recurringType">
    <div class="row clearfix mt-3 mb-3">
        <div class="col-md-3 mt-3">
            <span class="float-right pr-2">Recurring (Repeat)</span>
        </div>
        <div class="col-md-6">
            <div class="form-group form-group-default">
                <select id="recurring_type" class="form-control @error('recurring_type') is-invalid @enderror"
                    name="recurring_type" value="{{ old('recurring_type') }}" required autocomplete="recurring_type"
                    autofocus data-init-plugin="select2">
                    <option value="DAILY" @if ($task->recurring_type == 'DAILY') {{ 'selected' }} @endif>Daily</option>
                    <option value="MONTHLY" @if ($task->recurring_type == 'MONTHLY') {{ 'selected' }} @endif>Monthly</option>
                    <option value="QUARTERLY" @if ($task->recurring_type == 'QUARTERLY') {{ 'selected' }} @endif>Quarterly</option>
                    <option value="TWICE A YEAR" @if ($task->recurring_type == 'TWICE A YEAR') {{ 'selected' }} @endif>Twice a year</option>
                    <option value="ANNUALLY" @if ($task->recurring_type == 'ANNUALLY') {{ 'selected' }} @endif>Annually</option>
                </select>
                @error('recurring_type')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-md-3">
            <div class="custom-control custom-checkbox mt-3">
                <input type="checkbox" name="escorted" class="custom-control-input"
                    @if ($task->escorted == 'Yes') checked @endif id="escorted2">
                <label class="custom-control-label ml-1" for="escorted2" style="font-size: 20px;">Escorted</label>
            </div>
        </div>
    </div>
</div>
<div style="display: none" id="monthly">
    <div class="row clearfix mt-3 mb-3">
        <div class="col-lg">
            <div class="form-group form-group-default">
                <label>ON</label>
                <input type="text" name="monthly_date_month"
                    class="form-control date_month @error('monthly_date_month') is-invalid @enderror"
                    autocomplete="off" value="{{ old('monthly_date_month') ?? yymm($task->date_month) }}"
                    id="monthly_date_month" />
                @error('monthly_date_month')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-lg">
            <div class="form-group form-group-default">
                <label for="date_day">Is Specific date</label>
                <select onchange="isSpecific(this)" name="is_specific" id="date_day" class="form-control @error('date_day') is-invalid @enderror" name="date_day" data-init-plugin="select2">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select>
                @error('or_each')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        
        <div class="col-lg is-specific-fields is-specific-fields-Yes">
            <div class="form-group form-group-default">
                <label>Specify Date</label>
                <select id="date_day" class="form-control
                        @error('recurring_type') is-invalid @enderror"
                    name="date_day" autofocus data-init-plugin="select2">   
                    <option value="">Select Day</option>
                    @for($i=1;$i<=30;$i++)
                    <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>  
                @error('date_day')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-lg is-specific-fields is-specific-fields-No">
            <div class="form-group form-group-default">
                <label>OR EACH</label>
                <input type="hidden" name="" class="orEachDay" id="" value="{{ $task->or_each_day }}">
                <?php
//                dump($task->or_each_day);
                ?>
                <select id="or_each" class="form-control @error('or_each') is-invalid @enderror" name="or_each"
                     autocomplete="or_each" data-init-plugin="select2">
                    <option value="">Select Week</option>
                    <option value="1" {{ ($task->or_each_day==1)?'selected':'' }}>First</option>
                    <option value="2" {{ ($task->or_each_day==2)?'selected':'' }}>Second</option>
                    <option value="3" {{ ($task->or_each_day==3)?'selected':'' }}>Third</option>
                    <option value="4" {{ ($task->or_each_day==4)?'selected':'' }}>Fourth</option>
                </select>
                @error('or_each')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-lg is-specific-fields is-specific-fields-No">
            <div class="form-group form-group-default">
                <label for="">Week Day</label>
                <select id="week_day" class="form-control @error('week_day') is-invalid @enderror" name="week_day"
                    value="{{ old('week_day') }}" required autocomplete="week_day" data-init-plugin="select2">
                    <option value="">Select Day</option>
                    <option value="MONDAY" @if ($task->week_day == 'MONDAY') {{ 'selected' }} @endif>Monday</option>
                    <option value="TUESDAY" @if ($task->week_day == 'TUESDAY') {{ 'selected' }} @endif>Tuesday</option>
                    <option value="WEDNESDAY" @if ($task->week_day == 'WEDNESDAY') {{ 'selected' }} @endif>Wednesday</option>
                    <option value="THURSDAY" @if ($task->week_day == 'THURSDAY') {{ 'selected' }} @endif>Thursday</option>
                    <option value="FRIDAY" @if ($task->week_day == 'FRIDAY') {{ 'selected' }} @endif>Friday</option>
                    <option value="SATURDAY" @if ($task->week_day == 'SATURDAY') {{ 'selected' }} @endif>Saturday</option>
                    <option value="SUNDAY" @if ($task->week_day == 'SUNDAY') {{ 'selected' }} @endif>Sunday</option>
                </select>
                @error('week_day')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        </div>
      <div class="row clearfix mt-3 mb-3">
        <div class="col-md-3">
            <div class="form-group form-group-default">
                <label for="">End Recurring On</label>
                <input type="text" name="recurring_end_date"
                    value="{{ (old('recurring_end_date'))?old('recurring_end_date'): formatDate($task->recurring_end_date) }}"
                    class="end-recurring-on form-control recurring_end_date @error('recurring_end_date') is-invalid @enderror"
                    autocomplete="off" />
                @error('recurring_end_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>
</div>
<div style="display: none" id="annually">
    <div class="row clearfix mt-3 mb-3">
        <div class="col-md">
            <div class="form-group form-group-default">
                <label>ON</label>
                <input type="text" name="annually_date_month"
                    value="{{ old('annually_date_month') ?? yymm($task->date_month) }}"
                    class="form-control date_month @error('annually_date_month') is-invalid @enderror"
                    autocomplete="off" />
                @error('annually_date_month')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col">
            <div class="form-group form-group-default">
                <label for="">End Recurring On</label>
                <input type="text" name="annually_recurring_end_date"
                        value="{{ (old('annually_recurring_end_date'))?old('annually_recurring_end_date'):formatDate($task->recurring_end_date) }}"
                    class=" end-recurring-on form-control recurring_end_date @error('annually_recurring_end_date') is-invalid @enderror"
                    autocomplete="off" />
                @error('annually_recurring_end_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>
</div>
<div style="display: none" id="daily">
    <div class="row clearfix mt-3 mb-3">
        <div class="col-md-4">
            <div class="form-group form-group-default">
                <label for="">Starting Date</label>
                <input type="text" name="starting_date" value="{{ old('starting_date') ?? $task->starting_date }}"
                    class="form-control datepicker @error('starting_date') is-invalid @enderror" autocomplete="off" />
                @error('starting_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-md-4 d-flex">
            <div class="form-group form-group-default">
                <label for="">Every</label>
                <input type="text" name="every_days" value="{{ old('every_days') ?? $task->every_days }}"
                    class="form-control integeronly @error('every_days') is-invalid @enderror" autocomplete="off" />
                @error('every_days')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <span class="align-self-center">Days</span>
        </div>
        
        <div class="col-md-4">
            <div class="form-group form-group-default">
                <label for="">End Recurring On</label>
                <input type="text" name="daily_recurring_end_date"
                    value="{{ (old('daily_recurring_end_date'))?old('daily_recurring_end_date'):formatDate($task->recurring_end_date) }}"
                   class="end-recurring-on form-control recurring_end_date @error('daily_recurring_end_date') is-invalid @enderror"
                    autocomplete="off" />
                @error('daily_recurring_end_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required">
            <label for="is_active" class="col-md-12 col-form-label text-md-left">{{ __('Status') }}</label>
            <select id="is_active" class="form-control @error('is_active') is-invalid @enderror" name="is_active"
                value="{{ old('is_active') }}" autocomplete="is_active" autofocus>
                <option value="">Select Status</option>
                <option value="1" @if ($task->is_active == '1') {{ 'selected' }} @endif>Active</option>
                <option value="0" @if ($task->is_active == '0') {{ 'selected' }} @endif>Inactive</option>
            </select>
            @error('is_active')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
@if(isset($completed))
<a href="<?=url('backend/workOrder-revert/'.request()->id)?>" class="btn btn-complete float-right ml-2">Revert to schedule</a>
<a href="<?=url('backend/task-list/COMPLETED')?>" class="btn btn-primary float-right">Cancel</a>

@endif
<button id="save-form" type="submit" class="btn btn-primary float-right @if(isset($completed)) d-none @endif">
    @isset($task->id)
        {{ __('Update') }}
    @else
        {{ __('Create') }}
    @endisset
</button>
