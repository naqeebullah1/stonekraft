@extends('master')
@section('title', 'Task Schedule')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h4 class="semi-bold text-center light-heading">{{ __('Schedule Work Order') }}</h4>
                    <div class="card-body">
                        <form method="POST" id="task-form" action="{{ route('admin.schedule.task.store', $task->id) }}">
                            @include('pages.task.scheduleForm')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
$('#task-form').validate();
            $('.date_month').datepicker({
                autoclose: true,
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months"
            });

            $('.recurring_end_date').datepicker({
                autoclose: true,
                format: "mm-dd-yyyy",
            });
        });
    </script>
@endsection
