@extends('master')
@section('title', 'schedular')
@section('content')

<div class="container-fluid">
    @if($client)
    <div class="card">
        <!-- Start of Extra Information-->
        <h4 class="bold" style="padding:0px 10px;font-size:14px;"><a class="toggler" href="javascript:;"
                                                                     onClick="toggleSearch('client-info', '0', this)">{{ __('Client Information') }} <span
                    class="text-danger">*</span><span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>

        <div class="card-body" style="display:none"
             id="client-info">
            @include('contracts.partials.client_information')

        </div>
    </div>

    <div class="card">
        <!-- Start of Extra Information-->
        <h4 class="bold" style="padding:0px 10px;font-size:14px;"><a class="toggler" href="javascript:;"
                                                                     onClick="toggleSearch('detailfields', '0', this)">{{ __('Property Information') }} <span
                    class="text-danger">*</span><span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>

        <div class="card-body" style="display:none"
             id="detailfields">
            @include('contracts.partials.property_information')
        </div>
    </div>
    @include('contracts.partials.contract_info')
    @endif
    @if(!$client || request()->has('search'))
        @include('pages.task.task_search')
    @endif
    @if (Session::has('message'))
    <div class="row">
        <div class="container text-center">
            <div class="alert alert-success message">
                {{ Session::get('message') }}
            </div>
        </div>
    </div>
    @endif
    @can('workOrder-create')
    <!--<a href="{{ route('admin.tasks.create') }}" class="btn btn-primary float-right mb-2" role="button">Create Work Order</a>-->
    @endcan
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card text-center">
                <!-- navigation in .card-header -->
                <div class="card-header">
                    <?php
                    $isFilterApplied = '';
                    if (request()->all()) {
                        $filters = request()->all();
                        $isFilterApplied = '?' . http_build_query($filters, '', '&');
                    }
                    ?>

                    <ul class="nav nav-tabs nav-fill">

                        <li class="nav-item">
                            <a id="scheduled-link" class='nav-link pill1 @if($type=="SCHEDULED") active @endif' 
                            
                               href="{{ route('admin.task.index', ['type'=> 'SCHEDULED']).$isFilterApplied }}">Maintenance Scheduled</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link pill2 @if($type=='NOT_SCHEDULED') active @endif" 
                               
                               href="{{ route('admin.task.index', ['type'=> 'NOT_SCHEDULED']).$isFilterApplied }}">Maintenance Not Scheduled</a>
                        </li>

                        <li class="nav-item">
                            <a 
                               class="nav-link pill3 @if($type=='COMPLETED') active @endif"
                               href="{{ route('admin.task.index', ['type'=> 'COMPLETED']).$isFilterApplied }}">Maintenance Completed</a>
                        </li>
                    </ul>
                </div>

                <!-- .card-body.tab-content  -->
                <div class="card-body tab-content">
                    <div class="tab-pane fade show active" id="home">
                        @include('contracts.partials.all_tasks')
                    </div>
                </div><!-- /.card-body -->
            </div><!-- /.card -->
        </div>
    </div>
    <div class="row">
    <div class="col-sm-12">
        <div class="card text-center">
            <!-- navigation in .card-header -->
            <div class="card-header all-task-container">
                @if($client)
                    @include('contracts.partials.assign_tasks')
                @endif
            </div>
        </div>
    </div>    
    </div>
</div>

<script>
    $(document).ready(function () {
        //getTask('#scheduled-link');
    });
    function paginatedTasks($this) {
        var href = $($this).attr('href');
        $('#home').load(href);
        return false;
    }
    function getTask($this) {
        $('.nav-link').removeClass('active');
        $($this).addClass('active');
        var href = $($this).attr('href');
        $('#home').load(href);
        return false;
    }
    function editTask($id) {
        $('.all-task-container').load("{{ url('backend/contract_task/edit') }}/" + $id, function () {
            $('.d-datepicker').datepicker({
                autoclose: true
            });
            $('.d-select2').select2();
        });
    }
    function taskUploadImages($id) {
        $('.holder').html('');
        $('.all-task-container').load("{{ url('backend/contract_task/upload_images') }}/" + $id, function () {
            $('.d-datepicker').datepicker({
                autoclose: true
            });
            $('.d-select2').select2();
        });
    }

    function getIssuesList() {
        resetTasks();
    }
    function resetTasks() {
        var issue_id = $('#issue_id').val();
        $('#issues-list-container').load('{{url("backend/load/issue-list/")}}/' + issue_id, function () {
            $('#selected-issues-container').html('');
        });
    }
    function moveBack($this) {
        $($this).attr('onclick', 'moveThis(this)');
        $($this).addClass('not-selected');
        $($this).appendTo('.issues-list-container');
        $($this).find('.issue-list-id').attr('disabled', true);
    }
    function moveThis($this) {
        $($this).attr('onclick', 'moveBack(this)');
        $($this).removeClass('not-selected');
        $($this).find('.issue-list-id').removeAttr('disabled');
        $($this).appendTo('#selected-issues-container');
    }
    function toggleFields() {
        $('.dd').addClass('d-none');
        $('.d-' + $('#type').val()).removeClass('d-none');
    }
    function removeIssue($this) {
        if ($('.other-task').length == 1) {
            alert('Last Element can not be deleted. You may left empty.');
            return false;

        }
        $($this).parents('.other-task').remove();
    }  

        $('.client').change(function () {

            if ($(this).val() !== '') {
                $('.contact_name_label').show();
                $('.phone_label').show();
                $('.email_label').show();
                $('.address_label').show();
            } else {
                $('.contact_name_label').hide();
                $('.phone_label').hide();
                $('.email_label').hide();
                $('.address_label').hide();
            }
        });


        $('#client_id').change(function () {

            var client_id = $(this).val();

            if (client_id !== '') {
                $.ajax({
                    url: "{{ route('admin.get.client.info') }}",
                    type: "GET",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "client_id": client_id
                    },
                    cache: false,
                    async: false,
                    success: function (data) {
                            console.log(data);
                        var data = JSON.parse(data);

                        $('.contact_name').html(data.first_name);
                        $('.phone').html(data.phone_one);
                        $('.email').html(data.email);
                        $('.address').html(data.address_one);

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                $('.contact_name').text('');
                $('.phone').text('');
                $('.email').text('');
                $('.address').text('');
            }
        });      
</script>
@endsection


