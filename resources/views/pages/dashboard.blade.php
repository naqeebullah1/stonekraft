@extends('master')
@section('title', 'Dashboard')
@section('content')
<div class="container-fluid">
    <div class="col-sm-12">
        <h2 style="font-weight:bold;margin:0px 0px 10px 0px;">Dashboard</h2>

        <div class="card text-center">
            <div class="row" style="">
                <ul class="nav nav-pills card-header-pills custom-pills">                    
                    <!--PENDING-->
                    <div class="col-md-4 mb-2">
                        <li class="nav-item">

                            <a class="nav-link pill3 {{ ($type=='PENDING')?'active':'' }}" href="{{url('backend/dashboard/PENDING')}}">
                                <div class="card-counter info">
                                    <i class="fa fa-ticket"></i>
                                    <span class="count-numbers">
                                        {{ $pendingLeadCount }}
                                    </span>
                                    <span class="count-name">Active Leads</span>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="col-md-4 mb-2">
                        <li class="nav-item">
                            <a class="nav-link {{ ($type=='SCHEDULED')?'active':'' }} pill1" href="{{url('backend/dashboard/SCHEDULED')}}">
                                <div class="card-counter primary">
                                    <i class="fa fa-gear"></i>
                                    <span class="count-numbers">
                                        {{ $scheduledCount->count() }}
                                    </span>
                                    <span class="count-name">Scheduled tasks to be completed in next 60 days</span>
                                </div>
                            </a>
                        </li>
                    </div>
                    <div class="col-md-4 mb-2">
                        <li class="nav-item">
                            <!--NOT_SCHEDULED-->
                            <a class="nav-link pill2  {{ ($type=='NOT_SCHEDULED')?'active':'' }}" href="{{url('backend/dashboard/NOT_SCHEDULED')}}">
                                <div class="card-counter danger">
                                    <i class="fa fa-calendar"></i>
                                    <span class="count-numbers">
                                        {{ $notScheduledCount->count() }}
                                    </span>
                                    <span class="count-name">Tasks to be scheduled</span>
                                </div>
                            </a>
                        </li>
                    </div>

                    <!--PENDING-->
                    {{--
                    <div class="col-md-3 mb-2">
                        <li class="nav-item">

                            <a class="nav-link pill3 {{ ($type=='PENDING')?'active':'' }}" href="{{url('backend/dashboard/ARCHIVE')}}">
                                <div class="card-counter success">
                                    <i class="fa fa-ticket"></i>
                                    <span class="count-numbers">
                                        {{ $archiveLeadCount }}
                                    </span>
                                    <span class="count-name">Archived Leads</span>
                                </div>
                            </a>
                        </li>
                    </div>
                    --}}
                </ul>
            </div>
            <hr>
            <!-- .card-body.tab-content  -->
            <div class="card-body pt-0 tab-content">
                @if($type=="SCHEDULED")
                <div class="tab-pane fade {{ ($type=='SCHEDULED')?'show active':'' }}" id="pill1">
                <div class="pull-left mb-3">
                    @include('partials.legends')
                </div>
            
                    <!--include('partials.schedule_list')-->
                    @include('partials.order_list')
                </div>
                @endif
                @if($type=="NOT_SCHEDULED")
                <div class="tab-pane fade {{ ($type=='NOT_SCHEDULED')?'show active':'' }}" id="pill2">
                     <div class="pull-left mb-3">
                </div>
                    @include('partials.order_list')
                </div>
                @endif
                @if($type=="PENDING")
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active"  href="{{ url('backend/dashboard/PENDING') }}" role="tab">Active</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  href="{{ url('backend/dashboard/ARCHIVE') }}" role="tab">Archive</a>
                        </li>
                    </ul><!-- Tab panes -->
                    @include('partials.lead_list')
                {{--
                <div class="tab-pane fade {{ ($type=='PENDING')?'show active':'' }}" id="pill3">
                    <!--include('partials.pending_approval')-->
                    @include('partials.lead_list')

                </div>
                --}}
                @endif
                @if($type=="ARCHIVE")
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active"  href="{{ url('backend/dashboard/PENDING') }}" role="tab">Active</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"  href="{{ url('backend/dashboard/ARCHIVE') }}" role="tab">Archive</a>
                    </li>
                </ul><!-- Tab panes -->
                <div class="tab-pane fade {{ ($type=='ARCHIVE')?'show active':'' }}" id="pill3">
                    <!--include('partials.pending_approval')-->
                    @include('partials.archive_lead_list')

                </div>
                @endif
            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
</div>

<script>
    $(document).ready(function () {

        $('.pill1').click(function () {
            $('#pill1').show();
            $('#pill2').hide();
            $('.pill2').removeClass("active");
            $('#pill3').hide();
            $('.pill3').removeClass("active");
        });

        $('.pill2').click(function () {
            $('#pill1').hide();
            $('.pill1').removeClass("active");
            $('#pill2').show();
            $('#pill3').hide();
            $('.pill3').removeClass("active");
        });

        $('.pill3').click(function () {
            $('#pill1').hide();
            $('.pill1').removeClass("active");
            $('#pill2').hide();
            $('.pill2').removeClass("active");
            $('#pill3').show();
        });
    });
  
</script>
@endsection
