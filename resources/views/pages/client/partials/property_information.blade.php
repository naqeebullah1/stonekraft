<div class="row clearfix">
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label for="address_one" class="col-md-12 col-form-label text-md-left">{{ __('Address One') }}<span
                            class="text-danger">*</span></label>
                    <input id="address_one" type="text"
                           class="form-control @error('address_one') is-invalid @enderror" name="address_one"
                           value="{{ old('address_one') ?? $client->address_one }}" autofocus>
                    @error('address_one')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label for="address_two"
                           class="col-md-12 col-form-label text-md-left">{{ __('Address Two') }}</label>
                    <input id="address_two" type="text"
                           class="form-control @error('address_two') is-invalid @enderror" name="address_two"
                           value="{{ old('address_two') ?? $client->address_two }}" autofocus>
                    @error('address_two')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-4">
                <div class="form-group form-group-default">
                    <label for="city"
                           class="col-md-12 col-form-label text-md-left">{{ __('City ') }}<span
                            class="text-danger">*</span></label>
                    <input id="city" type="text" class="form-control @error('city') is-invalid @enderror"
                           name="city" value="{{ old('city') ?? $client->city }}" autofocus>
                    @error('city_one')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-group-default">
                    <label for="state"
                           class="col-md-12 col-form-label text-md-left">{{ __('State/Province') }}<span
                            class="text-danger">*</span></label>
                    @include('pages.client.state_partial')
           <!--<input id="state" type="text" class="form-control @error('state') is-invalid @enderror"-->
                    <!--    name="state" value="{{ old('state') ?? $client->state }}" autofocus>-->
                    <!--@error('state')-->
                    <!--    <span class="invalid-feedback" role="alert">-->
                    <!--        <strong>{{ $message }}</strong>-->
                    <!--    </span>-->
                    <!--@enderror-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-group-default">
                    <label for="zip" class="col-md-12 col-form-label text-md-left">{{ __('Zip/ Postal Code') }}<span
                            class="text-danger">*</span></label>
                    <input id="zip" type="number" min="0" class="form-control zip @error('zip') is-invalid @enderror"
                           name="zip" value="{{ old('zip') ?? $client->zip }}" autofocus maxlength="5">
                    @error('zip')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>