@php
$client_label1 = 'First Name';
$client_label2 = 'Last Name';
$client_label3 = 'Phone';
$client_label4 = 'Email';
$client_label5 = 'Status';
$client_label6 = 'Action';
@endphp
<div class="container grid-wrapper p-3 mt-2">
    <div class="grid-header">
        <div class="row d-none d-md-flex mb-md-2 p-md-1">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label1 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label2 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label3 }}</h4>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label4 }}</h4>
            </div>
            {{--
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label5 }}</h4>
            </div>
            --}}
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label6 }}</h4>
            </div>
        </div>
    </div>
    <div class="grid-body">
        @foreach ($clients as $client)
            @php
                $active = '<i style="" class="rounded-circle bg-primary" data-toggle="tooltip" data-placement="top" title=""></i>';
                $inactive = '<i style="" class="rounded-circle bg-danger" data-toggle="tooltip" data-placement="top" title=""></i>';
            @endphp
            <div class="row mb-md-0 p-md-1 mb-3 p-1">
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label1 }}</h4>
                    <p class="m-0 p-1">{{ $client->first_name }}</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label2 }}</h4>
                    <p class="m-0 p-1">{{ $client->last_name }}</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label3 }}</h4>
                    <p class="m-0 p-1">{{ $client->phone_one }}</p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label4 }}</h4>
                    <p class="m-0 p-1">{{ $client->email }}</p>
                </div>
                {{--
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label5 }}</h4>
                    <p class="m-0 p-1">
                        @if ($client->is_active == 1)
                            @php echo $active @endphp
                        @else
                            @php echo $inactive @endphp
                        @endif
                    </p>
                </div>
                --}}
                <div class="col-2 col-sm-2 col-md-2">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label6 }}</h4>
                    <br class="d-none d-md-none d-sm-block " />
                    <a href="{{route('admin.contracts.index',$client->id)}}"
                            class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button">
                            Contracts
                    </a>
                    @can('client-edit')
                        <a href="{{route('admin.clients.edit',$client->id)}}"
                            class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button">
                            Edit</a>
                    @endcan
                
                </div>
            </div> <!-- End of ROW -->
        @endforeach
    </div>
</div>

{{ $clients->links() }}
