@csrf
<div class="card">
    <!-- Start of Extra Information-->
    <h4 class="bold" style="padding:0px 10px;font-size:14px;"><a class="toggler" href="javascript:;"
                                                                 onClick="toggleSearch('client-info', '0',this)">{{ __('Client Information') }} <span
                class="text-danger">*</span><span class="pull-right"><i class="fa fa-chevron-up"></i></span></a></h4>

    <div class="card-body" style="display:block"
         id="client-info">
                @include('pages.client.partials.client_information')

    </div>
</div>


<div class="card">
    <!-- Start of Extra Information-->
    <h4 class="bold" style="padding:0px 10px;font-size:14px;"><a class="toggler" href="javascript:;"
                                                                 onClick="toggleSearch('detailfields', '0',this)">{{ __('Property Information') }} <span
                class="text-danger">*</span><span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>

    <div class="card-body" style="display:none"
         id="detailfields">
        @include('pages.client.partials.property_information')
    </div>
</div>
<!-- End of extra Information -->

<div class="card">
    <!-- Start of Extra Information-->
    <h4 class="bold" style="padding:0px 10px;font-size:14px;">
        <a class="toggler" href="javascript:;"
           onClick="toggleSearch('what-can-we-do', '0',this)">{{ __('What can we do for you') }} <span
                class="text-danger">*</span><span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>

    <div class="card-body" style="display:none" id="what-can-we-do">
        <div class="row clearfix">
            <div class="col-md-12 mb-1">
                <?php
                
                $services=['counter','flooring','cabinet'];
                $client_services=$client->services;
                $client_services_array=[];
//                dump($client->services);
                if($client->services->count()){
                    $client_services_array=$client->services->pluck('service')->toArray();
                }
                
                ?>
                @foreach($services as $ser)
                <label>
                    <input
                         @if(in_array($ser,$client_services_array)) 
                         checked 
                         @endif
                        type="checkbox" value="<?=$ser?>" name="services[]">
                    <?=$ser?>
                </label>
                @endforeach
               
            </div>
            <div class="col-md-12">
                <div class="form-group form-group-default">
                    <label for="description" class="col-md-12 col-form-label text-md-left">{{ __('Notes') }}</label>
                    <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror"
                              name="description" autocomplete="description" autofocus>{{ old('description') ?? $client->description }}
                    </textarea>
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

        </div>

    </div>
</div>
<!-- End of extra Information -->



@isset($client->is_active)
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required">
            <label for="is_active" class="col-md-12 col-form-label text-md-left">{{ __('Status') }}</label>
            <select name="is_active" class="full-width" data-init-plugin="select2">
                <option value="">Select Status</option>
                <option value="1" @if ($client->is_active == 1) {{ 'selected' }} @endif>Active</option>
                <option value="0" @if ($client->is_active == 0) {{ 'selected' }} @endif>Inactive
            </option>
        </select>
        @error('is_active')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
</div>
@endisset

<button id="save-form" type="submit" class="btn btn-primary float-right">
    @isset($client->id)
    {{ __('Update') }}
    @else
    {{ __('Create') }}
    @endisset
</button>
