<select id="state" class="@error('state') is-invalid @enderror form-control" name="state">
    <option value="">Select State</option>
    <option value="AL" @if ($client->state == "AL") {{ 'selected' }} @endif>Alabama</option>
	<option value="AK" @if ($client->state == "AK") {{ 'selected' }} @endif>Alaska</option>
	<option value="AZ" @if ($client->state == "AZ") {{ 'selected' }} @endif>Arizona</option>
	<option value="AR" @if ($client->state == "AR") {{ 'selected' }} @endif>Arkansas</option>
	<option value="CA" @if ($client->state == "CA") {{ 'selected' }} @endif>California</option>
	<option value="CO" @if ($client->state == "CO") {{ 'selected' }} @endif>Colorado</option>
	<option value="CT" @if ($client->state == "CT") {{ 'selected' }} @endif>Connecticut</option>
	<option value="DE" @if ($client->state == "DE") {{ 'selected' }} @endif>Delaware</option>
	<option value="DC" @if ($client->state == "DC") {{ 'selected' }} @endif>District Of Columbia</option>
	<option value="FL" @if ($client->state == "FL") {{ 'selected' }} @endif>Florida</option>
	<option value="GA" @if ($client->state == "GA") {{ 'selected' }} @endif>Georgia</option>
	<option value="HI" @if ($client->state == "HI") {{ 'selected' }} @endif>Hawaii</option>
	<option value="ID" @if ($client->state == "ID") {{ 'selected' }} @endif>Idaho</option>
	<option value="IL" @if ($client->state == "IL") {{ 'selected' }} @endif>Illinois</option>
	<option value="IN" @if ($client->state == "IN") {{ 'selected' }} @endif>Indiana</option>
	<option value="IA" @if ($client->state == "IA") {{ 'selected' }} @endif>Iowa</option>
	<option value="KS" @if ($client->state == "KS") {{ 'selected' }} @endif>Kansas</option>
	<option value="KY" @if ($client->state == "KY") {{ 'selected' }} @endif>Kentucky</option>
	<option value="LA" @if ($client->state == "LA") {{ 'selected' }} @endif>Louisiana</option>
	<option value="ME" @if ($client->state == "ME") {{ 'selected' }} @endif>Maine</option>
	<option value="MD" @if ($client->state == "MD") {{ 'selected' }} @endif>Maryland</option>
	<option value="MA" @if ($client->state == "MA") {{ 'selected' }} @endif>Massachusetts</option>
	<option value="MI" @if ($client->state == "MI") {{ 'selected' }} @endif>Michigan</option>
	<option value="MN" @if ($client->state == "MN") {{ 'selected' }} @endif>Minnesota</option>
	<option value="MS" @if ($client->state == "MS") {{ 'selected' }} @endif>Mississippi</option>
	<option value="MO" @if ($client->state == "MO") {{ 'selected' }} @endif>Missouri</option>
	<option value="MT" @if ($client->state == "MT") {{ 'selected' }} @endif>Montana</option>
	<option value="NE" @if ($client->state == "NE") {{ 'selected' }} @endif>Nebraska</option>
	<option value="NV" @if ($client->state == "NV") {{ 'selected' }} @endif>Nevada</option>
	<option value="NH" @if ($client->state == "NH") {{ 'selected' }} @endif>New Hampshire</option>
	<option value="NJ" @if ($client->state == "NJ") {{ 'selected' }} @endif>New Jersey</option>
	<option value="NM" @if ($client->state == "NM") {{ 'selected' }} @endif>New Mexico</option>
	<option value="NY" @if ($client->state == "NY") {{ 'selected' }} @endif>New York</option>
	<option value="NC" @if ($client->state == "NC") {{ 'selected' }} @endif>North Carolina</option>
	<option value="ND" @if ($client->state == "ND") {{ 'selected' }} @endif>North Dakota</option>
	<option value="OH" @if ($client->state == "OH") {{ 'selected' }} @endif>Ohio</option>
	<option value="OK" @if ($client->state == "OK") {{ 'selected' }} @endif>Oklahoma</option>
	<option value="OR" @if ($client->state == "OR") {{ 'selected' }} @endif>Oregon</option>
	<option value="PA" @if ($client->state == "PA") {{ 'selected' }} @endif>Pennsylvania</option>
	<option value="RI" @if ($client->state == "RI") {{ 'selected' }} @endif>Rhode Island</option>
	<option value="SC" @if ($client->state == "SC") {{ 'selected' }} @endif>South Carolina</option>
	<option value="SD" @if ($client->state == "SD") {{ 'selected' }} @endif>South Dakota</option>
	<option value="TN" @if ($client->state == "TN") {{ 'selected' }} @endif>Tennessee</option>
	<option value="TX" @if ($client->state == "TX") {{ 'selected' }} @endif>Texas</option>
	<option value="UT" @if ($client->state == "UT") {{ 'selected' }} @endif>Utah</option>
	<option value="VT" @if ($client->state == "VT") {{ 'selected' }} @endif>Vermont</option>
	<option value="VA" @if ($client->state == "VA") {{ 'selected' }} @endif>Virginia</option>
	<option value="WA" @if ($client->state == "WA") {{ 'selected' }} @endif>Washington</option>
	<option value="WV" @if ($client->state == "WV") {{ 'selected' }} @endif>West Virginia</option>
	<option value="WI" @if ($client->state == "WI") {{ 'selected' }} @endif>Wisconsin</option>
	<option value="WY" @if ($client->state == "WY") {{ 'selected' }} @endif>Wyoming</option>
</select>
@error('state')
<span class="invalid-feedback" role="alert">
    <strong>{{ $message }}</strong>
</span>
@enderror