<div class="clearfix"></div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <h4 class="bold" style="padding:0px 10px;font-size:18px;"><a class="toggler" href="#"
                    onClick="toggleSearch('searchfields')">{{ __('Advanced Search') }}<span class="pull-right"><i
                            class="fa fa-chevron-down"></i></span></a></h4>
            <?php
            $display = 'display:none;';
            ?>
            <div class="card-body" style="{{ $display }}" id="searchfields">
                <form method="Post" id="searchform" accept-charset="utf-8"
                    action="{{ route('admin.client.index') }}">
                    @csrf
                    <div class="row">
                        <input type="hidden" name="sort_field" class="sort_field" value="">
                        <input type="hidden" name="sort_order" class="sort_order" value="">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <input autofocus="" type="text" name="first_name" value="" class=" searchfield form-control"
                                placeholder="First Name" id="first_name">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <input autofocus="" type="text" name="last_name" value="" class=" searchfield form-control"
                                placeholder="Last Name" id="last_name">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <input autofocus="" type="text" name="email" value="" class=" searchfield form-control"
                                placeholder="email" id="email">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <select name="is_active" class="form-control">
                                <option value="">Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" name="reset" class="btn btn-primary" style="margin-top:25px;"
                                value="Reset" onClick="resetForm('searchform')">
                            <input type="submit" name="search" class="btn btn-primary" style="margin-top:25px;"
                                value="Search" id="searchBnt">
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
