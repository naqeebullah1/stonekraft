@extends('master')
@section('title','Client')
@section('content')
<div class="container-fluid">
        @if (Session::has('message'))
            <div class="row">
                <div class="container text-center">
                    <div class="alert alert-success message">
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>
        @endif
    <h1 class="float-left">Contracts/Clients List</h1>
    @include('pages.client.client_search_form')
    <!--
    <div class="pull-left">
    
		<i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-primary ml-2 mr-2" data-original-title="Active"></i>
		<b>Active </b>
		
		<i style="padding: 1px 9px;" class="rounded-circle bg-danger ml-2 mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
		<b>Inactive</b>
	</div>
    -->
    @can('client-create')
    <a href="{{ route('admin.clients.create') }}" class="btn btn-primary float-right" role="button">Create Client</a>
    @endcan	
    <div class="clearfix"></div>
</div>
@include('pages.client.list')    

@include('partials.loadmorejs')
<script>
    $(document).ready(function() {
        setTimeout(() => {
            $('.message').fadeOut('slow');
        }, 3000);
    });
</script>
@endsection
