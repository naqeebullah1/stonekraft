@extends('master')
@section('title', 'Client Property Edit')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <h4 class="semi-bold text-center light-heading">{{ __('Edit Client Property') }}</h4>

                    <div class="card-body">
                        <form id="client-property-form" method="POST"
                            action="{{ route('admin.client_properties.update', $clientProperty->id) }}">
                            @method('PATCH')
                            @include('pages.client_property.form')
                        </form>
                        @if ($clientProperty->id)
                            @can('client-property-delete')
                                <button class="btn btn-danger float-left"
                                    onClick="event.preventDefault();deleteConfirm('client_property-delete-form-{{ $clientProperty->id }}', 'You are about to delete a client property, Are you sure?')">
                                Delete
                                </button>
                                <form class="client_property-form" id="client_property-delete-form-{{ $clientProperty->id }}"
                                    style="display:none;"
                                    action="{{ route('admin.client_properties.destroy', $clientProperty->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endcan
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document.body).on('click', '#save-form', function() {

            $('#client-property-form').validate({
                rules: {
                    property_id: {
                        required: true,
                    },
                    client_id: {
                        required: true,
                    },
                    is_active: {
                        required: true,
                    },

                },
                message: {

                },
                submitHandler: function(form) {
                    $("#submitForm").html("Please wait...").attr('disabled', true);
                    form.submit();
                }

            });

        });
    </script>
@endsection
