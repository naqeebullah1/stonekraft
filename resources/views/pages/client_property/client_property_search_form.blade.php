<div class="clearfix"></div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <h4 class="bold" style="padding:0px 10px;font-size:18px;"><a class="toggler" href="#" onClick="toggleSearch('searchfields')">{{ __('Advanced Search') }}<span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
            <?php
            $display = 'display:none;';
            if(request()->all()):
            $display = 'display:block;';
                
            endif;
            ?>
            <div class="card-body" style="{{ $display }}" id="searchfields">
                <form method="Post" id="searchform" accept-charset="utf-8" action="{{ route('admin.client_properties.index') }}">
                    @csrf
                    <div class="row">
                        <input type="hidden" name="sort_field" class="sort_field" value="">
                        <input type="hidden" name="sort_order" class="sort_order" value="">

                        <div class="col-lg col-sm-12 col-xs-12">
                            <input name="first_name" value="{{request()->first_name}}"placeholder="Client First Name" class="form-control" >

                        </div>
                        <div class="col-lg  col-sm-12 col-xs-12">
                            
                            <input name="last_name" value="{{request()->last_name}}" placeholder="Client Last Name" class="form-control" >

                        </div>
                        <div class="col-lg col-md-3 col-sm-12 col-xs-12">
                            <input name="property_name" value="{{ request()->property_name }}" class="form-control" placeholder="Property Name" >
                        </div>
                        <div class="col-lg col-md-2 col-sm-12 col-xs-12">
                            <select name="is_active" class="form-control" >
                                <option value="">Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" name="reset" class="btn btn-primary" style="margin-top:25px;" value="Reset" onClick="resetForm('searchform')">
                            <input type="submit" name="search" class="btn btn-primary" style="margin-top:25px;" value="Search" id="searchBnt">
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
