@extends('master')
@section('title', 'Client Property Create')
@section('content')
    <div class="container">
        @if (Session::has('message'))
            <div class="row">
                <div class="container text-center">
                    <div class="alert alert-danger">
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h4 class="semi-bold text-center light-heading">{{ __('Create Client Property') }}</h4>

                    <div class="card-body">
                        <form method="POST" id="client-property-form" action="{{ route('admin.client_properties.store') }}">
                            @include('pages.client_property.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document.body).on('click', '#save-form', function() {

            $('#client-property-form').validate({
                rules: {
                    property_id: {
                        required: true,
                    },
                    client_id: {
                        required: true,
                    },
                    is_active: {
                        required: true,
                    },

                },
                message: {

                },
                submitHandler: function(form) {
                    $("#submitForm").html("Please wait...").attr('disabled', true);
                    form.submit();
                }

            });

        });
    </script>
@endsection
