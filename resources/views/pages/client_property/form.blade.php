@csrf
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="property_id" class="col-md-12 col-form-label text-md-left">{{ __('Property') }}</label>
            <select id="property_id" class="form-control @error('property_id') is-invalid @enderror" name="property_id"
                value="{{ old('property_id') }}" required autocomplete="property_id" autofocus>
                <option value="">Select Property</option>
                @foreach ($properties as $property)
                    <option value="{{ $property->id }}" @if ($property->id == $clientProperty->property_id) {{ 'selected' }} @endif>
                        {{ $property->property_name }}</option>
                @endforeach
            </select>
            @error('property_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="client_id" class="col-md-12 col-form-label text-md-left">{{ __('Client') }}</label>
            <select id="client_id" class="form-control @error('client_id') is-invalid @enderror" name="client_id"
                value="{{ old('client_id') }}" required autocomplete="client_id" autofocus>
                <option value="">Select Client</option>
                @foreach ($clients as $client)
                    <option value="{{ $client->id }}" @if ($client->id == $clientProperty->client_id) {{ 'selected' }} @endif>
                        {{ $client->first_name.' '.$client->last_name.' '.$client->email }}</option>
                @endforeach
            </select>
            @error('client_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required">
            <label for="is_active" class="col-md-12 col-form-label text-md-left">{{ __('Status') }}</label>
            <select name="is_active" class="full-width" data-init-plugin="select2">
                <option value="">Select Status</option>
                <option value="1" @if ($clientProperty->is_active == 1) {{ 'selected' }} @endif>Active</option>
                <option value="0" @if ($clientProperty->is_active == 0) {{ 'selected' }} @endif>Inactive</option>
            </select>
            @error('is_active')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>
<button id="save-form" class="btn btn-primary float-right">
    @isset($clientProperty->id)
        {{ __('Update') }}
    @else
        {{ __('Create') }}
    @endisset
</button>
