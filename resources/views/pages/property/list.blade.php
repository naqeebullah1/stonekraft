@php
$property_label1 = 'Property Name';
$property_label2 = 'Note';
$property_label3 = 'Status';
$property_label4 = 'Action';
@endphp
<div class="container grid-wrapper p-3 mt-2">
    <div class="grid-header">
        <div class="row d-none d-md-flex mb-md-2 p-md-1">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $property_label1 }}</h4>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $property_label2 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $property_label3 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $property_label4 }}</h4>
            </div>
        </div>
    </div>
    <div class="grid-body">
        @foreach ($properties as $property)
            @php
                $active = '<i style="" class="rounded-circle bg-primary" data-toggle="tooltip" data-placement="top" title=""></i>';
                $inactive = '<i style="" class="rounded-circle bg-danger" data-toggle="tooltip" data-placement="top" title=""></i>';
            @endphp
            <div class="row mb-md-0 p-md-1 mb-3 p-1">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $property_label1 }}</h4>
                    <p class="m-0 p-1">{{ $property->property_name }}</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $property_label2 }}</h4>
                    <p class="m-0 p-1">
                        <?php $small = substr($property->description, 0, 100);
                    ?>
                    <td class="text-left">
                        {{$small}}
                        @if(strlen($property->description)>100)
                                                <a href="javascript:" class="btn-link" data-toggle="popover" data-trigger="hover" data-content="<?= $property->description ?>">...</a>

                        @endif
                        
                        </p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $property_label3 }}</h4>
                    <p class="m-0 p-1">
                        @if ($property->is_active == 1)
                            @php echo $active @endphp
                        @else
                            @php echo $inactive @endphp
                        @endif
                    </p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $property_label4 }}</h4>
                    <br class="d-none d-md-none d-sm-block " />
                    @can('property-edit')
                        <a href="{{route('admin.properties.edit',$property->id)}}"
                            class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button">Edit</a>
                    @endcan
                  
                </div>
            </div> <!-- End of ROW -->
        @endforeach
    </div>
</div>

{{ $properties->links() }}
