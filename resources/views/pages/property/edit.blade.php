@extends('master')
@section('title', 'Property Edit')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h4 class="semi-bold text-center light-heading">{{ __('Edit Property') }}</h4>
                    <div class="card-body">
                        <form id="property-form" enctype="multipart/form-data" method="POST" action="{{ route('admin.properties.update', $property->id) }}">
                            @method('PATCH')
                            @include('pages.property.form')
                        </form>
                        @can('property-delete')
                            @if ($property->id)
                                <button class="btn btn-danger float-left"
                                        onClick="event.preventDefault();deleteConfirm('property-delete-form-{{ $property->id }}', 'You are about to delete a property, Are you sure?')">
                                            Delete
                                </button>
                                <form class="property-form" id="property-delete-form-{{ $property->id }}" style="display:none;"
                                    action="{{ route('admin.properties.destroy', $property->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endif
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document.body).on('click', '#save-form', function() {
            $('#property-form').validate({
                rules: {
                    property_name: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 45,
                        minlength: 2

                    },
                    address_one: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    address_two: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    alarm_code: {
                        alphanumeric: true,
                        required: true,
                        minlength: 2
                    },
                    access_information: {
                        alphanumeric: true,
                        required: true,
                        minlength: 2
                    },
                    address_two: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 120,
                        minlength: 5
                    },
                    city: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 20,
                        minlength: 2
                    },
                    state: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 20,
                        minlength: 2
                    },
                    zip: {
                        alphanumeric: true,
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    },
                },
                message: {
                    property_name: {
                        alphanumeric: "please enter characters only",
                        required: "Enter your property name, please.",
                        maxlength: "property Name too long.",
                        minlength: "Min 2 Char"
                    },
                },
                submitHandler: function(form) {
                    $("#submitForm").html("Please wait...").attr('disabled', true);
                    form.submit();
                }

            });

        });
        $(document).ready(function() {
            $('.hide_unhide').click(function() {
                if ($('#alarm_code').attr('type') === 'password') {
                    $('#alarm_code').attr('type', 'text');
                } else {
                    $('#alarm_code').attr('type', 'password');
                }
            });

            $('.gate_code').click(function() {
                if ($('#gate_code').attr('type') === 'password') {
                    $('#gate_code').attr('type', 'text');
                } else {
                     $('#gate_code').attr('type', 'password');
                }
            });
            
            $('.garage_code').click(function() {
                if ($('#garage_code').attr('type') === 'password') {
                    $('#garage_code').attr('type', 'text');
                } else {
                     $('#garage_code').attr('type', 'password');
                }
            });
        });
        function deleteFile(url) {

            $.confirm({
                theme: 'bootstrap',
                title: 'Confirm!',
                content: "You are about to delete a file, Are you sure?",
                buttons: {
                    Confirm: function() {
                        $.ajax({
                        url: url,
                        type: "GET",
                        success: function(data) {
                                        $('.dropify-clear').trigger("click");
                                        $('.contract-controls').addClass('disabled');
                                        
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                    },
                    Cancel: function() {
//                        return false;
                    }
                }
            });
        }
    </script>
@endsection
