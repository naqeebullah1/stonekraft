@csrf
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="property_name" class="col-md-12 col-form-label text-md-left">{{ __('Property Name') }}</label>
            <input id="property_name" type="text" class="form-control @error('property_name') is-invalid @enderror"
                   name="property_name" value="{{ old('property_name') ?? $property->property_name }}" required
                   autocomplete="off" autofocus>
            @error('property_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

    </div>
</div>
{{-- <div class="card">
    <!-- Start of Extra Information-->
    <h4 class="bold" style="padding:0px 10px;font-size:14px;"><a class="toggler" href="#"
            onClick="toggleSearch('detailfields','0')">{{ __('Extra Information') }}<span class="pull-right"><i
                                        class="fa fa-chevron-down"></i></span></a></h4>
                        <div class="card-body" style="display:none" id="detailfields">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label for="address_one"
                                               class="col-md-12 col-form-label text-md-left">{{ __('Address One') }}</label>
                                        <input id="address_one" type="text" class="form-control @error('address_one') is-invalid @enderror"
                                               name="address_one" value="{{ old('address_one') ?? $property->address_one }}" autofocus>
                                        @error('address_one')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label for="address_two"
                                               class="col-md-12 col-form-label text-md-left">{{ __('Address Two') }}</label>
                                        <input id="address_two" type="text" class="form-control @error('address_two') is-invalid @enderror"
                                               name="address_two" value="{{ old('address_two') ?? $property->address_two }}" autofocus>
                                        @error('address_two')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label for="city" class="col-md-12 col-form-label text-md-left">{{ __('City') }}</label>
                                        <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city"
                                               value="{{ old('city') ?? $property->city }}" autofocus>
                                        @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label for="state"
                                               class="col-md-12 col-form-label text-md-left">{{ __('State/Province') }}</label>
                                        <input id="state" type="text" class="form-control @error('state') is-invalid @enderror" name="state"
                                               value="{{ old('state') ?? $property->state }}" autofocus>
                                        @error('state')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label for="zip"
                                               class="col-md-12 col-form-label text-md-left">{{ __('Zip/Postal Code') }}</label>
                                        <input id="zip" type="text" class="form-control @error('zip') is-invalid @enderror" name="zip"
                                               value="{{ old('zip') ?? $property->state }}" autofocus>
                                        @error('zip')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div><!-- End of extra Information --> --}}
                    <div class="row clearfix">
                        <div class="col-md-3">
                            <div class="form-group form-group-default" aria-required="true">
                                <label for="alarm_code" class="col-md-12 col-form-label text-md-left">{{ __('Alarm Code') }}</label>
                                <input type="password" id="alarm_code" class="form-control @error('alarm_code') is-invalid @enderror"
                                       name="alarm_code" value="{{ old('alarm_code') ?? $property->alarm_code }}"
                                       autocomplete="alarm_code" autofocus>
                                @error('alarm_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <span class="fa fa-fw fa-eye hide_unhide" style="position: relative; bottom:45px;left:135px;"></span>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default" aria-required="true">
                                <label for="gate_code" class="col-md-12 col-form-label text-md-left">{{ __('Gate Code') }}</label>
                                <input type="password" id="gate_code" class="form-control @error('gate_code') is-invalid @enderror"
                                       name="gate_code" value="{{ old('gate_code') ?? $property->gate_code }}" 
                                       autocomplete="gate_code" autofocus>
                                @error('gate_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <span class="fa fa-fw fa-eye gate_code" style="position: relative; bottom:45px;left:135px;"></span>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default" aria-required="true">
                                <label for="garage_code" class="col-md-12 col-form-label text-md-left">{{ __('Garage Code') }}</label>
                                <input type="password" id="garage_code" class="form-control @error('garage_code') is-invalid @enderror"
                                       name="garage_code" value="{{ old('garage_code') ?? $property->garage_code }}"
                                       autocomplete="garage_code" autofocus>
                                @error('garage_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <span class="fa fa-fw fa-eye garage_code" style="position: relative; bottom:45px;left:135px;"></span>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-default required">
                                <label for="key" class="col-md-12 col-form-label text-md-left">{{ __('Do you have key?') }}</label>
                                <select name="key" class="full-width" data-init-plugin="select2">
                                    <option value="">Select Yes/No</option>
                                    <option value="1" @if ($property->key == 1) {{ 'selected' }} @endif>Yes</option>
                                    <option value="0" @if ($property->key == 0) {{ 'selected' }} @endif>No</option>
                                </select>
                                @error('key')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-group-default required" aria-required="true">
                                <label for="access_information"
                                       class="col-md-12 col-form-label text-md-left">{{ __('Access Information') }}</label>
                                <input id="access_information" type="text"
                                       class="form-control @error('access_information') is-invalid @enderror" name="access_information"
                                       value="{{ old('access_information') ?? $property->access_information }}"
                                       autocomplete="access_information" autofocus>
                                @error('access_information')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-group-default">
                                <label for="description" class="col-md-12 col-form-label text-md-left">{{ __('Notes') }}</label>
                                <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror"
                                          name="description" autocomplete="description" autofocus>{{ old('description') ?? $property->description }}</textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="form-group p-2" style="background-color:#f8f8f8 !important; border: 1px solid #ddd;">
                                <label style="text-transform: unset !important;" for="contract_file" class="d-flex col-md-12 col-form-label text-md-left mb-2">
                                    <span class="align-self-center">
                                        {{ __('Contract File') }}

                                    </span>
                                    <div class="flex-grow-1 text-right">
                                        <?php
                                        $disableBtns='disabled';
                                       
                                        ?>
                                    @if($property->contract_file && file_exists(public_path() . DS() .'uploads'.DS().'property_contracts'.DS() . $property->contract_file))
                                        <?php
                                        $disableBtns='';
                                        ?>

                                    @endif
                                        <a class="contract-controls btn btn-complete btn-sm btn-small {{$disableBtns}}" href="{{ asset('uploads/property_contracts/'.$property->contract_file) }}" target="_blank">Open</a>
                                        <a class="contract-controls btn btn-complete btn-sm btn-small {{$disableBtns}}" href="{{ asset('uploads/property_contracts/'.$property->contract_file) }}" download="">Download</a>
                                        <a onclick="deleteFile('{{url('backend/delete-contract-file/'.$property->id)}}')" class="contract-controls btn btn-danger btn-sm btn-small {{$disableBtns}}" href="#">Delete</a>

                                    </div>
                                </label>
                                <input type="file" class="dropify"
                                       @if($property->contract_file && file_exists(public_path() . DS().'uploads'.DS().'property_contracts'.DS() . $property->contract_file))
                                       data-default-file='{{ asset('uploads/property_contracts/'.$property->contract_file) }}'
                                       @endif
                                       name="contract_file">
                                       
                                       <p style="color: #008b8b;font-size:13px;" class="mb-0 mt-1 bold">Please move your cursor to see file name.</p>
                            </div>
                        </div>
                    </div>
                    @isset($property->is_active)
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-group-default required">
                                <label for="is_active" class="col-md-12 col-form-label text-md-left">{{ __('Status') }}</label>
                                <select name="is_active" class="full-width" data-init-plugin="select2">
                                    <option value="">Select Status</option>
                                    <option value="1" @if ($property->is_active == 1) {{ 'selected' }} @endif>Active</option>
                                    <option value="0" @if ($property->is_active == 0) {{ 'selected' }} @endif>Inactive</option>
                                </select>
                                @error('is_active')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    @endisset
                    <button id="save-form" type="submit" class="btn btn-primary float-right">
                        @isset($property->id)
                        {{ __('Update') }}
                        @else
                        {{ __('Create') }}
                        @endisset
                    </button>
