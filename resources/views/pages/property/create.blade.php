@extends('master')
@section('title', 'Property Create')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h4 class="semi-bold text-center light-heading">{{ __('Create Property') }}</h4>

                <div class="card-body">
                    <form autocomplete="off" enctype="multipart/form-data" method="POST" id="property-form" action="{{ route('admin.properties.store') }}">
                        @include('pages.property.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document.body).on('click', '#save-form', function () {
        $('#property-form').validate({
            rules: {
                property_name: {
                    alphanumeric: true,
                    required: true,
                    maxlength: 45,
                    minlength: 2

                }
            },
            message: {
                property_name: {
                    alphanumeric: "please enter characters only",
                    required: "Enter your property name, please.",
                    maxlength: "property Name too long.",
                    minlength: "Min 2 Char"
                },
            },
            submitHandler: function (form) {
                $("#submitForm").html("Please wait...").attr('disabled', true);
                form.submit();
            }

        });

    });

    $(document).ready(function () {
        $('.hide_unhide').click(function () {
            if ($('#alarm_code').attr('type') === 'password') {
                $('#alarm_code').attr('type', 'text');
            } else {
                $('#alarm_code').attr('type', 'password');
            }
        })
        $('.gate_code').click(function () {
            if ($('#gate_code').attr('type') === 'password') {
                $('#gate_code').attr('type', 'text');
            } else {
                $('#gate_code').attr('type', 'password');
            }
        })
        $('.garage_code').click(function () {
            if ($('#garage_code').attr('type') === 'password') {
                $('#garage_code').attr('type', 'text');
            } else {
                $('#garage_code').attr('type', 'password');
            }
        });
        setTimeout(removeChromeValues, 1000);
    });

    function removeChromeValues() {
        $('#property_name').val('');
        $('#alarm_code').val('');
    }
</script>
@endsection
