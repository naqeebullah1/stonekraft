@extends('master')
@section('title','Property')
@section('content')
<div>
        @if (Session::has('message'))
            <div class="row">
                <div class="container text-center">
                    <div class="alert alert-success message">
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>
        @endif
    <h1 class="float-left">Property List</h1>
    @include('pages.property.property_search_form')
    <div class="pull-left">
    
		<i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-primary ml-2 mr-2" data-original-title="Active"></i>
		<b>Active </b>
		
		 <i style="padding:1px 9px;" class="rounded-circle bg-danger ml-2 mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
		 <b>Inactive </b>
	</div>
    @can('property-create')
    <a href="{{ route('admin.properties.create') }}" class="btn btn-primary float-right" role="button">Create Property</a>
    @endcan	
    <div class="clearfix"></div>
</div>
@include('pages.property.list')    

@include('partials.loadmorejs')
<script>
    $(document).ready(function() {
        setTimeout(() => {
            $('.message').fadeOut('slow');
        }, 3000);
    });
</script>
@endsection
