<div class="header p-r-0 bg-primary">
    <div class="header-inner header-md-height">
        <a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-menu text-white" data-toggle="horizontal-menu"></a>
        <div class="">
            <div class="brand inline no-border d-sm-inline-block">
                
                    <img style="width: 175px;" src="{{ asset('logo.png') }}" alt="{{ config('app.name') }}"
                    data-src="{{ asset('logo.png') }}" data-src-retina="{{ asset('logo.png') }}"
                    class="img-fluid">                    
                
                  
            </div>
        </div>

        <div class="d-flex align-items-right">
            <h6 class="text-white text-left"><i class="fa fa-phone"></i> 813-628 8453</h6> &nbsp;&nbsp;
            <h6 class="text-white text-left"><i class="fa fa-envelope"></i> sam@stone-kraft.com</h6>&nbsp;&nbsp;
            <h6 class="text-white text-left"><i class="fa fa-map-pin"></i> 2402 S 50th St, Tampa , FL 33619</h6>
        </div>
        <div class="d-flex align-items-center">
            <!-- START User Info-->

            <div class="dropdown pull-right">
                <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="pull-left p-r-10 fs-14 font-heading d-lg-inline-block text-white">
                        <span class="semi-bold">Logged in as {{ Auth::user()->first_name }}</span>
                    </div>
                </button>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                    <!-- <a href="{{ url('/backend/user-profile') }}" class="dropdown-item"><i class="fa fa-user"></i> Profile</a> -->
                    <a href="{{ url('/') }}/backend/change-password" class="dropdown-item"><i
                            class="pg-settings_small"></i> Change Password</a>
                    <a href="javascript:"
                        onClick="event.preventDefault();document.getElementById('logout-form').submit()"
                        class="clearfix bg-master-lighter dropdown-item">
                        <form action="{{ route('admin.logout') }}" method="POST" style="display:none;" id="logout-form">
                            @csrf
                            @method('POST')
                        </form>
                        <span class="pull-left pr-2"><i class="pg-power"></i></span>
                        <span class="pull-left">Logout</span>
                        
                    </a>
                </div>
            </div>
            <!-- END User Info-->
        </div>
    </div>
    @auth
        <?php
        ?>
        <div class="bg-white">
            <div class="container">
                <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="">
                    <a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-close" data-toggle="horizontal-menu">
                    </a>
                    <ul>
                        @if(count(Auth::user()->roles)==1 && in_array(14,Auth::user()->roles->pluck('id')->toArray()))
                        @else
                        <li class="@if(in_array('dashboard',Request::segments())) {{'active'}} @endif">
                            <a href="{{url('/')}}/backend/dashboard">Dashboard</a>
                        </li>
                        @endif
                        @canany(['user-list', 'role-list'])
                            <li class="@if (in_array('users', Request::segments()) || in_array('users-list', Request::segments()) || in_array('roles', Request::segments())) {{ 'active' }} @endif">
                                <a class="nav-link dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false" href="javascript:">Users</a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                                    @can('user-list')
                                        <a class="nav-link" href="{{ route('admin.users.index') }}">Manage Users</a>
                                    @endcan
                                    @can('role-list')
                                        <a class="nav-link" href="{{ route('admin.roles.index') }}">Manage Roles</a>
                                    @endcan
                                </div>
                            </li>
                        @endcanany
                        <li class="@if(in_array('lead-list',Request::segments())) {{'active'}} @endif">
                            @can('lead-list')
                                <a href="{{ route('admin.lead.index') }}">Leads</a>
                            @endcan
                        </li>
                        <li class="@if(in_array('clients',Request::segments())) {{'active'}} @endif">
                            @can('client-list')
                                <a href="{{ route('admin.contracts.allcontracts') }}">Contracts</a>
                            @endcan
                        </li>
               

                        <li class="@if(in_array('task-list',Request::segments())) {{'active'}} @endif">
                            @can('workOrder-list')
                                <a href="{{ route('admin.task.index') }}">Work Order</a>
                            @endcan
                        </li>
<!--                        <li class="@if(in_array('contracts',Request::segments())) {{'active'}} @endif">
                                <a href="{{ route('admin.contracts.index') }}">Contracts</a>
                        </li>-->
                        <li class="@if(in_array('items',Request::segments())) {{'active'}} @endif">
                            @can('task-type-list')
                                <a href="{{ route('admin.items.index') }}">Item Types</a>
                            @endcan
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    @endauth
</div>

<div class="page-container ">
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
            <!-- START BREADCRUMBS -->
            <div class="bg-white">
                <div class="container">

                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item"><a href="{{url('/')}}/backend/dashboard">Home</a></li>
                           
                        @if(isset($breadcrumbs))
                        @foreach($breadcrumbs as $breadcrumb)
                         <li class="breadcrumb-item"><a href="<?= $breadcrumb['url'] ?>"><?= $breadcrumb['title'] ?></a> </li>
                        @endforeach
                        @else
                            <?php
                        $link = URL::to('/');
                        ?>
                        @for($i = 1; $i <= count(Request::segments()); $i++)
                        @if($i < count(Request::segments()) & $i > 0)
                        <?php
                        if (Request::segment($i) == 'edit') {
                            continue;
                        }
                        if (is_numeric(Request::segment($i))) {
                            continue;
                        }
                        $link .= "/" . Request::segment($i);
                        if ($link != URL::to('/') . "/backend") {
                            if ($i != 3 && is_int(intval(ucwords(str_replace('-', ' ', Request::segment($i)))))) {
                                $s2 = str_replace('-', ' ', Request::segment($i));
                                ?>
                                <li class="breadcrumb-item"><a href="<?= $link ?>">{{ ucwords($s2)}}</a> </li>
                                <?php
                            }
                        }
                        ?>
                        @else  
                        <?php
                        $s2 = str_replace('_', ' ', str_replace('-', ' ', Request::segment($i)));
                        ?>
                        <li class="breadcrumb-item active"> {{  ucwords($s2) }}</li>
                        @endif
                        @endfor  
                        @endif
                    </ol>
                </div>
            </div>
            <!-- END BREADCRUMBS -->
           
                {{-- @include('partials.alerts') --}}
