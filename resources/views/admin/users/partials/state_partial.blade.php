<select id="state" class="@error('state') is-invalid @enderror form-control" name="state">
    <option value="">Select State</option>
    <option value="AL" {{ old('state') == 'AL' ? 'selected' : '' }} @if ($user->state == "AL") {{ 'selected' }} @endif>Alabama</option>
	<option value="AK" {{ old('state') == 'AK' ? 'selected' : '' }} @if ($user->state == "AK") {{ 'selected' }} @endif>Alaska</option>
	<option value="AZ" {{ old('state') == 'AZ' ? 'selected' : '' }} @if ($user->state == "AZ") {{ 'selected' }} @endif>Arizona</option>
	<option value="AR" {{ old('state') == 'AR' ? 'selected' : '' }} @if ($user->state == "AR") {{ 'selected' }} @endif>Arkansas</option>
	<option value="CA" {{ old('state') == 'CA' ? 'selected' : '' }} @if ($user->state == "CA") {{ 'selected' }} @endif>California</option>
	<option value="CO" {{ old('state') == 'CO' ? 'selected' : '' }} @if ($user->state == "CO") {{ 'selected' }} @endif>Colorado</option>
	<option value="CT" {{ old('state') == 'CT' ? 'selected' : '' }} @if ($user->state == "CT") {{ 'selected' }} @endif>Connecticut</option>
	<option value="DE" {{ old('state') == 'DE' ? 'selected' : '' }} @if ($user->state == "DE") {{ 'selected' }} @endif>Delaware</option>
	<option value="DC" {{ old('state') == 'DC' ? 'selected' : '' }} @if ($user->state == "DC") {{ 'selected' }} @endif>District Of Columbia</option>
	<option value="FL" {{ old('state') == 'FL' ? 'selected' : '' }} @if ($user->state == "FL") {{ 'selected' }} @endif>Florida</option>
	<option value="GA" {{ old('state') == 'GA' ? 'selected' : '' }} @if ($user->state == "GA") {{ 'selected' }} @endif>Georgia</option>
	<option value="HI" {{ old('state') == 'HI' ? 'selected' : '' }} @if ($user->state == "HI") {{ 'selected' }} @endif>Hawaii</option>
	<option value="ID" {{ old('state') == 'ID' ? 'selected' : '' }} @if ($user->state == "ID") {{ 'selected' }} @endif>Idaho</option>
	<option value="IL" {{ old('state') == 'IL' ? 'selected' : '' }} @if ($user->state == "IL") {{ 'selected' }} @endif>Illinois</option>
	<option value="IN" {{ old('state') == 'IN' ? 'selected' : '' }} @if ($user->state == "IN") {{ 'selected' }} @endif>Indiana</option>
	<option value="IA" {{ old('state') == 'IA' ? 'selected' : '' }} @if ($user->state == "IA") {{ 'selected' }} @endif>Iowa</option>
	<option value="KS" {{ old('state') == 'KS' ? 'selected' : '' }} @if ($user->state == "KS") {{ 'selected' }} @endif>Kansas</option>
	<option value="KY" {{ old('state') == 'KY' ? 'selected' : '' }} @if ($user->state == "KY") {{ 'selected' }} @endif>Kentucky</option>
	<option value="LA" {{ old('state') == 'LA' ? 'selected' : '' }} @if ($user->state == "LA") {{ 'selected' }} @endif>Louisiana</option>
	<option value="ME" {{ old('state') == 'ME' ? 'selected' : '' }} @if ($user->state == "ME") {{ 'selected' }} @endif>Maine</option>
	<option value="MD" {{ old('state') == 'MD' ? 'selected' : '' }} @if ($user->state == "MD") {{ 'selected' }} @endif>Maryland</option>
	<option value="MA" {{ old('state') == 'MA' ? 'selected' : '' }} @if ($user->state == "MA") {{ 'selected' }} @endif>Massachusetts</option>
	<option value="MI" {{ old('state') == 'MI' ? 'selected' : '' }} @if ($user->state == "MI") {{ 'selected' }} @endif>Michigan</option>
	<option value="MN" {{ old('state') == 'MN' ? 'selected' : '' }} @if ($user->state == "MN") {{ 'selected' }} @endif>Minnesota</option>
	<option value="MS" {{ old('state') == 'MS' ? 'selected' : '' }} @if ($user->state == "MS") {{ 'selected' }} @endif>Mississippi</option>
	<option value="MO" {{ old('state') == 'MO' ? 'selected' : '' }} @if ($user->state == "MO") {{ 'selected' }} @endif>Missouri</option>
	<option value="MT" {{ old('state') == 'MT' ? 'selected' : '' }} @if ($user->state == "MT") {{ 'selected' }} @endif>Montana</option>
	<option value="NE" {{ old('state') == 'NE' ? 'selected' : '' }} @if ($user->state == "NE") {{ 'selected' }} @endif>Nebraska</option>
	<option value="NV" {{ old('state') == 'NV' ? 'selected' : '' }} @if ($user->state == "NV") {{ 'selected' }} @endif>Nevada</option>
	<option value="NH" {{ old('state') == 'NH' ? 'selected' : '' }} @if ($user->state == "NH") {{ 'selected' }} @endif>New Hampshire</option>
	<option value="NJ" {{ old('state') == 'NJ' ? 'selected' : '' }} @if ($user->state == "NJ") {{ 'selected' }} @endif>New Jersey</option>
	<option value="NM" {{ old('state') == 'NM' ? 'selected' : '' }} @if ($user->state == "NM") {{ 'selected' }} @endif>New Mexico</option>
	<option value="NY" {{ old('state') == 'NY' ? 'selected' : '' }} @if ($user->state == "NY") {{ 'selected' }} @endif>New York</option>
	<option value="NC" {{ old('state') == 'NC' ? 'selected' : '' }} @if ($user->state == "NC") {{ 'selected' }} @endif>North Carolina</option>
	<option value="ND" {{ old('state') == 'ND' ? 'selected' : '' }} @if ($user->state == "ND") {{ 'selected' }} @endif>North Dakota</option>
	<option value="OH" {{ old('state') == 'OH' ? 'selected' : '' }} @if ($user->state == "OH") {{ 'selected' }} @endif>Ohio</option>
	<option value="OK" {{ old('state') == 'OK' ? 'selected' : '' }} @if ($user->state == "OK") {{ 'selected' }} @endif>Oklahoma</option>
	<option value="OR" {{ old('state') == 'OR' ? 'selected' : '' }} @if ($user->state == "OR") {{ 'selected' }} @endif>Oregon</option>
	<option value="PA" {{ old('state') == 'PA' ? 'selected' : '' }} @if ($user->state == "PA") {{ 'selected' }} @endif>Pennsylvania</option>
	<option value="RI" {{ old('state') == 'RI' ? 'selected' : '' }} @if ($user->state == "RI") {{ 'selected' }} @endif>Rhode Island</option>
	<option value="SC" {{ old('state') == 'SC' ? 'selected' : '' }} @if ($user->state == "SC") {{ 'selected' }} @endif>South Carolina</option>
	<option value="SD" {{ old('state') == 'SD' ? 'selected' : '' }} @if ($user->state == "SD") {{ 'selected' }} @endif>South Dakota</option>
	<option value="TN" {{ old('state') == 'TN' ? 'selected' : '' }} @if ($user->state == "TN") {{ 'selected' }} @endif>Tennessee</option>
	<option value="TX" {{ old('state') == 'TX' ? 'selected' : '' }} @if ($user->state == "TX") {{ 'selected' }} @endif>Texas</option>
	<option value="UT" {{ old('state') == 'UT' ? 'selected' : '' }} @if ($user->state == "UT") {{ 'selected' }} @endif>Utah</option>
	<option value="VT" {{ old('state') == 'VT' ? 'selected' : '' }} @if ($user->state == "VT") {{ 'selected' }} @endif>Vermont</option>
	<option value="VA" {{ old('state') == 'VA' ? 'selected' : '' }} @if ($user->state == "VA") {{ 'selected' }} @endif>Virginia</option>
	<option value="WA" {{ old('state') == 'WA' ? 'selected' : '' }} @if ($user->state == "WA") {{ 'selected' }} @endif>Washington</option>
	<option value="WV" {{ old('state') == 'WV' ? 'selected' : '' }} @if ($user->state == "WV") {{ 'selected' }} @endif>West Virginia</option>
	<option value="WI" {{ old('state') == 'WI' ? 'selected' : '' }} @if ($user->state == "WI") {{ 'selected' }} @endif>Wisconsin</option>
	<option value="WY" {{ old('state') == 'WY' ? 'selected' : '' }} @if ($user->state == "WY") {{ 'selected' }} @endif>Wyoming</option>
</select>
@error('state')
<span class="invalid-feedback" role="alert">
    <strong>{{ $message }}</strong>
</span>
@enderror