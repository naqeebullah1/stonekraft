@extends('master')
@section('title','Dashboard')
@section('content')
@if (Session::has('success'))
<div class="row">
    <div class="container text-center">
        <div class="alert alert-success message">
            {{ Session::get('success') }}
        </div>
    </div>
</div>
@endif
<div>
    <h1 class="float-left">Users List</h1>
    @include('admin.users.partials.user_search_form')
    <div class="pull-left">
    
		<i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-primary ml-2 mr-2" data-original-title="Active"></i>
		<b>Active </b>
		
		
		
		<i style="padding: 1px 9px;" class="rounded-circle bg-danger ml-2 mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
		<b>Inactive </b>
	</div>
    @can('user-create')
    <a href="{{route('admin.users.create')}}" class="btn btn-primary float-right" role="button">Create User</a>
    @endcan	
    <div class="clearfix"></div>
</div>
@include('admin.users.partials.loop.list')    

@include('partials.loadmorejs')
@endsection
