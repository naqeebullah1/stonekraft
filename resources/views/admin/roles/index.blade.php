@extends('master')
@section('title','Role List')
@section('content')
@if (Session::has('success'))
<div class="row">
    <div class="container text-center">
        <div class="alert alert-success message">
            {{ Session::get('success') }}
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Role Management</h2>
        </div>
        <div class="pull-right pt-3">
        @can('role-create')
            <a class="btn btn-primary" href="{{ route('admin.roles.create') }}"> Create Role</a>
            @endcan
        </div>
    </div>
</div>

@include('admin.roles.partials.loop.list')
 
@include('partials.loadmorejs')
@endsection