@extends('master')
@section('title','Role Create')
@section('content')
<div class="container mb-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h4 class="semi-bold text-center light-heading">{{ __('Edit Role') }}</h4>

                <div class="card-body">

                    <form method="POST" action="{{ route('admin.roles.update',$role->id) }}">
                        @method('PUT')
                        @include('admin.roles.partials.form')
                    </form>
                      @if (strtolower($role->name) != 'admin')
                            @can('role-delete')
                                <button class="btn btn-danger float-left"
                                    onClick="event.preventDefault();deleteConfirm('role-delete-form-{{ $role->id }}','Users having this role will loose their permsisions , Are you sure you want to delete?').submit()">
                                    Delete
                                </button>
                                <form id="role-delete-form-{{ $role->id }}" style="display:none;"
                                    action="{{ route('admin.roles.destroy', $role->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endcan
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

