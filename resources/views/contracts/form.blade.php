@csrf
<div class="card">
    <!-- Start of Extra Information-->
    <h4 class="bold" style="padding:0px 10px;font-size:14px;"><a class="toggler" href="javascript:;"
                                                                 onClick="toggleSearch('client-info', '0', this)">{{ __('Client Information') }} <span
                class="text-danger">*</span><span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>

    <div class="card-body" style="display:none"
         id="client-info">
        @include('contracts.partials.client_information')

    </div>
</div>


<div class="card">
    <!-- Start of Extra Information-->
    <h4 class="bold" style="padding:0px 10px;font-size:14px;"><a class="toggler" href="javascript:;"
                                                                 onClick="toggleSearch('additionalsigners', '0', this)">{{ __('Additional Signers') }} <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>

    <div class="card-body" style="display:none"
         id="additionalsigners">
        @include('contracts.partials.additional_signers')
    </div>
</div>
<div class="card">
    <!-- Start of Extra Information-->
    <h4 class="bold" style="padding:0px 10px;font-size:14px;"><a class="toggler" href="javascript:;"
                                                                 onClick="toggleSearch('detailfields', '0', this)">{{ __('Property Information') }} <span
                class="text-danger">*</span><span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>

    <div class="card-body" style="display:none"
         id="detailfields">
        @include('contracts.partials.property_information')
    </div>
</div>
        @include('contracts.partials.contract_info')


<script>
    $(function () {
        $('#client-form').validate();
        //$('#client-info, #detailfields').find('input,select').attr('disabled', 'true');
        //$('#client-info, #detailfields').find('input, select').parent('div').addClass('disabled');
//        toggleFields();
        getIssuesList();
    });

    function getIssuesList() {
        resetTasks();
    }
    function resetTasks() {
        var issue_id = $('#issue_id').val();
        $('#issues-list-container').load('{{url("backend/load/issue-list/")}}/' + issue_id, function () {
            $('#selected-issues-container').html('');
        });
    }
    function moveBack($this) {
        $($this).attr('onclick', 'moveThis(this)');
        $($this).addClass('not-selected');
        $($this).appendTo('.issues-list-container');
        $($this).find('.issue-list-id').attr('disabled', true);
    }
    function moveThis($this) {
        $($this).attr('onclick', 'moveBack(this)');
        $($this).removeClass('not-selected');
        $($this).find('.issue-list-id').removeAttr('disabled');
        $($this).appendTo('#selected-issues-container');
    }
    function toggleFields() {
        $('.dd').addClass('d-none');
        $('.d-' + $('#type').val()).removeClass('d-none');
    }
    function removeIssue($this) {
        if ($('.other-task').length == 1) {
            alert('Last Element can not be deleted. You may left empty.');
            return false;

        }
        $($this).parents('.other-task').remove();
    }
</script>
