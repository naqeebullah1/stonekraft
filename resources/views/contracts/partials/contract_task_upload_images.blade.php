<form action="{{ url('backend/add-images-contract_tasks',request()->id) }}" method="post" enctype="multipart/form-data">
    @method('put')
    @csrf
    <div class="d-flex">
        <h4 class="font-weight-bold flex-grow-1">
            Task Images - (<?= $task->task_name ?>)
        </h4>
        <a onclick="getTask()" class="btn btn-success align-self-center" href="javascript:;">Back</a>
    </div>
    <div class="images-box bg-gray">
    <div class="row clearfix pt-3 p-row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-8">
                    {{ Form::file('images[]',NULL,[]) }}
                </div>
                <div class="col-md-2 mt-1">
                    <a class="btn btn-success btn-xs clone-row-btn"><i class="fa fa-plus"></i></a>
                </div>          
            </div>   
        </div>
        <div class="col-md-6">
            @foreach($task->images as $image)
                <img src="{{ asset('contract_tasks') }}/{{ $image->image_link }}" width="200px" />
            @endforeach
        </div>
       
    </div>
    <div class="holder"></div>
    </div>
    <div class="row">
        <div class="col-12 text-right mt-2">
            <button class="btn btn-primary" type="submit">Upload</button>
        </div>
    </div>
</form>
<script type="text/javascript">
    $('body').on('click', '.clone-row-btn', function() {
        $(".p-row").clone().removeClass('p-row').appendTo(".holder");
    });
</script>
 