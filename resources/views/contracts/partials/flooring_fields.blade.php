<div class='row dd d-<?= $key ?>' style="padding: 10px; background: #f8f8f8;margin-bottom: 10px;">
<h5 class="col-12">
    Scope of work for <?= $type ?>
    <div class="text-right">
    <input  type="checkbox" class="room-type-<?= $key ?>" checked name="room-type-<?= $key ?>[]" value="1"  onClick="roomAttrs({{$key}},1)"> Kitchen</label>
    <input  type="checkbox" class="room-type-<?= $key ?>" checked name="room-type-<?= $key ?>[]" value="2" onClick="roomAttrs({{$key}},2)"> Bathroom</label>
    </div>
</h5>
<div class="row kblock-<?= $key ?> d-none">
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Kitchen Flooring Color</label>
        {{ Form::hidden('contract_work['.$key.'][type]',$key,['class'=>'form-control','required']) }}
        {{ Form::text('contract_work['.$key.'][kitchen_flooring_color]',$scopeOfWork->kitchen_flooring_color,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Flooring Material</label>
        {{ Form::text('contract_work['.$key.'][kitchen_flooring_material]',$scopeOfWork->kitchen_flooring_material,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Kitchen Square Footage</label>
        {{ Form::text('contract_work['.$key.'][kitchen_flooring_square_footage]',$scopeOfWork->kitchen_flooring_square_footage,['class'=>'form-control','required']) }}
    </div>

</div>

<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Size</label>
        {{ Form::text('contract_work['.$key.'][kitchen_flooring_size]',$scopeOfWork->kitchen_flooring_size,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Installation?</label>
        {{ Form::select('contract_work['.$key.'][kitchen_flooring_installation]',['yes'=>'Yes','no'=>'No'],$scopeOfWork->kitchen_flooring_installation,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Trim Work</label>
        {{ Form::text('contract_work['.$key.'][kitchen_flooring_trim_work]',$scopeOfWork->kitchen_flooring_trim_work,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Grout Color</label>
        {{ Form::text('contract_work['.$key.'][kitchen_flooring_grout_color]',$scopeOfWork->kitchen_flooring_grout_color,['class'=>'form-control','required']) }}
    </div>
</div>
</div>
<div class="row bblock-<?= $key ?> d-none">
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Bathroom Color</label>
        {{ Form::text('contract_work['.$key.'][bathroom_flooring_color]',$scopeOfWork->bathroom_flooring_color,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Flooring Material</label>
        {{ Form::text('contract_work['.$key.'][bathroom_flooring_material]',$scopeOfWork->bathroom_flooring_material,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Installation</label>
        {{ Form::select('contract_work['.$key.'][bathroom_flooring_installation]',['yes'=>'Yes','no'=>'No'],$scopeOfWork->bathroom_flooring_installation,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Bathroom Square Footage</label>
        {{ Form::text('contract_work['.$key.'][bathroom_flooring_square_footage]',$scopeOfWork->bathroom_flooring_square_footage,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-3">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Bathroom Size</label>
        {{ Form::text('contract_work['.$key.'][bathroom_flooring_size]',$scopeOfWork->bathroom_flooring_size,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-3">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Bathroom Show Remodel</label>
        {{ Form::text('contract_work['.$key.'][bathroom_flooring_shower_remodel]',$scopeOfWork->bathroom_flooring_shower_remodel,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-3">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Toilet/Tub Remover </label>
        {{ Form::select('contract_work['.$key.'][bathroom_flooring_toilet_tub_remover]',['yes'=>'Yes','no'=>'No'],$scopeOfWork->bathroom_flooring_toilet_tub_remover,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-3">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Bathroom Grout Color</label>
        {{ Form::text('contract_work['.$key.'][bathroom_flooring_grout_color]',$scopeOfWork->bathroom_flooring_grout_color,['class'=>'form-control','required']) }}
    </div>

</div>

</div>
</div>