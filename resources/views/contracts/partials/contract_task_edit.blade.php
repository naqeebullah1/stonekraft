<form action="{{ url('backend/edit-contract_tasks',request()->id) }}" method="post" enctype="multipart/form-data">
    @method('put')
    @csrf
    <div class="d-flex">
        <h4 class="font-weight-bold flex-grow-1">
            Shedule Task - (<?= $task->task_name ?>)
        </h4>
        <a onclick="getTask()" class="btn btn-success align-self-center" href="javascript:;">Back</a>
    </div>

    <div class="row clearfix pt-3">

        <div class="col-md-4">
            <div class="form-group form-group-default required" aria-required="true">
                <label class="col-md-12 col-form-label text-md-left ">Task Name</label>
                {{ Form::text('contract_task[task_name]',$task->task_name,['class'=>'form-control']) }}
            </div>
        </div>
        <div class="col-md-4 " style="padding-right: 10px;">
            <div class="form-group form-group-default required" aria-required="true">
                <label class="col-md-12 col-form-label text-md-left ">Due Date</label>
                {{ Form::text('contract_task[task_date]',formatDate($task->task_date),['class'=>'form-control d-datepicker']) }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-group-default required" aria-required="true">
                <label class="col-md-12 col-form-label text-md-left ">Team Member</label>
                {{ Form::select('contract_task[worker_id]',[''=>'Select Team Member']+$workers->toArray(),null,['class'=>'form-control d-select2']) }}
            </div>
        </div>
    </div>
    <div class="row clearfix pt-3">
        <div class="col-md-12">
            <div class="form-group form-group-default required" aria-required="true">
                <label class="col-md-12 col-form-label text-md-left ">Task Description</label>
                {{ Form::text('contract_task[task_description]',$task->task_description,['class'=>'form-control','rows'=>3,'required']) }}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right mt-2">
            <button class="btn btn-primary" type="submit">Update</button>
        </div>
    </div>
</form>
