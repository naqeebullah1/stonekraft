<div class='row dd d-<?= $key ?>' style="padding: 10px; background: #f8f8f8;margin-bottom: 10px;">
<h5 class="col-12">
    Scope of work for <?= $type ?>
    <div class="text-right">
    <input  type="checkbox" class="room-type-<?= $key ?>" 
        @if(!$scopeOfWork->room_type_id) checked @endif
        @if($scopeOfWork->room_type_id && in_array($scopeOfWork->room_type_id,[1,3])) checked @endif 
        name="room-type-<?= $key ?>[]" value="1"  onClick="roomAttrs({{$key}},1)"> Kitchen</label>
    <input  type="checkbox" class="room-type-<?= $key ?>" 
        @if(!$scopeOfWork->room_type_id) checked @endif
        @if($scopeOfWork->room_type_id && in_array($scopeOfWork->room_type_id,[2,3])) checked @endif
     name="room-type-<?= $key ?>[]" value="2" onClick="roomAttrs({{$key}},2)"> Bathroom</label>
    </div>
</h5>
<div class="row kblock-<?= $key ?> d-none">
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Kitchen Color</label>
        {{ Form::hidden('contract_work['.$key.'][type]',$key,['class'=>'form-control','required']) }}
        {{ Form::text('contract_work['.$key.'][kitchen_granite_color]',$scopeOfWork->kitchen_granite_color,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Kitchen Finish</label>
        {{ Form::text('contract_work['.$key.'][kitchen_granite_finish]',$scopeOfWork->kitchen_granite_finish,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Kitchen Square Footage</label>
        {{ Form::text('contract_work['.$key.'][kitchen_granite_square_footage]',$scopeOfWork->kitchen_granite_square_footage,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Kitchen Sink</label>
        {{ Form::text('contract_work['.$key.'][kitchen_granite_sink]',$scopeOfWork->kitchen_granite_sink,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Installation</label>
        {{ Form::select('contract_work['.$key.'][kitchen_granite_installation]',['yes'=>'Yes','no'=>'No'],$scopeOfWork->kitchen_granite_installation,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Kitchen Granite Edge</label>
        {{ Form::text('contract_work['.$key.'][kitchen_granite_edge]',$scopeOfWork->kitchen_granite_edge,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Brackets</label>
        {{ Form::text('contract_work['.$key.'][kitchen_granite_brackets]',$scopeOfWork->kitchen_granite_brackets,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Diswasher Mount</label>
        {{ Form::text('contract_work['.$key.'][kitchen_granite_dishwasher_mount]',$scopeOfWork->kitchen_granite_dishwasher_mount,['class'=>'form-control','required']) }}
    </div>
</div>
</div>
<div class="row bblock-<?= $key ?> d-none">
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Bathroom Color</label>
        {{ Form::text('contract_work['.$key.'][bathroom_granite_color]',$scopeOfWork->bathroom_granite_color,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Bathroom Finish</label>
        {{ Form::text('contract_work['.$key.'][bathroom_granite_finish]',$scopeOfWork->bathroom_granite_finish,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Bathroom Size</label>
        {{ Form::text('contract_work['.$key.'][bathroom_granite_square_footage]',$scopeOfWork->bathroom_granite_square_footage,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Installation</label>
        {{ Form::select('contract_work['.$key.'][bathroom_granite_installation]',['yes'=>'Yes','no'=>'No'],$scopeOfWork->bathroom_granite_installation,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-3">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Sink</label>
        {{ Form::text('contract_work['.$key.'][bathroom_granite_sink]',$scopeOfWork->bathroom_granite_sink,['class'=>'form-control','required']) }}
    </div>

</div>


<div class="col-3">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Nitch/Window seal </label>
        {{ Form::select('contract_work['.$key.'][bathroom_granite_nitch_window_seal]',['yes'=>'Yes','no'=>'No'],$scopeOfWork->bathroom_granite_nitch_window_seal,['class'=>'form-control','required']) }}
    </div>
</div>

</div>
</div>
