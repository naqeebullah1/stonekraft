<div class="row clearfix">
    <div class="col-md-3">
        <div class="form-group form-group-default " aria-required="true">
            <label for="second_signer_name" class="col-md-12 col-form-label text-md-left">{{ __('Signer Full Name') }}</label>
            <input id="second_signer_name" type="text" class="form-control @error('second_signer_name') is-invalid @enderror"
                   name="contracts[second_signer_name]" value="{{ old('second_signer_name') ?? $contract->second_signer_name }}" 
                   autocomplete="second_signer_name" autofocus>
            @error('second_signer_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

    </div>
    <div class="col-md-4">
        <div class="form-group form-group-default ">
            <label for="second_signer_email" class="col-md-12 col-form-label text-md-left">{{ __('Email') }}</label>
            <input id="second_signer_email" type="text" class="form-control @error('email') is-invalid @enderror"
                   name="contracts[second_signer_email]" value="{{ old('second_signer_email') ?? $contract->second_signer_email }}"  autocomplete="email" autofocus>
            @error('second_signer_email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>    
</div>
<div class="row clearfix">
    <div class="col-md-3">
        <div class="form-group form-group-default " aria-required="true">
            <label for="third_signer_name" class="col-md-12 col-form-label text-md-left">{{ __('Signer Full Name') }}</label>
            <input id="third_signer_name" type="text" class="form-control @error('third_signer_name') is-invalid @enderror"
                   name="contracts[third_signer_name]" value="{{ old('third_signer_name') ?? $contract->third_signer_name }}" 
                   autocomplete="third_signer_name" autofocus>
            @error('third_signer_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

    </div>
    <div class="col-md-4">
        <div class="form-group form-group-default ">
            <label for="third_signer_email" class="col-md-12 col-form-label text-md-left">{{ __('Email') }}</label>
            <input id="third_signer_email" type="text" class="form-control @error('email') is-invalid @enderror"
                   name="contracts[third_signer_email]" value="{{ old('third_signer_email') ?? $contract->third_signer_email }}"  autocomplete="email" autofocus>
            @error('third_signer_email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>    
</div>
