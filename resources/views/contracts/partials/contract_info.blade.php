<!-- End of extra Information -->
@if($contract)
<div class="card">
    <!-- Start of Extra Information-->
    <h4 class="bold" style="padding:0px 10px;font-size:14px;">
        <a class="toggler" href="javascript:;"
           onClick="toggleSearch('what-can-we-do', '0', this)">{{ __('Contract Information') }} <span
                class="text-danger">*</span><span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>

    <div class="card-body" @if(request()->has('contract_id')) style="display:none" @else  style="display:block" @endif id="what-can-we-do">
        <div class="row clearfix">

            <div class="col-12">
                <div class="row">
                    <div class="col-3">
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Total Contract Price</label>
                            {{ Form::number('contracts[contract_price]',$contract->contract_price,['id'=>'contract_price','onchange'=>'inWords(this,"contract_price");setDeposit()','class'=>'form-control','step'=>'any','min'=>0,'required']) }}
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Total Contract Price Words</label>
                            <input type="text" id="contract_price-inwords" class="form-control" disabled="" value="{{ convertToWOrd($contract->contract_price) }}">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Approx. Sqft</label>
                            {{ Form::number('contracts[sq_ft]',$contract->sq_ft,['step'=>'any','min'=>0,'class'=>'form-control','required']) }}
                        </div>
                    </div>
                    <div class="col-3">
                        <?php
                        $deposit = ($contract->deposit_percent) ? $contract->deposit_percent : 50;
                        ?>
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Deposit Percent</label>
                            {{ Form::number('contracts[deposit_percent]',$deposit,['onchange'=>'setDeposit()','id'=>'deposit_percent','class'=>'form-control','step'=>'any','min'=>0,'required']) }}
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Deposit Amount</label>
                            {{ Form::number('contracts[deposit]',$contract->deposit,['readonly','id'=>'deposit_amount','class'=>'form-control','step'=>'any','min'=>0,'required']) }}
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Second Payment</label>
                            {{ Form::number('contracts[second_payment]',$contract->second_payment,['id'=>'second_payment','class'=>'form-control','onchange'=>'setDeposit()','step'=>'any','min'=>0]) }}
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Balance</label>
                            {{ Form::number('contracts[balance]',$contract->balance,['readonly','id'=>'balance','class'=>'form-control','step'=>'any','min'=>0,'required']) }}
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Date of contract</label>
                            {{ Form::text('contracts[date_of_contract]',formatDate($contract->date_of_contract,'u'),['class'=>'form-control datepicker','required']) }}
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Date of template</label>
                            {{ Form::text('contracts[date_of_template]',formatDate($contract->date_of_template,'u'),['class'=>'form-control datepicker','required']) }}
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="form-group form-group-default required" aria-required="true">
                            <label class="col-md-12 col-form-label text-md-left">Date of installation</label>
                            {{ Form::text('contracts[date_of_installation]',formatDate($contract->date_of_installation,'u'),['class'=>'form-control datepicker','required']) }}
                        </div>

                    </div>


                </div>
                <div class="col-md-12 mb-3">
                    <?php
                    $types = contractTypes();
                    $scopeOfWorks = $contract->contract_works
                            ->groupBy('type');

//                    dump($scopeOfWorks);
                    ?>
                    @foreach($types as $key=>$type)
                    <label class="mr-5">
                        <input <?= (isset($scopeOfWorks[$key])) ? 'checked' : '' ?> id="type-{{ $key }}" type="checkbox" class="contract_type" value="<?= $key ?>" name="types[]"> <?= $type ?>
                    </label>
                    @endforeach
                </div>
                <!--SCOPE OF WORK-->
                @foreach($types as $key=>$type)
                <?php
                if (isset($scopeOfWorks[$key])) {
                    $scopeOfWork = $scopeOfWorks[$key]->first();
                } else {
                    $scopeOfWork = new \App\Models\ContractWork;
                }
                ?>
                @switch($key)
                    @case(1)
                        @include('contracts.partials.counter_top_fields')
                        @break
                    @case(2)
                        @include('contracts.partials.cabinet_fields')
                        @break
                    @case(3)
                        @include('contracts.partials.flooring_fields')
                        @break
                    @case(4)
                        @include('contracts.partials.others')
                        @break
                    @default

                @endswitch
                @endforeach
            </div>



        </div>
        <button id="save-form" type="submit" onclick="return checkServices()" class="btn btn-primary float-right">
            @if(isset(request()->id) || (isset($contract) && $contract->id!=null))
            {{ __('Update') }}
            @else
            {{ __('Create') }}
            @endif
        </button>
    </div>
</div>
@endif
<script>
    function checkServices(){
        if($('.contract_type:checkbox:checked').length == 0){
          alert("Please check at least one service");
          return false;
        }else{
            return true;
        }    
    }
//    Remove toggleFields
    function inWords($this, $field) {
        if ($($this).val() == '') {
            $('#' + $field + '-inwords').val('');
            return false;
        }
        $('#' + $field + '-inwords').load('{{ url("backend/to-words") }}/' + $($this).val(), function ($a) {
            $('#' + $field + '-inwords').val($a);
        });
    }
    function setDeposit() {
        var $totalPrice = $('#contract_price').val();
        var $percent = $('#deposit_percent').val();
        var second_payment = $('#second_payment').val();
        var depositAmount = Number((($percent / 100) * $totalPrice).toFixed(2));
        $('#deposit_amount').val(depositAmount);
        $('#balance').val($totalPrice - depositAmount - second_payment);

    }
    function scopeOfWorks() {
        $('.dd').addClass('d-none');
        $('.contract_type').each(function () {
            var type_id = $(this).val();
            if ($(this).prop('checked')) {
                $('.d-' + type_id).removeClass('d-none');
            }
        $("input:checkbox[class=room-type-"+$(this).val()+"]:checked").each(function () {
            if($(this).val() == 1){
                $('.kblock-' + type_id).removeClass('d-none');
            }
            if($(this).val() == 2){
                $('.bblock-' + type_id).removeClass('d-none');
            }
        });
        });
    }
    function ToggleScopeOfWorks(ElementClass){
        $('.d-' + ElementClass).removeClass('d-none');   
        $('#type-' + ElementClass).prop('checked',true);    
    }
    function roomAttrs(key,type){

        $("input:checkbox[class=room-type-"+key+"]").each(function () {
            if($(this).val() == 1){
                if(this.checked){
                    $('.kblock-' + key).removeClass('d-none');                    
                }else{
                    $('.kblock-' + key).addClass('d-none');
                }
            }
            if($(this).val() == 2){
                if(this.checked){
                    $('.bblock-' + key).removeClass('d-none');                    
                }else{
                    $('.bblock-' + key).addClass('d-none');
                }                
            }
        });

    }
    $(function () {
        $('.contract_type').change(function () {
            scopeOfWorks();
        });

        $('.room_type').change(function () {
            if($(this).is(':checked')){

            }else{

            }
        });
        scopeOfWorks();

        @if($client->services)
        @foreach($client->services as $key=>$service)
            @switch($service->service)
            @case('counter')
                ToggleScopeOfWorks(1);
            @break;
            @case('cabinet')
                ToggleScopeOfWorks(2);
            @break;
            @case('flooring')
                ToggleScopeOfWorks(3);
            @break;
            @case('other')
                ToggleScopeOfWorks(4);
            @break;
            @endswitch
        @endforeach
        @endif

    })
</script>
<!-- End of extra Information -->