<div class="row clearfix">
    <div class="col-md-6">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="first_name" class="col-md-12 col-form-label text-md-left">{{ __('First Name') }}</label>
            <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror"
                   name="first_name" value="{{ old('first_name') ?? $client->first_name }}" required
                   autocomplete="first_name" autofocus>
            @error('first_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

    </div>
    <div class="col-md-6">
        <div class="form-group form-group-default required">
            <label for="last_name" class="col-md-12 col-form-label text-md-left">{{ __('Last Name') }}</label>
            <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror"
                   name="last_name" value="{{ old('last_name') ?? $client->last_name }}" required autocomplete="last_name"
                   autofocus>
            @error('last_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

    </div>
</div>

<div class="row clearfix">
    <div class="col-md">
        <div class="form-group form-group-default required">
            <label for="email" class="col-md-12 col-form-label text-md-left">{{ __('Email') }}</label>
            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                   name="email" value="{{ old('email') ?? $client->email }}" required autocomplete="email" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-md">
        <div class="form-group form-group-default required">
            <label for="email_confirmation"
                   class="col-md-12 col-form-label text-md-left">{{ __('Confirm Email') }}</label>
            <input id="email_confirmation" type="text"
                   class="form-control @error('email_confirmation') is-invalid @enderror" name="email_confirmation"
                   required autocomplete="email_confirmation" autofocus value="{{ old('email') ?? $client->email }}">
            @error('email_confirmation')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-6">
        <div class="form-group form-group-default">
            <label for="phone_one" class="col-md-12 col-form-label text-md-left">{{ __('Phone Number') }}<span
                    class="text-danger">*</span></label>
            <input id="phone_one" type="text" class="phonemask form-control @error('phone_one') is-invalid @enderror"
                   name="phone_one" value="{{ old('phone_one') ?? $client->phone_one }}" autofocus>
            @error('phone_one')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group form-group-default">
            <label for="phone_two" class="col-md-12 col-form-label text-md-left">{{ __('Alt. Phone Number') }}</label>
            <input id="phone_two" type="text" class="phonemask form-control @error('phone_two') is-invalid @enderror"
                   name="phone_two" value="{{ old('phone_two') ?? $client->phone_two }}" autofocus>
            @error('two_one')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>


<!--BILLING ADDRESS-->
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default">
            <label for="address_one" class="col-md-12 col-form-label text-md-left">{{ __('Billing Address 1') }}<span
                    class="text-danger">*</span></label>
            <input id="address_one" type="text" class="form-control @error('address_one') is-invalid @enderror"
                   name="address_one" value="{{ old('address_one') ?? $client->address_one }}" autofocus>
            @error('address_one')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default">
            <label for="address_two" class="col-md-12 col-form-label text-md-left">{{ __('Billing Address 2') }}<span
                    class="text-danger">*</span></label>
            <input id="address_two" type="text" class="form-control @error('address_two') is-invalid @enderror"
                   name="address_two" value="{{ old('address_two') ?? $client->address_two }}" autofocus>
            @error('address_two')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-4">
        <div class="form-group form-group-default">
            <label for="city" class="col-md-12 col-form-label text-md-left">{{ __('Billing City') }}<span
                    class="text-danger">*</span></label>
            <input id="city" type="text" class="form-control @error('city') is-invalid @enderror"
                   name="city" value="{{ old('city') ?? $client->city }}" autofocus>
            @error('city')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-group-default">
            <label for="state" class="col-md-12 col-form-label text-md-left">{{ __('Billing State/Province') }}<span
                    class="text-danger">*</span></label>
            @include('pages.lead.state_partial')
            @error('state')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-group-default">
            <label for="zip" class="col-md-12 col-form-label text-md-left">{{ __('Billing Zip/Postal Code') }}<span
                    class="text-danger">*</span></label>
            <input id="zip" type="number" maxlength="5" class="form-control @error('zip') is-invalid @enderror"
                   name="zip" value="{{ old('zip') ?? $client->zip }}" autofocus>
            @error('zip')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>