<div class="row clearfix pt-3">
    <div class="col-12 text-left mb-3">
        @include('partials.legends')
    </div>
    <div class="col-12">
        <table border="1" class="table table-striped table-bordered table-sm">
            <thead>
                <tr>
                    <th style="width:10px;" class="p-2"></th>
                    <th>Due Date</th>
                    <th>Team Member</th>
                    <th>Type</th>
                    <th>Task Name</th>
                    <th>Task Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tasks as $task)
                <tr>
                    <td>
                        <?php
                        $c = 'bg-orange';
                        if ($task->task_date) {
                            if ($task->task_date <= date('Y-m-d', strtotime(date('Y-m-d') . ' + 7 days'))) {
                                $c = 'bg-warning';
                            }
                            if ($task->task_date <= date('Y-m-d')) {
                                $c = 'bg-danger';
                            }
                        }
                        ?>
                        <i style="padding: 1px 9px;" class="<?= $c ?> rounded-circle mr-2"  data-toggle="tooltip" data-placement="top"></i>
                    </td>
                    <td>
                        <?php
                        if ($task->task_date) {
                            echo formatDate($task->task_date, 'u');
                        } else {
                            echo "Pending";
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($task->worker_id) {
                            $worker = $task->worker;
                            echo $worker->first_name . ' ' . $worker->last_name;
                        } else {
                            echo "Pending";
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $task->issue_id?$task->issue->issue:'Custom';
                        
                        ?>
                    </td>
                    <td>
                        <?php
//                        if ($task->issue_id) {
                        echo $task->task_name;
//                        } 
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $task->task_description;
                        ?>
                    </td>
                    <td>

                        <a  href="javascript:;" onclick="taskUploadImages(<?= $task->id ?>)" tooltip="Add Images" class="btn btn-success btn-sm btn-small">
                            <i class="fa fa-camera"></i>
                        </a>                        
                        @can('workOrder-edit')
                        <a  href="javascript:;" onclick="editTask(<?= $task->id ?>)" tooltip="Assign Task" class="btn btn-success btn-sm btn-small">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endcan
                        @if(isset($type) && $type=='SCHEDULED' && $task->is_completed==0)
                        <a   href="{{ route('admin.task.complete',$task->id) }}" class="btn btn-success btn-sm btn-small">
                            <i class="fa fa-check"></i> Mark as Complete
                        </a>
                        @endif
                        @if(isset($type) && $type=='COMPLETED' && $task->is_completed==1)

                        <a   href="{{ route('admin.task.incomplete',$task->id) }}" class="btn btn-warning btn-sm btn-small">
                            <i class="fa fa-check"></i> Mark as InComplete
                        </a>
                        @endif
                        @can('workOrder-edit')
                        <a  href="{{ route('admin.task.unschedule',['id'=>$task->id]) }}"  class="btn btn-warning btn-sm btn-small">
                            <i class="fa fa-undo"></i>
                        </a>
                        @endcan
                        @can('workOrder-delete')
                        <form action="{{route('admin.task.delete',$task->id)}}" method="post" style="display: inline;">
                            @csrf
                            {{method_field('delete')}}
                            <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                        </form>
                        @endcan
                       
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="">
            {{ $tasks->links()->with('onclick','paginatedTasks(this)') }}
        </div>
    </div>
</div>
<script type="text/javascript">
   function taskUploadImages($id) {
        $('.all-task-container').load("{{ url('backend/contract_task/upload_images') }}/" + $id, function () {
            $('.d-datepicker').datepicker({
                autoclose: true
            });
            $('.d-select2').select2();
        });
    }
</script>