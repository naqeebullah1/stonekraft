<div class='row dd d-<?= $key ?>' style="padding: 10px; background: #f8f8f8;margin-bottom: 10px;">
<h5 class="col-12">
    Scope of work for <?= $type ?>
    <div class="text-right">
    <input  type="checkbox" class="room-type-<?= $key ?>" checked name="room-type-<?= $key ?>[]" value="1"  onClick="roomAttrs({{$key}},1)"> Kitchen</label>
    <input  type="checkbox" class="room-type-<?= $key ?>" checked name="room-type-<?= $key ?>[]" value="2" onClick="roomAttrs({{$key}},2)"> Bathroom</label>
    </div>    
</h5>
<div class="row col-12">
<div class="col-12">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Others</label>
        {{ Form::hidden('contract_work['.$key.'][type]',$key,['class'=>'form-control','required']) }}
        {{ Form::textarea('contract_work['.$key.'][others]',$scopeOfWork->others,['class'=>'form-control editor','required','rows'=>'5']) }}
    </div>

</div>

</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        CKEDITOR.replaceAll('editor'); 
    })
</script>