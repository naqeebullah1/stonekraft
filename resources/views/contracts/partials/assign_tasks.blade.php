@if($contract)
<form action="{{ url('backend/save-contract_tasks') }}" method="post">
    @method('put')
    @csrf
    <input name="contract_id" hidden="" value="<?= $contract->id ?>">
    <div class="row clearfix pt-3">
        <div class="col-md-5">
            <div class="form-group form-group-default required" aria-required="true">
                <label class="col-md-12 col-form-label text-md-left fade">Select Type</label>
                @php
                    $choosen_issues = [''=> 'Select Type'];
                    $chooseScope    = $contract->contract_works
                            ->groupBy('type');
                    $types = contractTypes();
                    foreach($types as $key=>$type){
                       if(!isset($chooseScope[$key])){ continue; }
                        $choosen_issues[$key] = $type;
                    }
                @endphp
                {{ Form::select('issue_id',$choosen_issues,null,['onchange'=>'getIssuesList()','id'=>'issue_id','class'=>'form-control ','data-init-plugin'=>'select2']) }}
            </div>
        </div>
        <div class="col-12 ">
            <div class="row">
                <div class="col-6" >
                    <div style="border: 1px solid #ccc;" class="p-3 ">
                        <h5>Tasks</h5>
                        <div id="issues-list-container"  class="row issues-list-container">
                        </div>
                    </div>
                </div>


                <div class="col-6">
                    <div class="p-3" style="border: 1px solid #ccc">
                        <h5>Selected Tasks</h5>
                        <div  id="selected-issues-container" class="row selected-issues-container"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-3 mt-2" style="background: #eee;">
        <div class="row">
            <h4 class="col-11">Add Custom Task 
            </h4>
            <div class="col-1 text-right mt-2">
                <button onclick="cloneTasks()" class="btn btn-success btn-sm btn-small" type="button">Add More</button>
            </div>
        </div>
        <div class="other-task-container">
            <div class="row other-task mb-2">
                <div class="col-5">
                    <input class="form-control" name="task_name[]" placeholder="Task Name">
                </div>
                <div class="col-6">
                    <input class="form-control" name="task_description[]" placeholder="Task Description">
                </div>
                <div class="col-1">
                    <a href="javascript:;" onclick="removeIssue(this)" class="btn btn-danger "><i class="fa fa-trash"></i></a>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right mt-2">
            <button class="btn btn-success" type="submit">Create Task</button>
        </div>
    </div>
</form>
@endif
