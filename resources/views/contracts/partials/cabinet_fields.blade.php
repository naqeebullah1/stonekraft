<div class='row dd d-<?= $key ?>' style="padding: 10px; background: #f8f8f8;margin-bottom: 10px;">
<h5 class="col-12">
    Scope of work for <?= $type ?>
    <div class="text-right">
    <input  type="checkbox" class="room-type-<?= $key ?>" checked name="room-type-<?= $key ?>[]" value="1"  onClick="roomAttrs({{$key}},1)"> Kitchen</label>
    <input  type="checkbox" class="room-type-<?= $key ?>" checked name="room-type-<?= $key ?>[]" value="2" onClick="roomAttrs({{$key}},2)"> Bathroom</label>
    </div>
</h5>
<div class="row kblock-<?= $key ?> d-none">
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Kitchen Color</label>
        {{ Form::hidden('contract_work['.$key.'][type]',$key,['class'=>'form-control','required']) }}
        {{ Form::text('contract_work['.$key.'][kitchen_cabinet_color]',$scopeOfWork->kitchen_cabinet_color,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Cabinet Brand</label>
        {{ Form::text('contract_work['.$key.'][kitchen_cabinet_brand]',$scopeOfWork->kitchen_cabinet_brand,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Island Color</label>
        {{ Form::text('contract_work['.$key.'][kitchen_cabinet_island_color]',$scopeOfWork->kitchen_cabinet_island_color,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Installation</label>
        {{ Form::select('contract_work['.$key.'][kitchen_cabinet_installation]',['yes'=>'Yes','no'=>'No'],$scopeOfWork->kitchen_cabinet_installation,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Knobs</label>
        {{ Form::text('contract_work['.$key.'][kitchen_cabinet_knobs]',$scopeOfWork->kitchen_cabinet_knobs,['class'=>'form-control','required']) }}
    </div>
</div>
</div>
<div class="row bblock-<?= $key ?> d-none">
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Bathroom Color</label>
        {{ Form::text('contract_work['.$key.'][bathroom_cabinet_color]',$scopeOfWork->bathroom_cabinet_color,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Cabinet Brand</label>
        {{ Form::text('contract_work['.$key.'][bathroom_cabinet_brand]',$scopeOfWork->bathroom_cabinet_brand,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-4">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Installation</label>
        {{ Form::select('contract_work['.$key.'][bathroom_cabinet_installation]',['yes'=>'Yes','no'=>'No'],$scopeOfWork->bathroom_cabinet_installation,['class'=>'form-control','required']) }}
    </div>
</div>
<div class="col-3">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Knobs</label>
        {{ Form::text('contract_work['.$key.'][bathroom_cabinet_knobs]',$scopeOfWork->bathroom_cabinet_knobs,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-3">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Special Parts</label>
        {{ Form::text('contract_work['.$key.'][bathroom_cabinet_special_parts]',$scopeOfWork->bathroom_cabinet_special_parts,['class'=>'form-control','required']) }}
    </div>

</div>
<div class="col-3">
    <div class="form-group form-group-default required" aria-required="true">
        <label class="col-md-12 col-form-label text-md-left">Others</label>
        {{ Form::text('contract_work['.$key.'][bathroom_cabinet_other]',$scopeOfWork->bathroom_cabinet_other,['class'=>'form-control','required']) }}
    </div>

</div>

</div>
</div>