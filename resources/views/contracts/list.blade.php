@php
$client_label2 = 'Price';
$client_label3 = 'SQ FT';
$client_label4 = 'Deposit';
$client_label5 = 'Balance';
$client_label6 = 'Template Date';
$client_label7 = 'Installation Date';
$client_label8 = 'Client';
$client_label9 = 'Contract Status';
$client_label10 = 'Action';
@endphp
<div class="container grid-wrapper p-3 mt-2">
    <div class="grid-header">
        <div class="row d-none d-md-flex mb-md-2 p-md-1">
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label2 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label3 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label4 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label5 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label6 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label7 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label8 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label9 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $client_label10 }}</h4>
            </div>
        </div>
    </div>
    <div class="grid-body">
        @foreach ($clients as $client)
            @php
                $active = '<i style="" class="rounded-circle bg-primary" data-toggle="tooltip" data-placement="top" title=""></i>';
                $inactive = '<i style="" class="rounded-circle bg-danger" data-toggle="tooltip" data-placement="top" title=""></i>';
            @endphp
            <div class="row mb-md-0 p-md-1 mb-3 p-1">
                
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label2 }}</h4>
                    <p class="m-0 p-1">{{ $client->contract_price }}</p>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label3 }}</h4>
                    <p class="m-0 p-1">{{ $client->sq_ft }}</p>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label4 }}</h4>
                    <p class="m-0 p-1">{{ $client->deposit }}</p>
                </div>
               
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label5 }}</h4>
                    <p class="m-0 p-1">{{ $client->balance }}</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label6 }}</h4>
                    <p class="m-0 p-1">{{ formatDate($client->date_of_template,'u') }}</p>
                </div>
               
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label7 }}</h4>
                    <p class="m-0 p-1">{{ formatDate($client->date_of_installation,'u') }}</p>
                </div>
                
                <div class="col-1 col-sm-1 col-md-1">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label8 }}</h4>
                    <p class="m-0 p-1">{{ $client->client->first_name }} {{ $client->client->last_name }}</p>
                </div>
                <div class="col-2 col-sm-2 col-md-2">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label8 }}</h4>
                    @if($client->status == 0)
                        <span class="badge badge-warning">Pending</span>
                    @else
                        <a  target="_blank" class="badge badge-success" href="{{ route('admin.retrieve_document',$client->envelope_id) }}">View Contract</a>
                    @endif
                </div>
                <div class="col-1 col-sm-1 col-md-1">
                    <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $client_label9 }}</h4>
                    <br class="d-none d-md-none d-sm-block " />
                   
                    @can('client-edit')
                        <a href="{{route('admin.contracts.edit',[$client->client_id,$client->id])}}"
                            class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button">
                            Edit</a>
                    @endcan
                
                </div>
            </div> <!-- End of ROW -->
        @endforeach
    </div>
</div>

{{ $clients->links() }}
