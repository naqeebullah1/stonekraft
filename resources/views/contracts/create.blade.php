@extends('master')
@section('title', 'Contract Create')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <h4 class="semi-bold text-center light-heading">{{ __('Create Contract - '.$client->first_name.' '.$client->last_name) }}</h4>

                    <div class="card-body">
                        <form method="POST" id="client-form" action="{{ route('admin.contracts.store',request()->client_id) }}">
                            @include('contracts.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
