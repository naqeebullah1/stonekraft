@extends('master')
@section('title', 'Contract Edit')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <h4 class="semi-bold text-center light-heading">
                    {{ __('Edit Contract - '.$client->first_name.' '.$client->last_name) }}
                </h4>
                <div class="card-body">
                    <form method="POST" id="client-form" action="{{ route('admin.contracts.update',[request()->client_id,request()->id]) }}">
                        @method('put')
                        @include('contracts.form')
                    </form>
                    @if($contract->status == 1)
                    <div class="card">
                        <!-- Start of Extra Information-->
                        <h4 class="bold" style="padding:0px 10px;font-size:14px;">
                            <a class="toggler" href="javascript:;"
                               onClick="toggleSearch('task-info', '0', this)">{{ __('Task Information') }} <span
                                    class="text-danger">*</span><span class="pull-right"><i class="fa fa-chevron-up"></i></span></a></h4>
                        <div class="card-body" style="display:block" id="task-info">
                            <div class="row" style="display:block" id="task-info">
                                <div class="col-lg-6">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item flex-grow-1">
                                            <a onclick="getTask()" class="text-center nav-link active btn-block" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">All/Assigned Tasks</a>
                                        </li>
                                        <li class="nav-item flex-grow-1 te">
                                            <a  class="text-center nav-link btn-block" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Create Task</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-content" id="myTabContent">
                                <!--include('contracts.partials.all_tasks')-->
                                <div class="tab-pane pt-3 fade show active all-task-container" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    @include('contracts.partials.all_tasks')
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    @include('contracts.partials.assign_tasks')
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    function cloneTasks() {
        var $ele = $('.other-task:last').clone();
        $ele.find('input,textarea').val('');
        $ele.appendTo('.other-task-container');
    }
    $(document).ready(function () {
        //getTask();
    });
    function paginatedTasks($this) {
        var href = $($this).attr('href');
        $('#home').load(href);
        return false;
    }
    function getTask() {
        //$('#home').load('{{ url("backend/contract_task/all/".request()->id) }}');
    }
    function editTask($id) {
        $('.all-task-container').load("{{ url('backend/contract_task/edit') }}/" + $id, function () {
            $('.d-datepicker').datepicker({
                autoclose: true
            });
            $('.d-select2').select2();
        });
    }
</script>
@endsection
