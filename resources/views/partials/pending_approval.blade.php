<?php
$task_labels = ['Created Date', 'Work Order #', 'Property', 'Item', 'Notes', 'Action'];
?>
<div class="container-fluid grid-wrapper">
    <div class="grid-header">
        <div class="row d-none d-md-flex mb-md-2 p-md-1">
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label1 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label2 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label3 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label4 }}</h4>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label5 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label6 }}</h4>
            </div>
        </div>
    </div>
    <div class="grid-body">
        @foreach ($tasks as $task)
        @if ($task->schedule == 'PENDING' && $task->created_by == 'Client')
        <div class="row mb-md-0 p-md-1 mb-3 p-1">
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label1 }}</h4>
                {{ date_format($task->created_at, 'm-d-Y') }}</p>

            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label2 }}</h4>
                <p class="m-0 p-1">{{ $task->work_order }}</p>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label3 }}</h4>
                @isset($task->property->property_name)
                {{ $task->property->property_name }}
                @endisset
            </div>

            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label4 }}</h4>
                <p class="m-0 p-1">@isset($task->issue->issue){{ $task->issue->issue }}@endisset
                </p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label5 }}</h4>
                <p class="m-0 p-1">{{ substr($task->description, 0, 150) }} ....</p>

            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label6 }}</h4>
                <br class="d-none d-md-none d-sm-block " />
                @can('workOrder-edit')
                <a href="{{ route('admin.tasks.edit', $task->id) }}" class="btn btn-primary pt-1 pb-1"
                   style="padding-left: 24px;padding-right:24px;" role="button"><i
                        class="fa fa-check"></i></a>
                @endcan

                <a href="{{ route('admin.ticket.reject', ['id' => $task->id]) }}"
                   class="btn btn-danger pt-1 pb-1" style="padding-left: 24px;padding-right:24px;"><i
                        class="fa fa-times"></i></a>
            </div>
        </div>
        @endif
        @endforeach
    </div> <!-- End of ROW -->
</div>
