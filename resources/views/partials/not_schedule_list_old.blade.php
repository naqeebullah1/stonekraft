<?php
$task_label1='To be Schedule Date';
$task_label2='Work Order';
$task_label4='F/Name';
$task_label5='L/Name';
$task_label6='Property';
$task_label7='Item';
$task_label8='Recurring/One Time';
$task_label9='Action';
?>
<div class="container-fluid grid-wrapper">
    <div class="grid-header">
        <div class="row d-none d-md-flex mb-md-2 p-md-1 text-left">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label1 }}</h4>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label2 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label4 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label5 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label6 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label7 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label8 }}</h4>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label9 }}</h4>
            </div>
        </div>
    </div>
    <div class="grid-body">
        @foreach ($tasks as $task)
        @if ($task->schedule == 'NOT_SCHEDULED' && $task->remaining_days <= 60)
        <div class="row mb-md-0 p-md-1 mb-3 p-1 text-left">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label1 }}</h4>
                <p class="m-0 p-1">@if ($task->recurring_type == 'DAILY')
                    {{ date('m-Y') }}
                    @else
                    {{ $task->date_month }}
                    @endif</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label2 }}</h4>
                <p class="m-0 p-1">{{ $task->work_order }}</p>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label4 }}</h4>
                @if ($task->client_id != null)
                <p class="m-0 p-1">

                    {{ ($task->client)?$task->client->first_name:'' }}
                </p>

                @endif
                @if ($task->user_id != null)
                <p class="m-0 p-1">

                    {{ $task->user->first_name }}
                </p>
                @endif
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label5 }}</h4>
                @if ($task->client_id != null)
                <p class="m-0 p-1">
                    {{ ($task->client)?$task->client->last_name:'' }}
                </p>
                @endif
                @if ($task->user_id != null)
                <p class="m-0 p-1">
                    {{ $task->user->last_name }}</p>
                @endif
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label6 }}</h4>
                <p class="m-0 p-1">
                    @isset($task->property->property_name)
                    {{ $task->property->property_name }}
                    @endisset</p>
            </div>
            <div class="col-lg-1 col-md-1    col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label7 }}</h4>
                <p class="m-0 p-1">@isset($task->issue->issue)
                    {{ $task->issue->issue }}
                    @endisset</p>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label8 }}</h4>
                @if ($task->type == 'one_time')
                <p class="m-0 p-1">One Time</p>
                @endif
                @if ($task->type == 'recurring')
                <p class="m-0 p-1">Recurring</p>
                @endif
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label9 }}</h4>
                <br class="d-none d-md-none d-sm-block " />

                <a href="{{ route('admin.schedule.edit', $task->id) }}" class="btn btn-default" style="border:1px solid #666">Schedule</a>

            </div>
        </div>
        <?php
//                dump($task->client)
        ?>
        @endif
        @endforeach
    </div> <!-- End of ROW -->
<div class="text-right">
    {{$tasks->links()}}
</div>
</div>