
<table class="table table-bordered table-sm-styling table-sm table-striped">
    <thead>
        <tr>
            <th>Lead Date</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Services Requested</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>

        @foreach($pendingLeads as $lead)
        <tr>
            <td class="text-left">{{ date("d-m-Y",strtotime($lead->created_at)) }}</td>
            <td class="text-left">{{ $lead->first_name }}</td>
            <td class="text-left">{{ $lead->last_name }}</td>
            <td class="text-left">{{ $lead->phone_one }}</td>
            <td class="text-left">
                @if(isset($lead->services) && $lead->services->count()>0)
                @foreach($lead->services as $service)
                    <label class="label label-info">{{ $service->service }}</label>
                @endforeach
                @else
                    -- 
                @endif
            </td>
            <td class="text-center"><a href="{{ route('admin.contracts.create',$lead->id) }}" class="btn btn-primary" role="button" title="Create Contract"><i class="fa fa-file-text-o"></i></a>
                <a class="btn btn-warning" href="{{ route('admin.lead.archive',['id'=>\Crypt::encrypt($lead->id)]) }}" title="Remove Lead"><i class="fa fa-archive"></i></a>
            </td>
        </tr>
        @endforeach
</table>
<div class="text-right">
    {{ $pendingLeads->links() }}
</div>
<!--</div>-->