@php
$task_label1 = 'Scheduled Date';
$task_label2 = 'Work Order #';
$task_label3 = 'Property';
$task_label4 = 'Item';
$task_label5 = 'Action';
@endphp
<div class="container-fluid grid-wrapper p-3">
    <div class="grid-header">
        <div class="row d-none d-md-flex mb-md-2 p-md-1">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label1 }}</h4>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label2 }}</h4>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label3 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label4 }}</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <h4 class="small bold m-0">{{ $task_label5 }}</h4>
            </div>
        </div>
    </div>
    <div class="grid-body">
        @foreach ($tasks as $task)
                <div class="row mb-md-0 p-md-1 mb-3 p-1">
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label1 }}</h4>
                       
                             <?php
                        if (isset($task->scheduled_task)) {

                            $date = explode('-', $task->scheduled_task);
                            $date = $date[1] . '-' . $date[2] . '-' . $date[0];
                        }
                        ?>
                        <p class="m-0 p-1">{{ $date }}</p>

                      
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label2 }}</h4>
                        <p class="m-0 p-1">{{ $task->work_order }}</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label3 }}</h4>
                        <p class="m-0 p-1">
                            @isset($task->property->property_name)
                                {{ $task->property->property_name }}
                            @endisset
                        </p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label4 }}</h4>
                        <p class="m-0 p-1">
                            @isset($task->issue->issue)
                                {{ $task->issue->issue }}
                            @endisset
                        </p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h4 class="d-md-none d-sm-inline bold small m-0 p-0">{{ $task_label5 }}</h4>
                        <br class="d-none d-md-none d-sm-block " />
                        <a href="{{ route('admin.task.complete', $task->id) }}" class="btn btn-default"
                            style="border: 1px solid #666"><i style="padding: 1px 9px;" data-toggle="tooltip"
                                data-placement="top" title="" class="fa fa-check"
                                ></i></a>
                        @can('task-edit')
                            <a href="{{ route('admin.schedule.edit', $task->id) }}"
                                class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button"><i
                                    class="fa fa-pencil"></i></a>
                        @endcan
                    </div>
                </div>
        @endforeach
    </div> <!-- End of ROW -->
</div>
