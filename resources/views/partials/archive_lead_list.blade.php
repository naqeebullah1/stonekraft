
<table class="table table-bordered table-sm-styling table-sm table-striped">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Services Requested</th>
            <th>Lead Date</th>
        </tr>
    </thead>
    <tbody>
        @foreach($archiveLeads as $lead)
        <tr>
            <td>{{ $lead->first_name }}</td>
            <td>{{ $lead->last_name }}</td>
            <td>{{ $lead->phone_one }}</td>
            <td>
                @if(isset($lead->services) && $lead->services->count()>0)
                @foreach($lead->services as $service)
                    <label class="label label-info">{{ $service->service }}</label>
                @endforeach
                @else
                    -- 
                @endif
            </td>
            <td>{{ date("d-m-Y",strtotime($lead->created_at)) }}</td>
        </tr>
        @endforeach
</table>
<div class="text-right">
    {{ $archiveLeads->fragment('tab_id')->links() }}
</div>
<!--</div>-->