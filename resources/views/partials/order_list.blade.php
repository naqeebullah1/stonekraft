<table class="table table-bordered table-sm-styling table-sm table-striped">
    <thead>
        <tr>
            @foreach($task_labels as $i=>$label)
            <th
                <?php
                $nowrap = "";
                if ($i == 'task_description') {
                    $styling = "width:30%";
                } 
                elseif ($i == 'task_date') {
                    $styling = "max-width: 77px;";
                } 
                elseif ($i == 'first_name' || $i=="last_name") {
                    $styling = "max-width: 50px;";
                } 
                elseif ($i == 'property_name') {
                    $styling = "";
                } 
                elseif ($i == 'work_order') {
                    $nowrap = "nowrap";
                } else {
                    $styling = "max-width:120px";
                }
                ?>
                style="<?=$styling ?>" <?=$nowrap?>
                ><?php echo $label ?></th>
            @endforeach
            <?php
//            dd($task_labels);
            ?>
        </tr>
    </thead>
    <tbody>

        @foreach($tasks as $fields)
        <tr>
            <?php
            $fields = $fields->toArray();
            ?>
            @foreach($fields as $index=>$field)
            <?php
            switch ($index) {
                case "status":
                    ?>
            <!--$task_labels-->
                    <td  class="text-left" data-title="Status">
                        <i style="padding: 1px 9px;" class="rounded-circle 
                           @if($type=="SCHEDULED")
                           {{ checkScheduledStatus($field) }}
                           @endif

                           @if($type=="NOT_SCHEDULED")
                           {{ checkNotScheduledStatus($field) }}
                           @endif
                           "></i>
                    </td>
                    <?php
                    break;
                case "id":
                    ?>
                    <td  class="text-left" data-title="<?=$task_labels[$index] ?>">
                        @if($type=="NOT_SCHEDULED")
                        <a href="{{ route('admin.task.index', ['type'=> 'NOT_SCHEDULED','client_id'=>$fields['client_id'],'contract_id'=>$field]) }}" class="btn btn-default" title="Schedule" style="border:1px solid #666">Schedule</a>
                        @endif                        
                        @if($type=="SCHEDULED")
                        <a href="{{ route('admin.task.complete', $field) }}" class="btn btn-default"
                           style="border: 1px solid #666">Completed</a>
                        @can('task-edit')
                        <a href="{{ route('admin.schedule.edit', $field) }}"
                           class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button">Edit
                        </a>
                        @endcan
                        @endif                        

                        @if($type=="COMPLETED")
                        <a class="btn btn-primary btn-small" href="<?= url('backend/schedule-edit', [$fields['id'], 'COMPLETED']) ?>">
                            Edit
                        </a>
                        @endif
                        @if($type=="PENDING")
                        @can('workOrder-edit')
                        <a href="{{ route('admin.tasks.edit', $field) }}" class="btn btn-primary pt-1 pb-1"
                           style="padding-left: 24px;padding-right:24px;" role="button">
                            Approve
                        </a>
                        @endcan

                        <a href="{{ route('admin.ticket.reject', ['id' => $field]) }}"
                           class="btn btn-danger pt-1 pb-1" style="padding-left: 24px;padding-right:24px;">
                            Reject
                        </a>
                        @endif                        

                    </td>
                    <?php
                    break;
                case "description":
                    $small = substr($field, 0, 100);
                    ?>
                    <td  data-title="<?=$task_labels[$index] ?>" class="text-left">
                        {{$small}}
                        @if(strlen($field)>100)
                        <a href="javascript:" class="btn-link" data-toggle="popover" data-trigger="hover" data-content="<?= $field ?>">...</a>
                        @endif
                    </td>
                    <?php
                    break;
                case "type":
                    /* echo '<td>';
                      echo ucwords(str_replace('_', ' ', $field));
                      echo '</td>';
                     */
                    break;
                case "updated_at":
                    echo '<td data-title="'.$task_labels[$index].'">';
                    echo date('m-d-Y', strtotime($field));
                    echo '</td>';
                    break;
                case "property_id":
                    break;
                case "client_id":
                    break;
                case "p_notes":
                    break;
                case "c_notes":
                    break;
             case "last_name":
                    ?>
                    <td class="text-left">
                        <a>{{ $field }}</a>
                    </td>
                    <?php
                    break;
                case "property_name":
                    $changeBg = '';
                    if ($fields['p_notes']) {
                        $changeBg = 'background-color:#f0f8ff !important;';
                    }
                    ?>
                    <td data-title="<?=$task_labels[$index] ?>" nowrap style="{{$changeBg}}" class="text-left">
                        <a href="{{ url('backend/properties/'.$fields['property_id'].'/edit') }}">{{ $field }}</a>
                    </td>
                    <?php
                    break;
                case "created_at":
                    echo '<td data-title="'.$task_labels[$index].'">';

                    echo date('m-d-Y', strtotime($field));
                    echo '</td>';
                    break;
                case "date_month":
                    echo '<td data-title="'.$task_labels[$index].'">';
                    if ($type == 'NOT_SCHEDULED'):
                        $md = explode('-', $field);
//                        dump($field);
                        echo $md[1] . '-' . $md[0];
                    endif;
                    echo '</td>';
                    break;
                case "recurring_type":
                    echo '<td data-title="'.$task_labels[$index].'" nowrap>';
                    if ($fields['type'] == 'one_time') {
                        echo ucwords(str_replace('_', ' ', $fields['type']));
                    } else {
                        echo ucwords(strtolower($field));
                        if ($field == 'DAILY'):
                            echo " (" . $fields['every_days'] . ')';
                        endif;
                    }

                    echo '</td>';
                    break;
                case "every_days":
                    break;
                case "scheduled_task":
                    echo '<td data-title="'.$task_labels[$index].'">';
                    echo date('m-d-Y', strtotime($field))
                    . '<br>';
//                                date('h:i a', strtotime($fields['schedule_time']))
                    if ($fields['schedule_time']) {
                        echo $fields['schedule_time'] . '-' . $fields['schedule_to_time'];
                    }
//                        echo $field.'';
                    echo '</td>';
                    break;
                case "schedule_time":
                    break;
                case "schedule_to_time":
                    break;
                case "work_order":
                    ?>
                    <td data-title="<?=$task_labels[$index] ?>" nowrap>
                        @if($type=='SCHEDULED')
                        <a href="<?= url('backend/schedule-edit', $fields['id']) ?>">
                            <?= $field ?>
                        </a>
                        @elseif($type=='COMPLETED')
                        <a href="<?= url('backend/schedule-edit', [$fields['id'], 'COMPLETED']) ?>">
                            <?= $field ?>
                        </a>
                        @else
                        <a href="<?= route('admin.tasks.edit', $fields['id']) ?>">
                            <?= $field ?>
                        </a>
                        @endif
                    </td>
                    <?php
                    break;

                default:
                    echo '<td class="text-left" nowrap data-title="'.$task_labels[$index].'">';
                    echo $field;
                    echo '</td>';
                    break;
            }
            ?>

            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
<div class="text-right">
    {{$tasks->links()}}
</div>
<!--</div>-->