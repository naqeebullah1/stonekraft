/**
* 
* the validation js file contains our custom code 
* for different kinds of validations.
* you have only to apply the mentioned css class 
* on the control where you want to apply validation.
* you do not need to write extra code for validation 
* in the blade files
* 
*/

$(document).ready(function(e) {

    $(".cnic").blur(function() {
        var data = $(this).val().trim();
        $(this).val(data);
        var regex = /^[0-9+]{5}-[0-9+]{7}-[0-9]{1}$/;

        if(data !== '') {
            if (!(regex.test(data))) {
                showCustomMessage('Invalid CNIC number / format.');
            }
        }
    });


    $(".email").blur(function() {
        var data = $(this).val().trim();
        $(this).val(data);
        var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        if(data !== '') {
            if (!(regex.test(data))) {
                showCustomMessage('Invalid email / format.');
            }
        }
    });

    $(".password").blur(function() {
        var password = $(this).val();
        var passwordLength = password.length;

        var isUpperCase = 0;
        var isInteger = 0;
        var isSpecial = 0;

        var isUpper = /[A-Z]/;
        if (isUpper.test(password)) {
            isUpperCase = 1;
        }

        var isNumberic = /[+-]?\d+(?:\.\d+)?/g;
        if (isNumberic.test(password)) {
            isInteger = 1;
        }

        var specialChars = /[-!$%^&*()_+|~=`{}[:;<>?,.@#\]]/g;
        if (specialChars.test(password)) {
            isSpecial = 1;
        }

        if (passwordLength < 8) {
            showCustomMessage('Password should be of 8 characters length atleast.');

        } else if (isUpperCase === 0) {
            showCustomMessage('password must contain atleast one upper case character');
        } else if (isInteger === 0) {
            showCustomMessage('password must contain atleast one integer value');
        } else if (isSpecial === 0) {
            showCustomMessage('password must contain atleast one special character');
        }
    });


    $('.alphaonly').bind('keyup blur', function () {
        var node = $(this);
        node.val(node.val().replace(/[^a-z A-Z ]/g, ''));
    });



    $('.integeronly').keypress(function (e) {
        if (e.which != 8 && e.which != 0 && 
            (e.which < 48 || e.which > 57)) {
            return false;
        }

    });

    $('.decimal').keypress(function (e) {
        if (e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }

    });


    $(".datepicker").attr("autocomplete", "off");

});


function showCustomMessage(message) {
    swal("Message!", message, {
        buttons: {
            confirm: {
                className: 'btn btn-success'
            }
        }
    });
}


function getCurrentDate() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() * 1 + 1;
    var day = date.getDate();

    if (month < 10) {
        month = "0" + month;
    }

    if (day < 10) {
        day = "0" + day;
    }

    var currentDate = month + "-" + day + "-" + year;

    return currentDate;

}
