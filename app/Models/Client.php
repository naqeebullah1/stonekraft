<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Client extends Model implements Auditable {

    use HasFactory,
        SoftDeletes,
        \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone_one',
        'phone_two',
        'address_one',
        'address_two',
        'city',
        'state',
        'zip',
        'is_active',
        'description',
        'password',
        'reset_token',
        'reset_token_expiry'
    ];

    // public function property()
    // {
    //     return $this->belongsToMany(Property::class, 'client_properties', 'client_id', 'property_id')->withPivot(
    //         'property_id'
    //         );
    // }

    public function clientProperty() {
        return $this->hasMany(ClientProperty::class, 'client_id', 'id');
    }
    public function services() {
        return $this->hasMany(ClientService::class);
    }

    public function contract() {
        return $this->hasMany(Contract::class);
    }

//    protected static function booted() {
//        static::deleting(function ($review) {
//            $review->clientProperty()->detach();
//        });
//    }

}
