<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Property extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;
    protected $table = 'properties';

    protected $fillable = [
        'property_name',
        'address_one',
        'address_two',
        'city',
        'state',
        'zip',
        'contact_info_status',
        'contact_first_name',
        'contact_last_name',
        'contact_email',
        'phone_one',
        'phone_two',
        'description',
        'is_active'
    ];

    public function task()
    {
        return $this->hasOne(Task::class, 'property_id', 'id');
    }

    public function clientProperty()
    {
        return $this->hasMany(ClientProperty::class, 'property_id', 'id');
    }
}
