<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContractTask extends Model {

    use HasFactory;

    protected $guarded = [];

    public function issue() {
        return $this->belongsTo(Issue::class, 'issue_id', 'id');
    }
    public function worker() {
        return $this->belongsTo(User::class, 'worker_id');
    }
    public function issue_list() {
        return $this->belongsTo(IssueList::class);
    }
    public function images() {
        return $this->hasMany(ContractTaskImage::class);
    }

}
