<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model {

    use HasFactory;

    protected $guarded = [];

    public function client() {
        return $this->belongsTo(Client::class);
    }
    public function contract_tasks() {
        return $this->hasMany(ContractTask::class);
    }
    public function contract_works() {
        return $this->hasMany(ContractWork::class);
    }

}
