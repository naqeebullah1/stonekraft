<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Task extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'work_order',
        'property_id',
        'issue_id',
        'type',
        'description',
        'recurring_type',
        'escorted',
        'or_each_day',
        'week_day',
        'every_days',
        'schedule',
        'date_month', 
        'recurring_end_date',
        'is_active',
        'remaining_days',
        'scheduled_task',
        'created_by',
        'user_id',
        'client_id',
        'schedule_time',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
    public function workOrder()
    {
        return $this->belongsTo(WorkOrder::class);
    }

    public function issue()
    {
        return $this->belongsTo(Issue::class, 'issue_id', 'id')->orderBy('issue');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }
}
