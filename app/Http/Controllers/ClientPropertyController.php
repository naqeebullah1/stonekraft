<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Property;
use App\Models\ClientProperty;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientPropertyController extends Controller {

    function __construct() {
        $this->middleware('permission:client-property-list', ['only' => ['index']]);
        $this->middleware('permission:client-property-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:client-property-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:client-property-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $clientProperties = ClientProperty::select('client_properties.id', 'properties.property_name', 'clients.first_name', 'clients.last_name', 'clients.last_name', 'clients.phone_one', 'client_properties.is_active')
                ->join('properties', 'properties.id', 'client_properties.property_id')
                ->join('clients', 'clients.id', 'client_properties.client_id')
                ->orderBy('client_properties.is_active', 'desc')
                ->orderBy('properties.property_name', 'asc')
                ->orderBy('clients.last_name', 'asc');

        
        
        if ($request->first_name) {
            $clientProperties = $clientProperties->where('first_name', 'like','%'.$request->first_name.'%');
        }
        if ($request->last_name) {
            $clientProperties = $clientProperties->where('last_name', 'like','%'.$request->last_name.'%');
        }
        if ($request->property_name) {
            $clientProperties = $clientProperties->where('property_name', 'like','%'.$request->property_name.'%');
        }


        $records = 10;
        $clientProperties = $clientProperties->paginate($records);
//        $clients = Client::where('is_active', 1)->orderBy('last_name')->get();
//        $properties = Property::where('is_active', 1)->orderBy('property_name')->get();


        return view('pages.client_property.index', compact('clientProperties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $clientProperty = new ClientProperty();
        $clients = Client::where('is_active', '=', 1)->orderBy('last_name')->get();
        $properties = Property::where('is_active', '=', 1)->orderBy('property_name')->get();

        return view('pages.client_property.create', compact('clients', 'properties', 'clientProperty'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $currentDateTime = date('Y-m-d H:i:s');

        $validateDate = $request->validate([
            'property_id' => 'required',
            'client_id' => 'required',
            'is_active' => 'required',
        ]);
        $validateDate['created_at'] = $currentDateTime;

        // $result = ClientProperty::where('property_id', '=', $validateDate['property_id'])
        //     // ->where('client_id', '=', $validateDate['client_id'])
        //     ->where('is_active', '=', $validateDate['is_active'])->first();

        $message = '';
        // if ($result == null) {
        try {
            DB::beginTransaction();

            ClientProperty::create($validateDate);

            DB::commit();

            $message = 'Client-Property has been added successfully.';
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
        // } 
        // else {
        //     $message = 'Sorry! This Property already have been assigned to other client.';
        //     return redirect()->route('admin.client_properties.create')->with('message', $message);
        // }


        return redirect()->route('admin.client_properties.index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $clientProperty = ClientProperty::find($id);

        if ($clientProperty != null) {
            $clients = Client::where('is_active', '=', 1)->orderBy('last_name')->get();
            $properties = Property::where('is_active', '=', 1)->orderBy('property_name')->get();

            return view('pages.client_property.edit', compact('clientProperty', 'clients', 'properties'));
        } else {
            return redirect()->route('admin.client_properties.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $currentDateTime = date('Y-m-d H:i:s');

        $validateDate = $request->validate([
            'property_id' => 'required',
            'client_id' => 'required',
            'is_active' => 'required',
        ]);
        $validateDate['created_at'] = $currentDateTime;

        // $result = ClientProperty::where('property_id', '=', $validateDate['property_id'])
        //     ->where('client_id', '=', $validateDate['client_id'])
        //     ->where('is_active', '=', $validateDate['is_active'])->first();

        $message = '';
        // if ($result == null) {
        try {
            DB::beginTransaction();

            ClientProperty::where('id', '=', $id)->update($validateDate);

            DB::commit();

            $message = 'Client-Property has been added successfully.';
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
        // } else {
        //     $message = 'Sorry! This Property already have been assigned to other client.';
        //     return redirect()->route('admin.client_properties.create')->with('message', $message);
        // }


        return redirect()->route('admin.client_properties.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $message = '';
        try {
            DB::beginTransaction();

            $clientProperty = ClientProperty::find($id);
            $clientProperty->delete();

            DB::commit();
            $message = "Client Property has been deleted successfully.";
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->route('admin.client_properties.index')->with('message', $message);
    }

}
