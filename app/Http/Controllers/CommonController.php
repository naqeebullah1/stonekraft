<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\ClientProperty;
use App\Models\Issue;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Models\Task;
use Illuminate\Support\Facades\Mail;

class CommonController extends Controller {

    public function clientLoginIndex() {
        return view('pages.client_login.login');
    }

    public function clientDashboard() {
        if (Session::has('client_name')) {
            $issue = Issue::where('is_active', '=', 1)->get();
            $clientProperty = ClientProperty::where('client_id', '=', Session::get('client_id'))->where('is_active', '=', 1)->get();
            $client = Client::find(Session::get('client_id'));

            // $workOrder = Task::orderBy('work_order', 'desc')->first();
            // if ($workOrder == null) {
            //     $number = 1;
            //     $letters = substr($client->last_name, 0, 3);
            //     $clientAddress = $client->address_one;
            //     $workOrder = $clientAddress . '-' . $letters . '-' . $number;
            // } else {
            //     $workOrder = explode('-', $workOrder->work_order);
            //     $number = $workOrder[2];
            //     $number = $number + 1;
            //     $letters = substr($client->last_name, 0, 3);
            //     $clientAddress = $client->address_one;
            //     $workOrder = $clientAddress . '-' . $letters . '-' . $number;
            // }


            $tickets = array();
            $data = Task::where('is_active', '=', 1)->get();

            foreach ($clientProperty as $k => $v) {
                foreach ($data as $key => $value) {
                    if ($value->property_id == $v->property_id && $value->issue != null) {
                        $tickets[] = array(
                            'property' => $value->property->property_name,
                            'work_order' => $value->work_order,
                            'scheduled_task' => $value->scheduled_task,
                            'schedule' => $value->schedule,
                            'type' => $value->type,
                            'date_month' => $value->date_month,
                            'issue' => $value->issue->issue,
                        );
                    }
                }
            }

            return view('pages.client_ticket.dashboard', compact('issue', 'clientProperty', 'tickets'));
        } else {
            return redirect('/');
        }
    }

    public function clientLogin(Request $request) {
        if ($request->method() == 'POST') {
            $email = $request->email;
            $password = $request->password;
            $request->validate([
                'green_box' => 'required | in:' . config('app.name'),
            ]);

            $client = Client::where('email', $email)->first();
//dd($client);

            if ($client) {
                if ($password == $client->password) {
                    if ($client->is_active == 1) {
                        $client_full_name = $client->first_name . ' ' . $client->last_name;

                        $request->session()->put('client_name', $client_full_name);
                        $request->session()->put('client_id', $client->id);

                        return redirect()->intended('client-dashboard')->with('success', 'Logged In Successfully');
                    }
                } else {

                    return redirect()->back()->with('error', 'Password is incorrect. Please Re-check your password.');
                }
            } else {
                return redirect()->back()->with('error', 'Invalid Login Details');
            }
        }
        return redirect()->back()->with('error', 'Invalid Login Details');
    }

    public function clientLogout(Request $request) {
        $request->session()->forget('client_name');
        return redirect('/');
    }

    public function storeTicket(Request $request) {
        $currentDateTime = date('Y-m-d');
        $valdidateData = $request->validate([
            'work_order' => 'required | min:3 |max:255',
            'property_id' => 'required',
            'issue_id' => 'required',
            'description' => 'required',
        ]);

        $valdidateData['created_at'] = $currentDateTime;
        $valdidateData['schedule'] = 'PENDING';
        $valdidateData['type'] = 'one_time';
        $valdidateData['created_by'] = 'Client';
        $valdidateData['client_id'] = $request->client_id;



        try {
            DB::beginTransaction();

            Task::create($valdidateData);

            DB::commit();
            $message = 'Ticket has been submited successfully.';
            return redirect()->route('client.dashboard')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function ticketRejected(Request $request) {
        Task::where('id', '=', $request->id)->update(['schedule' => 'REJECTED']);

        return redirect()->intended('backend/dashboard')->with('success', 'Ticket has been rejected');
    }

    public function resetPassword($token) {
        // dd($token);
        if ($token) {
            // check token valid and if expired
            $client = Client::where(['reset_token' => $token])->first();
            // dd($client);
            if ($client && date('Y-m-d H:i:s', strtotime($client->reset_token_expiry)) > date('Y-m-d H:i:s', time())) {
                return view('pages.client_ticket.resetpassword', ['token' => $token]);
            } else {
                return redirect(route('client-login-page'))->with('error', 'Your reset link got expired or in-valid. Try Again');
            }
        } else {
            return redirect(route('client-login-page'));
        }
    }

    public function changePassword(Request $request) {
        // dd($request);
        if ($request->method() == "POST") {

            $client = Client::where(['reset_token' => $request->reset_token])->first();
            // dd($client);

            if ($client && date('Y-m-d H:i:s', strtotime($client->reset_token_expiry)) > date('Y-m-d H:i:s', time())) {
                $request->validate([
                    'password' => 'required|between:8,255|confirmed',
                    'password_confirmation' => 'required'
                ]);
                $request->merge(['reset_token' => Null, 'reset_token_expiry' => Null]);
                $update_pass = Client::find($client->id)->update($request->only('reset_token', 'reset_token_expiry', 'password'));
                if ($update_pass) {
                    return redirect(route('client-login-page'))->with('success', 'Password Updated Successfully');
                } else {
                    return back()->with('error', 'Some Error Occured, Please Try Again');
                }
            }
        }
        return view('pages.client_ticket.resetpassword');
    }

    public function recoverPassword(Request $request) {
        if ($request->method() == "POST") {
            // Firt check if email exist in DB
            $request->validate([
                'email' => 'required'
            ]);
            $client = Client::where(['email' => $request->email])->first();

            if ($client) {
                // Logic to create reset token, expiry datetime, send in email the reset link
                $reset_token = base64_encode(rand() . time());
                $reset_expiry = date('Y-m-d H:i:s', strtotime('+1 hours'));
                $data_update = ['reset_token' => $reset_token, 'reset_token_expiry' => $reset_expiry];

                $client_update = Client::where(['id' => $client->id])->update($data_update);


                $reset_url = url('resetPasswordClient') . "/" . $reset_token;
                // Send Email

                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                          Soleil Property Solutions
                            
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">You have
                                            requested to reset your password</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            We cannot simply send you your old password. A unique link to reset your
                                            password has been generated for you. To reset your password, click the
                                            following link and follow the instructions.
                                        </p>
                                        <a href="' . $reset_url . '"
                                            style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                            Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . strtolower(config('app.name')) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';

                //$html = "Hi " . $user->first_name." ". $user->last_name .  ", <br /><br /> Below is your password reset link. Please click it to recover your password.<br><br><a href='$reset_url'>$reset_url</a><br /><br />Thanks<br />" . env('COMPANY_NAME');

                Mail::send(array(), array(), function ($message) use ($html, $client) {
                    $message->to($client->email)
                            ->subject(config('app.name') . " Account Password Reset")
                            ->setBody($html, 'text/html');
                });

                return back()->with('success', 'Reset pwd instructions has been sent to your registered email address');
            } else {
                return back()->with('error', 'credentials do not match our records.');
            }
        }
        return view('pages.client_ticket.forgotpassword');
    }

    public function changeClientPassword(Request $request) {
        if ($request->method() == "POST") {

            if (isset($request->changepassword)) {
                $request->validate([
                    'currentpassword' => 'required',
                    'password' => 'required|between:8,255|confirmed',
                    'password_confirmation' => 'required'
                ]);
                $client = Client::find(Session::get('client_id'));
                if ($request->currentpassword == $client->password) {
                    $update_pass = Client::find(Session::get('client_id'))->update($request->only('password'));
                    if ($update_pass) {
                        return redirect('/client-dashboard')->with('success', 'Password Updated Successfully');
                    } else {
                        return back()->with('error', 'Some Error Occured, Please Try Again');
                    }
                } else {
                    return back()->with('error', 'Current password is invalid');
                }
            }
        }
        return view('pages.client_ticket.changepassword');
    }

}
