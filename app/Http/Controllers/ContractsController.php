<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Contract;
use App\Models\Issue;
use App\Models\ContractTask;
use App\Models\ContractTaskImage;
use App\Models\User;
use App\Models\ContractWork;
use Illuminate\Support\Facades\DB;
use DocuSign\eSign\Client\ApiClient;
use DocuSign\eSign\Configuration;
use DocuSign\eSign\Client\Auth\OAuthToken;
use DocuSign\eSign\Api\EnvelopesApi;
use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\Document;
use DocuSign\eSign\Model\Signer;
use DocuSign\eSign\Model\CarbonCopy;
use DocuSign\eSign\Model\SignHere;
use DocuSign\eSign\Model\Recipients;
use DocuSign\eSign\Model\Tabs;
use setasign\Fpdi\Fpdi;

class ContractsController extends Controller {

    public function index(Request $request, $clientId) {
         //dd(Session::);
        $this->updateContractStatuses();
        $clients = Contract::where('client_id', $clientId);

        if ($request->first_name) {
            $clients = $clients->where('first_name', 'like', '%' . $request->first_name . '%');
        }

        if ($request->last_name) {
            $clients = $clients->where('last_name', 'like', '%' . $request->last_name . '%');
        }
        if ($request->email) {
            $clients = $clients->where('email', 'like', '%' . $request->email . '%');
        }

        if ($request->is_active != null) {
            $clients = $clients->where('is_active', $request->is_active);
        }

        $records = 10;
        $clients = $clients->paginate($records);

        return view('contracts.index', compact('clients'))->with('i', ($request->input('page', 1) - 1) * $records);
    }

    public function allContracts(Request $request) {
        // dd($request);
        $this->updateContractStatuses();
        $clients = Contract::with('client');

        if ($request->first_name) {
            $clients = $clients->where('first_name', 'like', '%' . $request->first_name . '%');
        }

        if ($request->last_name) {
            $clients = $clients->where('last_name', 'like', '%' . $request->last_name . '%');
        }
        if ($request->email) {
            $clients = $clients->where('email', 'like', '%' . $request->email . '%');
        }

        if ($request->is_active != null) {
            $clients = $clients->where('is_active', $request->is_active);
        }

        $records = 10;
        $clients = $clients->paginate($records);

        return view('contracts.index', compact('clients'))->with('i', ($request->input('page', 1) - 1) * $records);
    }

    public function create($id) {

        $client = Client::with('services')->find($id);
        $contract = new Contract;
        return view('contracts.create', compact('client', 'contract'));
    }

    public function edit($client_id, $id) {
        $client = Client::find($client_id);
        $contract = Contract::find($id);
        $tasks = ContractTask::where('contract_id', $contract->id)->paginate(10);
        $issues = Issue::select('issue', 'id')
                ->where('is_active', 1)
                ->pluck('issue', 'id');
        return view('contracts.edit', compact('client', 'contract', 'issues','tasks'));
    }

//    contractTaskStore
    public function contractTaskStore(Request $request) {
//        dd
        try {
            DB::beginTransaction();

            $basic = [
                'contract_id' => $request->contract_id,
                'issue_id' => $request->issue_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $issue_list_ids = $request->issue_list_id;
            $data = [];
            $key = 0;
//            dd($issue_list_ids);
            if($issue_list_ids){
            foreach ($issue_list_ids as $issue_list_id):
                $issueList = \App\Models\IssueList::find($issue_list_id);

                $data[$key] = $basic;
                $data[$key]['issue_list_id'] = 1;
                $data[$key]['task_name'] = $issueList->title;
                $data[$key]['task_description'] = $issueList->description;
                $key++;
            endforeach;
            }

            $task_names = $request->task_name;
            $descriptions = $request->task_description;
            if(count($task_names)>0){
            foreach ($task_names as $k => $tn):
                $data[$key] = $basic;
                $data[$key]['issue_id'] = null;
                $data[$key]['issue_list_id'] = null;

                $data[$key]['task_name'] = $tn;
                $data[$key]['task_description'] = $descriptions[$k];
                $key++;
            endforeach;
            }
//            dd($data);
            if(count($data)>0){
                ContractTask::insert($data);
            }

            DB::commit();
            $message = 'Task has been created successfully.';
            return redirect()->back()->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function store(Request $request, $client_id) {
        //dd($request->all());

        try {

            $client = Client::find($client_id);
            DB::beginTransaction();
            $client_fields = $request->only('first_name','last_name','email','phone_one','phone_two','address_one','address_two','city','state','zip');
            $client_fields['is_active'] = 1;
            Client::where('id',$client_id)->update($client_fields);
//            $valdidateData['status'] = 1;
            $data = $request->contracts;
            $data['client_id'] = $client_id;
            $data['date_of_contract'] = formatDate($data['date_of_contract'], 'd');
            $data['date_of_template'] = formatDate($data['date_of_template'], 'd');
            $data['date_of_installation'] = formatDate($data['date_of_installation'], 'd');

            $contract = Contract::create($data);
            $lastId = $contract->id;

            $contractWorks = $request->contract_work;

            $types = $request->types;
            
            foreach ($types as $type):
                $work = $contractWorks[$type];
                $work['contract_id'] = $lastId;

                $room_types_c = count($request->get("room-type-".$type));
                $room_type = $request->get("room-type-".$type)[0];
                if($room_types_c>1){ $room_type = 3; }
                $work['room_type_id'] = $room_type;

                ContractWork::create($work);
//                dd($work);
            endforeach;
            
//            dd($scopeOfWorks);
            DB::commit();

            $contract = Contract::with('contract_works')->find($lastId);
            $result = $this->sendContract($contract,$client,$contractWorks,$types);
            Contract::where('id',$lastId)->update(['envelope_id'=>$result->envelopeId]);
            $message = 'Contract has been created successfully.';
            return redirect('backend/lead-list')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }
    public function sendContract($contract,$client,$contractWorks,$types){
            $works = [];
            if($contract->contract_works){
                $works = $contract->contract_works;
            }
            $granite = $works->where('type',1)->first();
            $cabinet = $works->where('type',2)->first();
            $flooring = $works->where('type',3)->first();
            
            $rsa_key = file_get_contents(storage_path('private.key'));



                $config = new Configuration;
                $config->setSslVerification(false); 
                $api = new ApiClient($config);
                $api->getOAuth()->setOAuthBasePath( 'account-d.docusign.com' );
                $response = $api->requestJWTUserToken(
                    'a574c265-19ab-42b8-b03d-7b67444b7e83',
                    '1a20daa7-00d4-4533-a932-220875f316c8',
                    $rsa_key,
                    'signature impersonation',
                );


                
                $access_token = $response[0]['access_token'];
                $demo_docs_path = public_path('master.pdf');
                $fpdi = new FPDI;

                
                $pdf = new FPDI;


                $count = $fpdi->setSourceFile($demo_docs_path);
                 for ($i=1; $i<=$count; $i++) {
                    $template   = $fpdi->importPage($i);
                    $size       = $fpdi->getTemplateSize($template);
                    $fpdi->AddPage($size['orientation'], array($size['width'], $size['height']));
                    $fpdi->useTemplate($template);
                    $others_text = "";
                    if(array_key_exists(4,$contractWorks)){
                        $others_text = $contractWorks[4]['others'];
                    }
                    if($i == 1){
                        
                        $c_name = $client->first_name.' '.$client->last_name;
                        $c_address = $client->address_one;
                        $c_address_two = $client->address_two;
                        $c_phone_one = $client->phone_one;
                        $c_phone_two = $client->phone_two;
                        $fpdi->SetFont("helvetica", "", 11);                        
                        $fpdi->Text(25,52,$c_name);
                        $fpdi->Text(44,36,date("m-d-Y",strtotime($contract->date_of_contract)));
                        $fpdi->Text(25,61,$c_address." , ".$c_address_two);
                        $fpdi->Text(25,69,$client->city);
                        $fpdi->Text(95,69,$client->state);
                        $fpdi->Text(145,69,$client->zip);
                        $fpdi->Text(25,80,$c_phone_one);
                        $fpdi->Text(105,80,$client->email);
                        if(count($types)>1){
                            
                        $fpdi->SetFont('arial','',10);
                        $fpdi->SetXY(10,103);
                        $fpdi->Cell(20,8,"Room",1,0);


                        $s_granite = false;
                        $s_cabinet = false;
                        $s_flooring = false;
                        $col_size_head = 56;
                        $col_size = 28;

                        if(count($types) == 1){
                            $col_size_head = 168;
                            $col_size = 84;
                        }
                        if(count($types) == 2){
                            $col_size_head = 90;
                            $col_size = 45;
                        }
                        if(in_array(1,$types)){
                            $s_granite = true;
                        }
                        if(in_array(2,$types)){
                            $s_cabinet = true;
                        }                        
                        if(in_array(3,$types)){
                            $s_flooring = true;
                        }

                               
                        if($s_granite){ $fpdi->Cell($col_size_head,8,"Counter Top",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size_head,8,"Cabinet",1,0); }
                        if($s_flooring){ $fpdi->Cell($col_size_head,8,"Flooring",1,0); }
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"Kitchen",1,0);

                        $kitchen_granite_color = (isset($granite)) ? $granite->kitchen_granite_color : ""; 
                        $kitchen_cabinet_color = (isset($cabinet)) ? $cabinet->kitchen_cabinet_color : ""; 
                        $kitchen_flooring_color = (isset($flooring)) ? $flooring->kitchen_flooring_color:""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Color",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$kitchen_granite_color,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Color",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$kitchen_cabinet_color,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Color",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$kitchen_flooring_color,1,0); }
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $kitchen_granite_finish = (isset($granite)) ? $granite->kitchen_granite_finish : ""; 
                        $kitchen_cabinet_brand = (isset($cabinet)) ? $cabinet->kitchen_cabinet_brand : ""; 
                        $kitchen_flooring_material = (isset($flooring)) ? $flooring->kitchen_flooring_material : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Finish",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$kitchen_granite_finish,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Brand",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$kitchen_cabinet_brand,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Material",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$kitchen_flooring_material,1,0); }

                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $kitchen_granite_sf = (isset($granite)) ? $granite->kitchen_granite_square_footage : ""; 
                        $kitchen_cabinet_ic = (isset($cabinet)) ? $cabinet->kitchen_cabinet_island_color : ""; 
                        $kitchen_flooring_sf = (isset($flooring)) ? $flooring->kitchen_flooring_square_footage : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Sqft",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$kitchen_granite_sf,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Island Color",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$kitchen_cabinet_ic,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Sqft",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$kitchen_flooring_sf,1,0); }

                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $kitchen_granite_sink = (isset($granite)) ? $granite->kitchen_granite_sink : ""; 
                        $kitchen_cabinet_brand = (isset($cabinet)) ? $cabinet->kitchen_cabinet_brand : ""; 
                        $kitchen_flooring_size = (isset($flooring)) ? $flooring->kitchen_flooring_size : "";

                        if($s_granite){ $fpdi->Cell($col_size,6,"Sink",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$kitchen_granite_sink,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Brand",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$kitchen_cabinet_brand,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Size",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$kitchen_flooring_size,1,0); }
                        
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $kitchen_granite_installation = (isset($granite)) ? $granite->kitchen_granite_installation : ""; 
                        $kitchen_cabinet_installation = (isset($cabinet)) ? $cabinet->kitchen_cabinet_installation : ""; 
                        $kitchen_flooring_installation = (isset($flooring)) ? $flooring->kitchen_flooring_installation : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Installation",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$kitchen_granite_installation,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Installation",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$kitchen_cabinet_installation,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Installation",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$kitchen_flooring_installation,1,0); }
                        
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $kitchen_granite_edge = (isset($granite)) ? $granite->kitchen_granite_edge : ""; 
                        $kitchen_cabinet_knobs = (isset($cabinet)) ? $cabinet->kitchen_cabinet_knobs : ""; 
                        $kitchen_flooring_trim_work = (isset($flooring)) ? $flooring->kitchen_flooring_trim_work : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Edge",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$kitchen_granite_edge,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Knobs",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$kitchen_cabinet_knobs,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Trim Work",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$kitchen_flooring_trim_work,1,0); }
                        
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $kitchen_granite_brackets = (isset($granite)) ? $granite->kitchen_granite_brackets : ""; 
                        $kitchen_flooring_grout_color = (isset($flooring)) ? $flooring->kitchen_flooring_grout_color : "";

                        if($s_granite){ $fpdi->Cell($col_size,6,"Brackets",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$kitchen_granite_brackets,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,'',1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Grout Color",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$kitchen_flooring_grout_color,1,0); }
                        
                        
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $kitchen_granite_diswasher_mount = (isset($granite)) ? $granite->kitchen_granite_dishwasher_mount : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"DW Mount",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$kitchen_granite_diswasher_mount,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,'',1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,'',1,0); }
                        
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"Bathroom",1,0);

                        $bathroom_granite_color = (isset($granite)) ? $granite->bathroom_granite_color : ""; 
                        $bathroom_cabinet_color = (isset($cabinet)) ? $cabinet->bathroom_cabinet_color : ""; 
                        $bathroom_flooring_color = (isset($flooring)) ? $flooring->bathroom_flooring_color:""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"DW Mount",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$bathroom_granite_color,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Color",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$bathroom_cabinet_color,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Color",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$bathroom_flooring_color,1,0); }
                        
                    
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $bathroom_granite_finish = (isset($granite)) ? $granite->bathroom_granite_finish : ""; 
                        $bathroom_cabinet_brand = (isset($cabinet)) ? $cabinet->bathroom_cabinet_brand: ""; 
                        $bathroom_flooring_material = (isset($flooring)) ? $flooring->bathroom_flooring_material:""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Finish",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$bathroom_granite_finish,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Brand",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$bathroom_cabinet_brand,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Material",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$bathroom_flooring_material,1,0); }
                        
                    
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);


                        $bathroom_granite_installation = (isset($granite)) ? $granite->bathroom_granite_installation : ""; 
                        $bathroom_cabinet_installation = (isset($cabinet)) ? $cabinet->bathroom_cabinet_installation : ""; 
                        $bathroom_flooring_installation = (isset($flooring)) ? $flooring->bathroom_flooring_installation : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Installation",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$bathroom_granite_installation,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Installation",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$bathroom_cabinet_installation,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Installation",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$bathroom_flooring_installation,1,0); }

                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);


                        $bathroom_granite_sf = (isset($granite)) ? $granite->bathroom_granite_square_footage : ""; 
                        $bathroom_cabinet_knobs = (isset($cabinet)) ? $cabinet->bathroom_cabinet_knobs : ""; 
                        $bathroom_flooring_sf = (isset($flooring)) ? $flooring->bathroom_flooring_square_footage : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Sqft",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$bathroom_granite_sf,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"Knobs",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$bathroom_cabinet_knobs,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Sqft",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$bathroom_flooring_sf,1,0); }
                        
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);



                        $bathroom_granite_sink = (isset($granite)) ? $granite->bathroom_granite_sink : ""; 
                        $bathroom_cabinet_special_parts = (isset($cabinet)) ? $cabinet->bathroom_cabinet_special_parts : ""; 
                        $bathroom_flooring_size = (isset($flooring)) ? $flooring->bathroom_flooring_size : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Sink",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$bathroom_granite_sink,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"SP Parts",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,$bathroom_cabinet_special_parts,1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Size",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$bathroom_flooring_size,1,0); }
                        
                    
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $bathroom_granite_edge = (isset($granite)) ? $granite->bathroom_granite_edge : ""; 
                        $bathroom_flooring_shower_remodel = (isset($flooring)) ? $flooring->bathroom_flooring_shower_remodel : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Sink",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$bathroom_granite_edge,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,'',1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Shower",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$bathroom_flooring_shower_remodel,1,0); }
                        
                    
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);

                        $bathroom_granite_nitch_window_seal = (isset($granite)) ? $granite->bathroom_granite_nitch_window_seal : ""; 
                        $bathroom_flooring_toilet_tub_remover = (isset($flooring)) ? $flooring->bathroom_flooring_toilet_tub_remover : ""; 

                        if($s_granite){ $fpdi->Cell($col_size,6,"Seal",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$bathroom_granite_nitch_window_seal,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,'',1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Toilet/Tub Remover",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$bathroom_flooring_toilet_tub_remover,1,0); }
                        
                        $fpdi->Ln();
                        $fpdi->Cell(20,6,"",1,0);
                        $bathroom_cabinet_other = (isset($granite)) ? $granite->bathroom_cabinet_other : ""; 
                        $bathroom_flooring_grout_color = (isset($flooring)) ? $flooring->bathroom_flooring_grout_color : ""; 
                        if($s_granite){ $fpdi->Cell($col_size,6,"Other",1,0); }
                        if($s_granite){ $fpdi->Cell($col_size,6,$bathroom_cabinet_other,1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,"",1,0); }
                        if($s_cabinet){ $fpdi->Cell($col_size,6,'',1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,"Grout Color",1,0); }
                        if($s_flooring){$fpdi->Cell($col_size,6,$bathroom_flooring_grout_color,1,0); }
                        }

                        if($others_text != ""){
                            $others_x = 220;
                            $others_x_label = 213;
                            if(in_array(4,$types) && count($types)==1){
                                $s_flooring = true;
                                $others_x = 110;
                                $others_x_label = 105;
                            }
                            $fpdi->SetFont("helvetica",'U', 18);
                            $fpdi->Text(10,$others_x_label,"Others:");
                            $html_tags = preg_split("/\r\n|\n|\r/", $others_text);
                            foreach($html_tags as $tag){
                                $tag_name = substr($tag,1,2);
                                $font_size = $this->tagFont($tag_name);
                                $fpdi->SetFont("helvetica", "", $font_size);    
                                $tag = str_replace("&nbsp;"," ",strip_tags($tag));
                                $inc = 5;
                                if($font_size<12){ $inc = 3; } 
                                $others_x_label = $others_x_label+$inc;
                                $fpdi->Text(10,$others_x_label,$tag);
                            }
                            $fpdi->SetFont("helvetica", "", 11);    
                        }
                    }
                    if($i == 2){
                        $f = new \NumberFormatter( locale_get_default(), \NumberFormatter::SPELLOUT );
                        $fpdi->Text(75,82,$contract->contract_price);
                        $fpdi->Text(25,90,$f->format($contract->contract_price));
                        //$fpdi->Text(155,130,$contract->sq_ft);
                        
                        $fpdi->Text(45,100,$contract->deposit);
                        $fpdi->Text(70,110,$contract->second_payment);
                        //$fpdi->Text(95,140,$f->format($contract->deposit));
                        $fpdi->Text(42,125,$contract->balance); 
                    }
                    if($i == 5){
                        $fpdi->Text(64,88,"$".$contract->contract_price);
                    }
                    if($i == 6){
                        $fpdi->Text(30,140,$c_name);
                        $fpdi->Text(30,175,$contract->second_signer_name);
                        $fpdi->Text(30,210,$contract->third_signer_name);
                    }
                }
                $outputFile = public_path($contract->id.'.pdf');
                
                $fpdi->Output($outputFile, 'F');      
                //echo $fpdi->Output($outputFile, 'I'); die;
                
                
                $content_bytes = file_get_contents($outputFile);
                $base64_file_content = base64_encode($content_bytes);
                $signer_name = $c_name;
                    $signer_email = $client->email;
                    $cc_name = $c_name;
                    $cc_email = $client->email;
                $ch = curl_init();
                $additional_signer_1 = "";
                $additional_signer_2 = "";
                if(!is_null($contract->second_signer_name) && !is_null($contract->second_signer_email)){
                $additional_signer_1 = '
                        ,{
                            "tabs": {
                                "signHereTabs": [
                                    {
                                        "documentId": "233", "pageNumber": "6",
                                        "xPosition": "70", "yPosition": "395"
                                    }
                                ],
                                "dateTabs": [
                                    {
                                        "documentId": "233", "pageNumber": "6",
                                        "xPosition": "270", "yPosition": "430"
                                    }
                                ]    
                            },
                            "email": "'.$contract->second_signer_email.'",
                            "name": "'. $contract->second_signer_name .'",
                            "recipientId": "335",
                            "routingOrder": "2",
                            "roleName": "Signer"
                        }';
                }
                if(!is_null($contract->third_signer_name) && !is_null($contract->third_signer_email)){
                $additional_signer_2 = '
                        ,{
                            "tabs": {
                                "signHereTabs": [
                                    {
                                        "documentId": "233", "pageNumber": "6",
                                        "xPosition": "70", "yPosition": "490"
                                    }
                                ],
                                "dateTabs": [
                                    {
                                        "documentId": "233", "pageNumber": "6",
                                        "xPosition": "270", "yPosition": "535"
                                    }
                                ]      
                            },
                            "email": "'.$contract->third_signer_email.'",
                            "name": "'. $contract->third_signer_name .'",
                            "recipientId": "336",
                            "routingOrder": "3",
                            "roleName": "Signer"
                        }';
                }
                $newData = '{
                "documents": [ { "documentId": "233", "name": "Contract Aggreement", "documentBase64": "'.$base64_file_content.'" } ],
                "recipients": {
                    "carbonCopies": [
                        {"recipientId": "2", "roleName": "cc", "routingOrder": "2","email": "'.$cc_email.'",
                            "name": "'. $cc_name .'",}
                    ],
                    "signers": [
                        {
                            "tabs": {
                                "signHereTabs": [
                                    {
                                        "documentId": "233", "pageNumber": "6",
                                        "xPosition": "70", "yPosition": "290"
                                    }
                                ],  
                                "textTabs": [
                                    
                                    {
                                        "documentId": "233", "pageNumber": "5",
                                        "tabLabel" : "Customer Name",
                                        "name" : "customer_name",
                                        "xPosition": "140", "yPosition": "630"
                                    },{
                                        "required" : "false",
                                        "documentId": "233", "pageNumber": "5",
                                        "tabLabel" : "Customers",
                                        "name" : "customers",
                                        "xPosition": "130", "yPosition": "650"
                                    },{
                                        "required" : "false",
                                        "documentId": "233", "pageNumber": "6",
                                        "tabLabel" : "customers",
                                        "name" : "customers_p6",
                                        "xPosition": "130", "yPosition": "200"
                                    }
                                ], 
                                "dateTabs": [
                                    {
                                        "documentId": "233", "pageNumber": "6",
                                        "xPosition": "270", "yPosition": "335"
                                    },
                                    {
                                        "documentId": "233", "pageNumber": "5",
                                        "xPosition": "340", "yPosition": "630"
                                    },
                                    {
                                        "optional" : "true",
                                        "documentId": "233", "pageNumber": "5",
                                        "xPosition": "340", "yPosition": "660"
                                    }
                                ] , 
                                "initialHereTabs": [
                                    
                                    {
                                        "documentId": "233", "pageNumber": "5",
                                        "xPosition": "60", "yPosition": "590"
                                    },
                                    {
                                        "documentId": "233", "pageNumber": "2",
                                        "xPosition": "50", "yPosition": "560"
                                    },
                                    {
                                        "documentId": "233", "pageNumber": "2",
                                        "xPosition": "50", "yPosition": "590"
                                    },
                                    {
                                        "documentId": "233", "pageNumber": "2",
                                        "xPosition": "50", "yPosition": "630"
                                    }
                                ]      
                            },
                            "email": "'.$signer_email.'",
                            "name": "'. $signer_name .'",
                            "recipientId": "334",
                            "routingOrder": "1",
                            "roleName": "Signer"
                        }
                     '.$additional_signer_1.'
                     '.$additional_signer_2.'
                    ]
                },
                
                "status": "sent",
                "emailSubject": "Contract Agreement",
            }';

            curl_setopt($ch, CURLOPT_URL, 'https://demo.docusign.net/restapi/v2/accounts/204060c1-535e-4bb5-8cb2-9758c064aa81/envelopes');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,  $newData);

            $headers = array();
            $headers[] = 'authorization: Bearer  '.$access_token;
            $headers[] = 'Accept: application/json';
            $headers[] = 'Content-Type: application/json';

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = json_decode(curl_exec($ch));
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            return $result;
    }
    public function tagFont($tag){
        switch($tag){
            case 'h1':
                return 17;
            break;
            case 'h2':
                return 16;
            break;
            case 'h3':
                return 15;
            break;
            case 'h4':
                return 14;
            break;
            case 'h5':
                return 13;
            break;
            case 'h6':
                return 12;
            break;
            default;    
               return 11;
        }
    }
    public function updateContractStatuses(){
        $rsa_key = file_get_contents(storage_path('private.key'));
        $config = new Configuration;
        $config->setSslVerification(false); 
        $api = new ApiClient($config);

        $api->getOAuth()->setOAuthBasePath( 'account-d.docusign.com' );
        $response = $api->requestJWTUserToken(
            'a574c265-19ab-42b8-b03d-7b67444b7e83',
            '1a20daa7-00d4-4533-a932-220875f316c8',
            $rsa_key,
            'signature impersonation',
        );

        $access_token = $response[0]['access_token'];
        
        $pending_contracts = Contract::where('status',0)->whereNotNull('envelope_id')->get();
        foreach($pending_contracts as $contract){
            $envelope_id = $contract->envelope_id;
            $ch = curl_init();
                $newData = '{
    
            }';            
            $url = 'https://demo.docusign.net/restapi/v2/accounts/204060c1-535e-4bb5-8cb2-9758c064aa81/envelopes/'.$envelope_id.''; 
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $headers = array();
            $headers[] = 'authorization: Bearer  '.$access_token;
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result = json_decode(curl_exec($ch));
            curl_close($ch);
            if($result->status=="completed"){
                $contract = Contract::find($contract->id);
                $contract->status =1;
                $contract->save();
            }
        }


    }  
    public function update(Request $request, $client_id, $id) {
    
//      dd($request->all());
        try {
            DB::beginTransaction();
//            $valdidateData['status'] = 1;
//            dd($id);
            $data = $request->contracts;
            $data['date_of_contract'] = formatDate($data['date_of_contract'], 'd');
            $data['date_of_template'] = formatDate($data['date_of_template'], 'd');
            $data['date_of_installation'] = formatDate($data['date_of_installation'], 'd');
            $data['status'] = 0;

            $currentDateTime = date('Y-m-d H:i:s');

            $valdidateClientData = $request->validate([
                'first_name' => 'required | min:3 |max:120',
                'last_name' => 'required | min:3 |max:120',
                'email' => 'required',
                'phone_one' => 'required | min:10 |max:20',
                'phone_two' => 'nullable | min:10 |max:20',
                'address_one' => 'required | min:3 |max:255',
                'address_two' => 'nullable | min:3 |max:255',
                'city' => 'required | min:3 |max:60',
                'zip' => 'required | min:3 |max:30'
            ]);

            $valdidateClientData['updated_at'] = $currentDateTime;
            $valdidateClientData['state'] = $request->billing_state;





            $client = Client::find($client_id);
            $client->find($client_id)->update($valdidateClientData);
            $client = Client::find($client_id);            
            $contract  = Contract::find($id);

            $contract->contract_works()->delete();
            $contract->update($data);

            $lastId = $id;

            $contractWorks = $request->contract_work;
            $types = $request->types;

            foreach ($types as $type):
                $room_types_c = count($request->get("room-type-".$type));
                $room_type = $request->get("room-type-".$type)[0];
                if($room_types_c>1){ $room_type = 3; }
                $work = $contractWorks[$type];
                $work['contract_id'] = $lastId;
                $work['room_type_id'] = $room_type;
                ContractWork::create($work);
            endforeach;
            DB::commit();
            $result = $this->sendContract($contract,$client,$contractWorks,$types);
            if($result->envelopeId){
                Contract::where('id',$lastId)->update(['envelope_id'=>$result->envelopeId]);     
                $message = 'Contract has been updated successfully.';
                return redirect('backend/contracts/allcontracts')->with('message', $message);                
            }else{
                $message = $result->message;
                return redirect()->back()->with('message', $message); 
            }
            

        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function retrieveDocument($envelope_id){
        $rsa_key = file_get_contents(storage_path('private.key'));
        $api = new ApiClient( new Configuration );
        $api->getOAuth()->setOAuthBasePath( 'account-d.docusign.com' );
        $response = $api->requestJWTUserToken(
            'a574c265-19ab-42b8-b03d-7b67444b7e83',
            '1a20daa7-00d4-4533-a932-220875f316c8',
            $rsa_key,
            'signature impersonation',
        );

        $access_token = $response[0]['access_token'];
        $ch = curl_init();
            $newData = '{

        }';
        $url = 'https://demo.docusign.net/restapi/v2/accounts/204060c1-535e-4bb5-8cb2-9758c064aa81/envelopes/'.$envelope_id.'/documents/233';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $headers = array();
        $headers[] = 'authorization: Bearer  '.$access_token;
        $headers[] = 'Content-Type: application/pdf';
        $headers[] = 'Content-Disposition: file; filename="NDA.pdf"; documentid="1';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $file_location = public_path().'/contracts/'.$envelope_id.".pdf";
        $fp = fopen($file_location, 'w+');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $result = json_decode(curl_exec($ch));
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return response()->file($file_location);

    }

    public function contractTaskUpdate(Request $request, $id) {
        try {
            DB::beginTransaction();
            $data = $request->contract_task;
            $data['task_date'] = formatDate($data['task_date'], 'd');

            $client = ContractTask::find($id)->update($data);
            if($request->images){
            foreach($request->images as $image){
                if(in_array($image->extension(),['png','jpg','jpeg'])){
                   $fname = time().".".$image->extension();
                   $image->move(public_path('contract_tasks'), $fname); 

                   ContractTaskImage::create([
                        'contract_task_id' => $id,
                        'image_link' => $fname
                   ]);
                }
            }
            }

            DB::commit();
            $message = 'Contract Task has been updated successfully.';
            return redirect()->back()->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }
    public function contractTaskAddImages(Request $request, $id) {
        try {
            DB::beginTransaction();
            $data = $request->contract_task;
            if($request->images){
            foreach($request->images as $image){
                if(in_array($image->extension(),['png','jpg','jpeg'])){
                   $fname = time().".".$image->extension();
                   $image->move(public_path('contract_tasks'), $fname); 

                   ContractTaskImage::create([
                        'contract_task_id' => $id,
                        'image_link' => $fname
                   ]);
                }
            }
            }

            DB::commit();
            $message = 'Contract Task has been updated successfully.';
            return redirect()->back()->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function contractTaskEdit($id) {
        $task = ContractTask::find($id);

        $workers = User::select('id', DB::raw('concat(first_name," ",last_name) as name'))->pluck('name', 'id');
//        dd($workers);
        return view('contracts.partials.contract_task_edit', compact('task', 'workers'));
    }

    public function contractTaskUploadImages($id) {
        $task = ContractTask::with('images')->find($id);

        $workers = User::select('id', DB::raw('concat(first_name," ",last_name) as name'))->pluck('name', 'id');
//        dd($workers);
        return view('contracts.partials.contract_task_upload_images', compact('task', 'workers'));
    }

    public function getContractTask($id) {

        $tasks = ContractTask::where('contract_id', $id)->paginate(1);
//        return 'abc';
        return view('contracts.partials.all_tasks', compact('tasks'))
                        ->render();
    }

    public function toWords($value) {
        return convertToWOrd($value);
    }

}
