<?php

namespace App\Http\Controllers;

use App\Models\Property;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Client;

class PropertyController extends Controller {

    function __construct() {
        $this->middleware('permission:property-list', ['only' => ['index']]);
        $this->middleware('permission:property-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:property-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:property-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {


        $properties = Property::orderBy('is_active', 'desc')->orderBy('property_name', 'asc');

        if ($request->property_name) {
            $properties = $properties->where('property_name', 'like', '%' . $request->property_name . '%');
        }

        if ($request->is_active != null) {
            $properties = $properties->where('is_active', $request->is_active);
        }

        $records = 10;
        $properties = $properties->paginate($records);

        return view('pages.property.index', compact('properties'))->with('i', ($request->input('page', 1) - 1) * $records);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $property = new Property();
        return view('pages.property.create', compact('property'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd('');

        $currentDateTime = date('Y-m-d H:i:s');

        $valdidateData = $request->validate([
            'property_name' => 'required | min:3 |max:120',
            'alarm_code' => 'nullable | min:3 |max:120',
            'gate_code' => 'nullable',
            'garage_code' => 'nullable',
            'key' => 'nullable',
            'access_information' => 'nullable |max:255',
            'description' => 'nullable',
        ]);

        $valdidateData['property_name'] = ucwords($valdidateData['property_name']);
        $valdidateData['created_at'] = $currentDateTime;
        // $valdidateData['alarm_code'] = Hash::make($valdidateData['alarm_code']);
        try {
            DB::beginTransaction();
            if ($request->file('contract_file')) {
                $file = $request->file('contract_file');
                $filename = date('YmdHi') . $file->getClientOriginalName();
                $file->move(public_path('uploads/property_contracts'), $filename);
                $valdidateData['contract_file'] = $filename;
            }
//            dd($valdidateData);

            Property::create($valdidateData);

            DB::commit();
            $message = 'Property has been added successfully.';
            return redirect()->route('admin.properties.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $property = Property::find($id);
        if ($property != null) {
            return view('pages.property.edit', compact('property'));
        } else {
            return redirect()->route('admin.properties.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $currentDateTime = date('Y-m-d H:i:s');

        $valdidateData = $request->validate([
            'property_name' => 'required | min:3 |max:120',
            'alarm_code' => 'nullable | min:3 |max:120',
            'gate_code' => 'nullable',
            'garage_code' => 'nullable',
            'key' => 'nullable',
            'access_information' => 'nullable | min:3 |max:255',
            'description' => 'nullable',
            'is_active' => 'nullable',
        ]);

        $valdidateData['property_name'] = ucwords($valdidateData['property_name']);
        $valdidateData['updated_at'] = $currentDateTime;



        try {
            DB::beginTransaction();

            if ($request->file('contract_file')) {
                $file = $request->file('contract_file');
                $filename = date('YmdHi') . $file->getClientOriginalName();
                $file->move(public_path('uploads/property_contracts'), $filename);
                if(strlen($filename)>255){
                     return redirect()->back()->with('error', 'File name is too large.');
                }
                $valdidateData['contract_file'] = $filename;
                
            }



            Property::where('id', '=', $id)->update($valdidateData);

            DB::commit();
            $message = 'Property has been update successfully.';
            return redirect()->route('admin.properties.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $message = '';
        try {
            DB::beginTransaction();

            $property = Property::find($id);
            $property->delete();

            DB::commit();
            $message = "Property has been deleted successfully.";
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->route('admin.properties.index')->with('message', $message);
    }

    public function deleteContractFile($id) {
//        dd('');
        $property = Property::find($id);
//        dd();
        if ($property->contract_file && file_exists(public_path() . '\uploads\property_contracts/' . $property->contract_file)) {
            unlink(public_path() . '\uploads\property_contracts/' . $property->contract_file);
        }
        return $property->update(['contract_file' => '']);
    }

}
