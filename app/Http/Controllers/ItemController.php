<?php

namespace App\Http\Controllers;

use App\Models\Issue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Models\IssueList;

class ItemController extends Controller {

    function __construct() {
        $this->middleware('permission:task-type-list', ['only' => ['index']]);
        $this->middleware('permission:task-type-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:task-type-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:task-type-delete', ['only' => ['destroy']]);
    }

    public function index() {
        $issues = Issue::orderBy('is_active', 'desc')->orderBy('issue', 'asc');

        $records = 10;
        $issues = $issues->paginate($records);

        return view('pages.issue.index', compact('issues'));
    }

    public function listsIndex($id) {
        $issue = Issue::find($id);
        $issues = IssueList::where('issue_id', $id)->orderBy('is_active', 'desc')
                ->orderBy('title', 'asc');
        $breadcrumbs = [
                [
                'title' => 'Items',
                'url' => url('backend/items'),
            ],
                [
                'title' => $issue->issue,
                'url' => '#',
            ],
        ];

        $records = 10;
        $issues = $issues->paginate($records);

        return view('pages.issue.lists.index', compact('issues', 'issue', 'breadcrumbs'));
    }

    public function create() {
        $issue = new Issue();
        return view('pages.issue.create', compact('issue'));
    }

    public function listsCreate($issueId) {
        $issue = Issue::find($issueId);
        $breadcrumbs = [
                [
                'title' => 'Items',
                'url' => url('backend/items'),
            ],
                [
                'title' => $issue->issue,
                'url' => url('backend/lists/index/' . $issueId),
            ],
                [
                'title' => 'Create',
                'url' => '#',
            ],
        ];
        $issue = new IssueList();
        return view('pages.issue.lists.create', compact('issue', 'breadcrumbs'));
    }

    public function store(Request $request) {
        $currentDateTime = date('Y-m-d');

        $valdidateData = $request->validate([
            'issue' => 'required | min:3 |max:120',
            'is_active' => 'required',
        ]);



        $valdidateData['issue'] = strtoupper($request->issue);
        $valdidateData['created_at'] = $currentDateTime;

        try {
            DB::beginTransaction();

            Issue::create($valdidateData);

            DB::commit();
            $message = 'Item has been added successfully.';
            return redirect()->route('admin.items.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function listsStore(Request $request, $issue_id) {
//        dd('yes');
        $currentDateTime = date('Y-m-d');

        $valdidateData = $request->validate([
            'title' => 'required | min:3 |max:120',
            'is_active' => 'required',
        ]);
//        dd('');
//        $valdidateData['issue'] = strtoupper($request->issue);
//        $valdidateData['created_at'] = $currentDateTime;

        try {
            DB::beginTransaction();
            $valdidateData['issue_id'] = $issue_id;
            $valdidateData['description'] = $request->description;
            IssueList::create($valdidateData);

            DB::commit();
            $message = 'Item List has been added successfully.';
            return redirect()->route('admin.lists.index', $issue_id)->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function listsUpdate(Request $request, $issue_id, $id) {
//        dd('yes');
//        $currentDateTime = date('Y-m-d');

        $valdidateData = $request->validate([
            'title' => 'required | min:3 |max:120',
            'is_active' => 'required',
        ]);
        try {
            DB::beginTransaction();
            $valdidateData['description'] = $request->description;
            IssueList::find($id)->update($valdidateData);

            DB::commit();
            $message = 'Item List has been updated successfully.';
            return redirect()->route('admin.lists.index', $issue_id)->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function edit($id) {
        $issue = Issue::find($id);

        if ($issue != '') {

            return view('pages.issue.edit', compact('issue'));
        } else {
            return redirect()->route('admin.items.index');
        }
    }

    public function listsEdit($issueId, $id) {

        $issue = Issue::find($issueId);
        $breadcrumbs = [
                [
                'title' => 'Items',
                'url' => url('backend/items'),
            ],
                [
                'title' => $issue->issue,
                'url' => url('backend/lists/index/' . $issueId),
            ],
                [
                'title' => 'Edit',
                'url' => '#',
            ],
        ];
        $issue = IssueList::find($id);

        if ($issue != '') {

            return view('pages.issue.lists.edit', compact('issue', 'breadcrumbs'));
        } else {
            return redirect()->route('admin.lists.index', $issueId);
        }
    }

    public function update(Request $request, $id) {
        $currentDateTime = date('Y-m-d');

        $valdidateData = $request->validate([
            'issue' => 'required | min:3 |max:120',
            'is_active' => 'required',
        ]);

        $valdidateData['issue'] = strtoupper($request->issue);
        $valdidateData['updated_at'] = $currentDateTime;

        try {
            DB::beginTransaction();

            Issue::where('id', '=', $id)->update($valdidateData);

            DB::commit();
            $message = 'Item has been updated successfully.';
            return redirect()->route('admin.items.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {
        $message = '';
        try {
            DB::beginTransaction();

            $taskType = Issue::find($id);
            $taskType->delete();

            DB::commit();
            $message = "Item has been deleted successfully.";
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
        return redirect()->route('admin.items.index')->with('message', $message);
        // return redirect()->back()->with('message', $message);
    }

    public function issuesList($id) {
        $tasks = IssueList::where('issue_id', $id)->get();
        return view('contracts.partials.issues_list', compact('tasks'))->render();
    }

}
