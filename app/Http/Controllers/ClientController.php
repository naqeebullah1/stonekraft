<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\ClientService;

class ClientController extends Controller {

    function __construct() {
        $this->middleware('permission:client-list', ['only' => ['index']]);
        $this->middleware('permission:client-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:client-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:client-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // dd($request);
        $clients = Client::where('is_active', '1')
                ->orderBy('last_name', 'asc');

        if ($request->first_name) {
            $clients = $clients->where('first_name', 'like', '%' . $request->first_name . '%');
        }

        if ($request->last_name) {
            $clients = $clients->where('last_name', 'like', '%' . $request->last_name . '%');
        }
        if ($request->email) {
            $clients = $clients->where('email', 'like', '%' . $request->email . '%');
        }

        if ($request->is_active != null) {
            $clients = $clients->where('is_active', $request->is_active);
        }

        $records = 10;
        $clients = $clients->paginate($records);

        return view('pages.client.index', compact('clients'))->with('i', ($request->input('page', 1) - 1) * $records);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $client = new Client();

        return view('pages.client.create', compact('client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd($request->all());
        $currentDateTime = date('Y-m-d H:i:s');

        $valdidateData = $request->validate([
            'first_name' => 'required | min:3 |max:120',
            'last_name' => 'required | min:3 |max:120',
            'email' => 'required| email|confirmed|unique:clients,email,NULL,deleted_at,deleted_at,NULL',
            'phone_one' => 'required | min:10 |max:20',
            'phone_two' => 'nullable | min:10 |max:20',
            'address_one' => 'required | min:3 |max:255',
            'address_two' => 'nullable | min:3 |max:255',
            'city' => 'required | min:3 |max:60',
            'state' => 'required',
            'zip' => 'required | min:3 |max:30',
            'description' => 'nullable',
        ]);

        $valdidateData['created_at'] = $currentDateTime;
        $valdidateData['state'] = $request->billing_state;

        try {
            DB::beginTransaction();
            $valdidateData['status'] = 1;
            $client = Client::create($valdidateData);


            if ($request->services) {
//                $client->services()->store($request->services);
                foreach ($request->services as $service) {
//                    $service
                    ClientService::create(['client_id' => $client->id, 'service' => $service]);
                }
            }

//            dd($request->services);
//            $reset_token = base64_encode(rand() . time());
//            $reset_expiry = date('Y-m-d H:i:s', strtotime('+1 hours'));
//            $data_update = ['reset_token' => $reset_token, 'reset_token_expiry' => $reset_expiry];
//            $client_update = Client::find($client->id)
//                    ->update($data_update);
            /*
              $reset_url = url('/resetPasswordClient') . "/" . $reset_token;
              $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
              style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
              <tr>
              <td>
              <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
              align="center" cellpadding="0" cellspacing="0">
              <tr>
              <td style="height:80px;">&nbsp;</td>
              </tr>
              <tr>
              <td style="text-align:center;">
              <a href="' . url('/') . '" title="logo" target="_blank">
              Soleil Property Solutions

              </a>
              </td>
              </tr>
              <tr>
              <td style="height:20px;">&nbsp;</td>
              </tr>
              <tr>
              <td>
              <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
              style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
              <tr>
              <td style="height:40px;">&nbsp;</td>
              </tr>
              <tr>
              <td style="padding:0 35px;">
              <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">You have
              requested to reset your password</h1>
              <span
              style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
              <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
              We cannot simply send you your old password. A unique link to reset your
              password has been generated for you. To reset your password, click the
              following link and follow the instructions.
              </p>
              <a href="' . $reset_url . '"
              style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
              Password</a>
              </td>
              </tr>
              <tr>
              <td style="height:40px;">&nbsp;</td>
              </tr>
              </table>
              </td>
              <tr>
              <td style="height:20px;">&nbsp;</td>
              </tr>
              <tr>
              <td style="text-align:center;">
              <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . strtolower(config('app.name')) . '.com</strong></p>
              </td>
              </tr>
              <tr>
              <td style="height:80px;">&nbsp;</td>
              </tr>
              </table>
              </td>
              </tr>
              </table>';

              Mail::send(array(), array(), function ($message) use ($html, $client) {
              $message->to($client->email)
              ->subject(config('app.name') . " Account Password Reset")
              ->setBody($html, 'text/html');
              });
             */
            DB::commit();
            $message = 'Client has been added successfully.';
            return redirect()->route('admin.clients.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $client = Client::find($id);

        if ($client != null) {
            return view('pages.client.edit', compact('client'));
        } else {
            return redirect()->route('admin.clients.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $currentDateTime = date('Y-m-d H:i:s');

        $valdidateData = $request->validate([
            'first_name' => 'required | min:3 |max:120',
            'last_name' => 'required | min:3 |max:120',
            'email' => 'required',
            'phone_one' => 'required | min:10 |max:20',
            'phone_two' => 'nullable | min:10 |max:20',
            'address_one' => 'required | min:3 |max:255',
            'address_two' => 'nullable | min:3 |max:255',
            'city' => 'required | min:3 |max:60',
            'state' => 'required',
            'zip' => 'required | min:3 |max:30',
            'description' => 'nullable',
            'is_active' => 'required',
        ]);

        $valdidateData['updated_at'] = $currentDateTime;
        $valdidateData['state'] = $request->billing_state;
        try {
            DB::beginTransaction();

            Client::find($id)->update($valdidateData);

            DB::commit();
            $message = 'Client has been updated successfully.';
            return redirect()->route('admin.clients.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $message = '';
        try {
            DB::beginTransaction();
            $client = Client::find($id);
            $client->clientProperty()->delete();
//            $client = Client::find($id);
//dd($client);
            $client->delete();
            DB::commit();
            $message = "Client has been deleted successfully.";
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->route('admin.clients.index')->with('message', $message);
    }

}
