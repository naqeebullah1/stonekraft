<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\ContractTask;
use App\Models\Client;
use App\Models\ClientProperty;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\OrderTrait;

class PagesController extends Controller {

    use OrderTrait;

    public function dashboard($type = "PENDING") {

        $getTaskWithLabels = $this->getTasks($type);
//        $fromDate = date('Y-m-d');
        $fromDate = '';
        $toDate = date('Y-m-d', strtotime('+2 months'));
//        $monthsForNotScheduled = $this->getListOfMonths($fromDate, $toDate);

        $tasks = $getTaskWithLabels['tasks'];
        if ($type == "SCHEDULED") {
            $tasks = $this->betweebFilter($tasks, $fromDate, $toDate);
        }
        $FD = '';
        if ($fromDate) {
            $FD = date('Y-m', strtotime($fromDate));
        }
        $TD = date('Y-m', strtotime($toDate));
        if ($type == "NOT_SCHEDULED") {
            $tasks = $this->checkFromAndTo($tasks, $FD, $TD);
        }

        $task_labels = $getTaskWithLabels['task_labels'];
        $tasks = $tasks->paginate(10);

        /*
         * Get Total schedule in 60 days
         */
        $scheduledCount = ContractTask::whereNotNull('task_date')->where('is_completed',0);

        $scheduledCount = $this->betweebFilter($scheduledCount, $fromDate, $toDate);
        /*
         * Get Total not_schedule in 60 days
         */
        $notScheduledCount = ContractTask::whereNull('task_date')->where('is_completed',0);
        $notScheduledCount = $this->checkFromAndTo($notScheduledCount, $FD, $TD);

        /*
         * Get Total Pendings
         */
        /*
        $pendingCount = Task::selectRaw('count(*) as count,schedule')
                ->groupBy('schedule')
                ->where('schedule', 'PENDING')
                ->pluck('count', 'schedule');
        */

        //pending leads
        $pendingLeadCount = Client::where('is_active',1)->doesntHave('contract')->count();
        $pendingLeads = Client::with('services')->where('is_active',1)->doesntHave('contract')->paginate(10);

        $archiveLeadCount = Client::where('is_active',0)->count();
        $archiveLeads = Client::with('services')->where('is_active',0)->paginate(10);

        return view('pages.dashboard', compact('tasks', 'scheduledCount', 'notScheduledCount', 'type', 'pendingLeads','pendingLeadCount','archiveLeadCount','archiveLeads', 'task_labels'));
    }

}
