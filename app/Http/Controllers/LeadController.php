<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\ClientService;
use App\Models\Property;
use App\Models\ClientProperty;

class LeadController extends Controller {

    function __construct() {
        $this->middleware('permission:client-list', ['only' => ['index']]);
        $this->middleware('permission:client-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:client-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:client-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $clients = Client::doesntHave('contract')->orderBy('last_name', 'asc');
        
        if ($request->first_name) {
            $clients = $clients->where('first_name', 'like', '%' . $request->first_name . '%');
        }

        if ($request->last_name) {
            $clients = $clients->where('last_name', 'like', '%' . $request->last_name . '%');
        }
        if ($request->email) {
            $clients = $clients->where('email', 'like', '%' . $request->email . '%');
        }

        if ($request->is_active != null) {
            $clients = $clients->where('is_active', $request->is_active);
        }

        $records = 10;
        $clients = $clients->paginate($records);

        return view('pages.lead.index', compact('clients'))->with('i', ($request->input('page', 1) - 1) * $records);
    }

    public function archive_lead($id){
        $client = Client::find(\Crypt::decrypt($id));
        $client->is_active = 0;
        if($client->save()){
            return redirect()->back();
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $client = new Client();

        return view('pages.lead.create', compact('client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $currentDateTime = date('Y-m-d H:i:s');
        $valdidateData = $request->validate([
            'first_name' => 'required | min:3 |max:120',
            'last_name' => 'required | min:3 |max:120',
            'email' => 'required| email|confirmed|unique:clients,email,NULL,deleted_at,deleted_at,NULL',
            'phone_one' => 'required | min:10 |max:20',
            'phone_two' => 'nullable | min:10 |max:20',
            'address_one' => 'required | min:3 |max:255',
            'address_two' => 'nullable | min:3 |max:255',
            'city' => 'required | min:3 |max:60',
            'state' => 'required',
            'zip' => 'required | min:3 |max:30',
            'description' => 'nullable',
        ]);

        if(is_null($request->contact_info_status)){
            $valdidateData = $request->validate([
                'first_name' => 'required | min:3 |max:120',
                'last_name' => 'required | min:3 |max:120',
                'email' => 'required| email|confirmed|unique:clients,email,NULL,deleted_at,deleted_at,NULL',
                'phone_one' => 'required | min:10 |max:20',
                'phone_two' => 'nullable | min:10 |max:20',
                'address_one' => 'required | min:3 |max:255',
                'address_two' => 'nullable | min:3 |max:255',
                'city' => 'required | min:3 |max:60',
                'state' => 'required',
                'zip' => 'required | min:3 |max:30',
                'description' => 'nullable',
                'contact_first_name' => 'required | min:3 |max:120',
                'contact_last_name' => 'required | min:3 |max:120',
                'contact_email' => 'required| email',
                'contact_phone_one' => 'required | min:10 |max:20',
                'contact_phone_two' => 'required | min:10 |max:20'
            ]);        

            $valdidateData['contact_first_name'] = $request->first_name;
            $valdidateData['contact_last_name'] = $request->last_name;
            $valdidateData['contact_email'] = $request->email;
            $valdidateData['contact_phone_one'] = $request->phone_one;
            $valdidateData['contact_phone_two'] = $request->phone_two;
        }

        $valdidateData['created_at'] = $currentDateTime;


        try {
            DB::beginTransaction();
            $valdidateData['status'] = 1;

            $reset_token = base64_encode(rand() . time());
            $reset_expiry = date('Y-m-d H:i:s', strtotime('+6 hours'));
            
            $valdidateData['reset_token'] = $reset_token;
            $valdidateData['reset_token_expiry'] = $reset_expiry;
            
            $client = Client::create($valdidateData);

            $property_fields = [
                'property_name' => '--',
                'address_one' => $request->address_one,
                'address_two'=> $request->address_one,
                'city' => $request->city,
                'state' => $request->state,
                'zip'=> $request->zip,
                'contact_info_status' => $request->has('contact_info_status')?1:0,
                'contact_first_name' => $request->contact_first_name,
                'contact_last_name' => $request->contact_last_name,
                'contact_email' => $request->contact_email,
                'phone_one' => $request->contact_phone_one,
                'phone_two' => $request->contact_phone_two
            ];
            $property = Property::create($property_fields);
            ClientProperty::create(['property_id'=>$property->id,'client_id'=>$client->id]);
            if ($request->services) {
//                $client->services()->store($request->services);
                foreach ($request->services as $service) {
//                    $service
                    ClientService::create(['client_id' => $client->id, 'service' => $service]);
                }
            }
//            dd($request->services);

            
            //$data_update = ['reset_token' => $reset_token, 'reset_token_expiry' => $reset_expiry];
            //$client_update = Client::find($client->id)->update($data_update);
            //dd(Client::find($client->id));
              $reset_url = url('/resetPasswordClient') . "/" . $reset_token;
              $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
              style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
              <tr>
              <td>
              <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
              align="center" cellpadding="0" cellspacing="0">
              <tr>
              <td style="height:80px;">&nbsp;</td>
              </tr>
              <tr>
              <td style="text-align:center;">
              <a href="' . url('/') . '" title="logo" target="_blank">
              StoneKraft

              </a>
              </td>
              </tr>
              <tr>
              <td style="height:20px;">&nbsp;</td>
              </tr>
              <tr>
              <td>
              <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
              style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
              <tr>
              <td style="height:40px;">&nbsp;</td>
              </tr>
              <tr>
              <td style="padding:0 35px;">
              <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">You have
              requested to reset your password</h1>
              <span
              style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
              <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
              We cannot simply send you your old password. A unique link to reset your
              password has been generated for you. To reset your password, click the
              following link and follow the instructions.
              </p>
              <a href="' . $reset_url . '"
              style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
              Password</a>
              </td>
              </tr>
              <tr>
              <td style="height:40px;">&nbsp;</td>
              </tr>
              </table>
              </td>
              <tr>
              <td style="height:20px;">&nbsp;</td>
              </tr>
              <tr>
              <td style="text-align:center;">
              <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . strtolower(config('app.name')) . '.com</strong></p>
              </td>
              </tr>
              <tr>
              <td style="height:80px;">&nbsp;</td>
              </tr>
              </table>
              </td>
              </tr>
              </table>';
              /*
              $is_sent = Mail::send(array(), array(), function ($message) use ($html, $client) {
              $message->to($client->email)
              ->subject(config('app.name') . " Account Password Reset")
              ->setBody($html, 'text/html');
              });
              */
             
            DB::commit();
            $message = 'Lead has been added successfully.';
            return redirect()->route('admin.leads.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $client = Client::find($id);

        if ($client != null) {
            return view('pages.lead.edit', compact('client'));
        } else {
            return redirect()->route('admin.leads.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
        $currentDateTime = date('Y-m-d H:i:s');

        $valdidateData = $request->validate([
            'first_name' => 'required | min:3 |max:120',
            'last_name' => 'required | min:3 |max:120',
            'email' => 'required',
            'phone_one' => 'required | min:10 |max:20',
            'phone_two' => 'nullable | min:10 |max:20',
            'address_one' => 'required | min:3 |max:255',
            'address_two' => 'nullable | min:3 |max:255',
            'city' => 'required | min:3 |max:60',
            'billing_state' => 'required',
            'zip' => 'required | min:3 |max:30',
            'description' => 'nullable',
            'is_active' => 'required',
        ]);

        $valdidateData['updated_at'] = $currentDateTime;
        $valdidateData['state'] = $request->billing_state;
        try {
            DB::beginTransaction();

            Client::find($id)->update($valdidateData);

            DB::commit();
            $message = 'Lead has been updated successfully.';
            return redirect()->route('admin.leads.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $message = '';
        try {
            DB::beginTransaction();
            $client = Client::find($id);
            $client->clientProperty()->delete();
//            $client = Client::find($id);
//dd($client);
            $client->delete();
            DB::commit();
            $message = "Client has been deleted successfully.";
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->route('admin.clients.index')->with('message', $message);
    }

    public function APIleadStore(Request $request) {
        //dd($request->all());
        $valdidateData = $request->all();
        try {
            DB::beginTransaction();
            $valdidateData['status'] = 1;
            $client = Client::create($valdidateData);

            $property_fields = [
                'property_name' => '--',
                'address_one' => $request->address_one,
                'address_two'=> $request->address_one,
                'city' => $request->city,
                'state' => $request->state,
                'zip'=> $request->zip,
                'contact_info_status' => $request->has('contact_info_status')?1:0,
                'contact_first_name' => $request->contact_first_name,
                'contact_last_name' => $request->contact_last_name,
                'contact_email' => $request->contact_email,
                'phone_one' => $request->contact_phone_one,
                'phone_two' => $request->contact_phone_two
            ];
            $property = Property::create($property_fields);
            ClientProperty::create(['property_id'=>$property->id,'client_id'=>$client->id]);
            if ($request->services) {
                foreach ($request->services as $service) {
//                    $service
                    ClientService::create(['client_id' => $client->id, 'service' => $service]);
                }
            }
            DB::commit();
            $message = 'Lead has been added successfully.';
            return 1;
        } catch (QueryException $e) {
            DB::rollback();
            return 0;
        }
    }    

}
