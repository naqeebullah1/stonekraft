<?php

namespace App\Http\Controllers;

use App\Models\ClientProperty;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Models\Property;
use App\Models\Client;
use App\Models\Contract;
use App\Models\Task;
use Illuminate\Http\Request;
use App\Models\Issue;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\OrderTrait;
use App\Models\ContractTask;

class TaskController extends Controller {

    use OrderTrait;

    function __construct() {
        $this->middleware('permission:workOrder-list', ['only' => ['index']]);
        $this->middleware('permission:workOrder-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:workOrder-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:workOrder-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request, $type = 'SCHEDULED') {

        $client_id = $request->get('client_id');
        $contract_id = $request->get('contract_id');

        $client = Client::find($client_id);
        $contract = Contract::find($contract_id);
        $issues = Issue::select('issue', 'id')
                ->where('is_active', 1)
                ->pluck('issue', 'id');

        $tasks = ContractTask::query();
        if ($type == "SCHEDULED") {
            $tasks = $tasks->whereNotNull('task_date')->where('is_completed',0);
        }
        if ($type == "COMPLETED") {
            $tasks = $tasks->where('is_completed',1);
        }
        if ($type == "NOT_SCHEDULED") {
            $tasks = $tasks->whereNull('task_date');
        }
        if ($type == "ALL") {

        }

        if(count(Auth::user()->roles)==1 && in_array(14,Auth::user()->roles->pluck('id')->toArray())){
            $tasks->where('worker_id',Auth::user()->id);
        }

        if ($request->ajax()) {
            return view('contracts.partials.all_tasks', compact('tasks','type'));
        }
        
        if ($request->client_id) {
            $contract_ids = Contract::where('client_id',$request->client_id)->pluck('id');
            $tasks = $tasks->whereIn('contract_id', $contract_ids);
        }
        $tasks = $tasks->paginate(10);
        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        $clients = Client::where('is_active', '=', 1)
                ->get();

       
        return view('pages.task.index', compact('tasks', 'clients', 'type', 'fromDate', 'toDate','client','contract','issues'));









        if ($request->client_id) {
            dd(Contract::where('client_id',$client_id)->get());
            $tasks = $tasks->where('property_id', $request->property_id);
        }

        if ($fromDate) {
            $fromDate = explode('-', $request->from_date);
            $fromDate = $fromDate[2] . '-' . $fromDate[0] . '-' . $fromDate[1];
            $fromDate = date('Y-m-d', strtotime($fromDate));
        }
        if ($toDate) {
            $toDate = explode('-', $request->to_date);
            $toDate = $toDate[2] . '-' . $toDate[0] . '-' . $toDate[1];
            $toDate = date('Y-m-d', strtotime($toDate));
        } else {
            $toDate = date('Y-m-d', strtotime('+2 months'));
        }
        if ($type == "SCHEDULED") {
            $tasks = $this->betweebFilter($tasks, $fromDate, $toDate);
        }
        if ($type == "NOT_SCHEDULED") {
            $FD = '';
            if ($fromDate) {
                $FD = date('Y-m', strtotime($fromDate));
            }
            $TD = date('Y-m', strtotime($toDate));
//            dd($FD);
            $tasks = $this->checkFromAndTo($tasks, $FD, $TD);
        }

//        $task_labels = $getTaskWithLabels['task_labels'];
        $clients = Client::where('is_active', '=', 1)
                ->get();

        $tasks = $tasks->paginate(10);
        return view('pages.task.index', compact('tasks', 'properties', 'type', 'task_labels', 'fromDate', 'toDate'));
    }

    public function create() {
        $properties = Property::where('is_active', '=', 1)->orderBy('property_name')->get();
        $task = new Task();
        $issues = Issue::where('is_active', '=', 1)
                ->where('is_active', 1)
                ->orderBy('issue')
                ->get();

        return view('pages.task.create', compact('properties', 'task', 'issues'));
    }

    public function show() {
        # code...
    }

    public function store(Request $request) {
//        dd($request->all());
        /*
         * date_month
         * monthly_date_month
         * annually_date_month
         * starting_date
         */
        $currentDateTime = date('Y-m-d H:i:s');

        $valdidateData = $request->validate([
            'work_order' => 'required | min:3 |max:255',
            'property_id' => 'required',
            'item' => 'required',
            'date_month' => 'nullable',
            'monthly_date_month' => 'nullable',
            'annually_date_month' => 'nullable',
            'type' => 'required',
            'description' => 'nullable',
            'item_desc' => 'required',
            'escorted' => 'nullable',
            'recurring_type' => 'nullable',
            'or_each' => 'nullable',
            'week_day' => 'nullable',
            'recurring_end_date' => 'nullable',
            'annually_recurring_end_date' => 'nullable',
            'daily_recurring_end_date' => 'nullable',
            'every_days' => 'nullable',
            'is_active' => 'required',
        ]);
        if ($request->date_month) {
            $valdidateData['date_month'] = yymm($request->date_month);
        }
        if ($request->monthly_date_month) {
            $valdidateData['monthly_date_month'] = yymm($request->monthly_date_month);
        }
        if ($request->annually_date_month) {
            $valdidateData['annually_date_month'] = yymm($request->annually_date_month);
        }



        $data = array();

        $staffName = Auth::user()->id;
        if ($valdidateData['type'] == 'one_time') {
            $data = array(
                'date_month' => $valdidateData['date_month'],
            );
        }

        $data['created_at'] = $currentDateTime;
        $data['schedule'] = 'NOT_SCHEDULED';
        $data['client_id'] = $request->client_id;
        $data['work_order'] = $valdidateData['work_order'];
        $data['property_id'] = $valdidateData['property_id'];
        $data['issue_id'] = $valdidateData['item'];
        $data['description'] = $valdidateData['description'];
        $data['item_desc'] = $valdidateData['item_desc'];
        $data['type'] = $valdidateData['type'];
        $data['created_by'] = $staffName;
        $data['is_active'] = $valdidateData['is_active'];
        $data['user_id'] = Auth::user()->id;

        $saveData = [];

        $workOrderData = [];
        $workOrderData['created_at'] = $currentDateTime;
        $workOrderData['client_id'] = $request->client_id;
        $workOrderData['property_id'] = $valdidateData['property_id'];
        $workOrderData['issue_id'] = $valdidateData['item'];
        $workOrderData['type'] = $valdidateData['type'];

        $workOrderData['is_active'] = $valdidateData['is_active'];
        $workOrderData['user_id'] = Auth::user()->id;


        if ($valdidateData['type'] == 'recurring') {
            $data['recurring_type'] = $valdidateData['recurring_type'];
            $workOrderData['recurring_type'] = $valdidateData['recurring_type'];

            if (!isset($valdidateData['escorted'])) {
                $data['escorted'] = 'No';
            } else {
                $data['escorted'] = 'Yes';
            }

            if ($valdidateData['recurring_type'] == 'MONTHLY') {
                $data['date_month'] = $valdidateData['monthly_date_month'];
                $data['or_each_day'] = $valdidateData['or_each'];
                $data['week_day'] = $valdidateData['week_day'];
                $recurringEndDate = explode('-', $valdidateData['recurring_end_date']);
            }

            if ($valdidateData['recurring_type'] == 'DAILY') {
                $data['every_days'] = $valdidateData['every_days'];
                $startingDate = explode('-', request()->starting_date);

                $data['date_month'] = $startingDate[2] . '-' . $startingDate[0] . '-' . $startingDate[1];
                $recurringEndDate = explode('-', $valdidateData['daily_recurring_end_date']);
            }

            if ($valdidateData['recurring_type'] == 'ANNUALLY') {
                $data['date_month'] = $valdidateData['annually_date_month'];
                $recurringEndDate = explode('-', $valdidateData['annually_recurring_end_date']);
            }
            if ($valdidateData['recurring_type'] == 'QUARTERLY') {
                $data['date_month'] = $valdidateData['annually_date_month'];
                $recurringEndDate = explode('-', $valdidateData['annually_recurring_end_date']);
            }
            if ($valdidateData['recurring_type'] == 'TWICE A YEAR') {
                $data['date_month'] = $valdidateData['annually_date_month'];
                $recurringEndDate = explode('-', $valdidateData['annually_recurring_end_date']);
            }
            $recurringEndDate = $recurringEndDate[2] . '-' . $recurringEndDate[0] . '-' . $recurringEndDate[1];
            $data['recurring_end_date'] = $recurringEndDate;
            $workOrderData['recurring_end_date'] = $recurringEndDate;



            /*
             * SAVING DATA
             */
            $ticketNumber = GenerateWorkOrder($valdidateData['property_id']);
//            dd($ticketNumber);
            $prefix = $ticketNumber['prefix'];
            $woNumber = $ticketNumber['wo_number'];
            $workOrderData['date_month'] = yymm($request->monthly_date_month);
            $workOrderData['recurring_end_date'] = date('Y-m-d', strtotime(str_replace('-', '/', $request->recurring_end_date)));
            try {
                DB::beginTransaction();
                $result = \App\Models\WorkOrder::create($workOrderData);
                $data['work_order_id'] = $result->id;
                DB::commit();
            } catch (QueryException $e) {
                DB::rollback();
                dd($e);
            }

            if ($valdidateData['recurring_type'] == 'MONTHLY') {
                $fromDate = $data['date_month'] . '-01';
                $toDate = $data['recurring_end_date'];
                $data['date_day'] = $request->date_day;

                if ($request->is_specific == "Yes" && !empty($request->date_day)) {
                    $data['schedule'] = 'SCHEDULED';
                    $data['or_each_day'] = '';
                    $data['week_day'] = '';
                }

                if ($request->is_specific == "No" && !empty($request->or_each) && !empty($request->week_day)) {
                    $data['schedule'] = 'SCHEDULED';
                    $data['date_day'] = '';
                }

                $occurs = $this->getListOfMonths($fromDate, $toDate);
                foreach ($occurs as $k => $occur):
                    $saveData[$k] = $data;
                    $saveData[$k]['work_order'] = $prefix . $woNumber;
                    $saveData[$k]['date_month'] = $occur['date_month'];
                    $saveData[$k]['scheduled_task'] = $occur['scheduled_task'];
                    $woNumber = str_pad(($woNumber + 1), 4, "0", STR_PAD_LEFT);
                endforeach;
            }

            if ($valdidateData['recurring_type'] == 'DAILY') {
                $fromDate = $data['date_month'];
                $toDate = $data['recurring_end_date'];
                $afterEvery = $data['every_days'];
                $occurs = dailyIntervel($fromDate, $toDate, $afterEvery . ' day');

                foreach ($occurs as $k => $occur):
                    $saveData[$k] = $data;
                    $saveData[$k]['work_order'] = $prefix . $woNumber;
                    $saveData[$k]['date_month'] = $occur;
                    $woNumber = str_pad(($woNumber + 1), 4, "0", STR_PAD_LEFT);
                endforeach;
            }

            if (in_array($valdidateData['recurring_type'], ['ANNUALLY', 'QUARTERLY', 'TWICE A YEAR'])) {
                $fromDate = $data['date_month'] . '-01';
                $toDate = $data['recurring_end_date'];
                if ($valdidateData['recurring_type'] == "ANNUALLY") {

                    $occurs = dailyIntervel($fromDate, $toDate, '1 year');
                }
                if ($valdidateData['recurring_type'] == "QUARTERLY") {
                    $occurs = dailyIntervel($fromDate, $toDate, '3 months');
                }
                if ($valdidateData['recurring_type'] == "TWICE A YEAR") {
                    $occurs = dailyIntervel($fromDate, $toDate, '6 months');
                }
                foreach ($occurs as $k => $occur):
                    $saveData[$k] = $data;
                    $saveData[$k]['work_order'] = $prefix . $woNumber;
                    $saveData[$k]['date_month'] = $occur;
                    $woNumber = str_pad(($woNumber + 1), 4, "0", STR_PAD_LEFT);
                endforeach;
            }
        }
        if (empty($saveData)) {
            $saveData = [$data];
        }
//        dd($saveData);
        try {
            DB::beginTransaction();
            $result = Task::insert($saveData);
            DB::commit();
            $message = 'Work Order has been added successfully.';
            return redirect()->route('admin.task.index', 'NOT_SCHEDULED')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function getClientInfo(Request $request) {
        $client_id = $request->client_id;
        $client = Client::find($client_id);
        return json_encode($client);
    }

    public function edit($id, $completed = null) {
//        dd($completed);
        $task = Task::find($id);
        if ($task) {
            $properties = Property::where('is_active', '=', 1)->orderBy('property_name')->get();
            $issues = Issue::where('is_active', '=', 1)
                    ->where('is_active', 1)
                    ->orderBy('issue')
                    ->get();
            $workOrder = null;
            return view('pages.task.edit', compact('properties', 'completed', 'task', 'issues', 'workOrder'));
        } else {
            return redirect()->route('admin.task.index');
        }
    }

    public function update(Request $request, $id) {
        $task = Task::find($id);
        $task_scheduled_date = $task->scheduled_task;
        $task_YY_MM = $task->date_month;
        $task_type = $task->schedule;
        $workOrderId = $task->work_order_id;

//        if ($task_type == 'NOT_SCHEDULED') {
        $tasks = Task::where('work_order_id', $task->work_order_id)
//                    ->where('schedule', 'NOT_SCHEDULED')
                ->where('date_month', '>=', $task_YY_MM)
                ->delete();
//        }

        $currentDateTime = date('Y-m-d H:i:s');

        $valdidateData = $request->validate([
            'work_order' => 'required | min:3 |max:255',
            'property_id' => 'required',
            'item' => 'required',
            'date_month' => 'nullable',
            'monthly_date_month' => 'nullable',
            'annually_date_month' => 'nullable',
            'type' => 'required',
            'description' => 'nullable',
            'item_desc' => 'required',
            'escorted' => 'nullable',
            'recurring_type' => 'nullable',
            'or_each' => 'nullable',
            'week_day' => 'nullable',
            'recurring_end_date' => 'nullable',
            'annually_recurring_end_date' => 'nullable',
            'daily_recurring_end_date' => 'nullable',
            'every_days' => 'nullable',
            'is_active' => 'required',
        ]);
        if ($request->date_month) {
            $valdidateData['date_month'] = yymm($request->date_month);
        }
        if ($request->monthly_date_month) {
            $valdidateData['monthly_date_month'] = yymm($request->monthly_date_month);
        }
        if ($request->annually_date_month) {
            $valdidateData['annually_date_month'] = yymm($request->annually_date_month);
        }



        $data = array();

        $staffName = Auth::user()->id;
        if ($valdidateData['type'] == 'one_time') {
            $data = array(
                'date_month' => $valdidateData['date_month'],
            );
        }
        $data['created_at'] = $currentDateTime;
        $data['schedule'] = 'NOT_SCHEDULED';
        $data['client_id'] = $request->client_id;
        $data['work_order'] = $valdidateData['work_order'];
        $data['property_id'] = $valdidateData['property_id'];
        $data['issue_id'] = $valdidateData['item'];
        $data['description'] = $valdidateData['description'];
        $data['item_desc'] = $valdidateData['item_desc'];
        $data['type'] = $valdidateData['type'];
        $data['created_by'] = $staffName;
        $data['is_active'] = $valdidateData['is_active'];
        $data['user_id'] = Auth::user()->id;

        $saveData = [];


        if ($valdidateData['type'] == 'recurring') {
            $data['recurring_type'] = $valdidateData['recurring_type'];

            if (!isset($valdidateData['escorted'])) {
                $data['escorted'] = 'No';
            } else {
                $data['escorted'] = 'Yes';
            }
//dd($valdidateData['or_each']);
            if ($valdidateData['recurring_type'] == 'MONTHLY') {
                $data['date_month'] = $valdidateData['monthly_date_month'];
                $data['or_each_day'] = $valdidateData['or_each'];
                $data['week_day'] = $valdidateData['week_day'];
                $recurringEndDate = explode('-', $valdidateData['recurring_end_date']);
            }

            if ($valdidateData['recurring_type'] == 'DAILY') {
                $data['every_days'] = $valdidateData['every_days'];
                $startingDate = explode('-', request()->starting_date);
                $data['date_month'] = $startingDate[2] . '-' . $startingDate[0] . '-' . $startingDate[1];
                $recurringEndDate = explode('-', $valdidateData['daily_recurring_end_date']);
            }

            if ($valdidateData['recurring_type'] == 'ANNUALLY') {
                $data['date_month'] = $valdidateData['annually_date_month'];
                $recurringEndDate = explode('-', $valdidateData['annually_recurring_end_date']);
            }
            if ($valdidateData['recurring_type'] == 'QUARTERLY') {
                $data['date_month'] = $valdidateData['annually_date_month'];
                $recurringEndDate = explode('-', $valdidateData['annually_recurring_end_date']);
            }
            if ($valdidateData['recurring_type'] == 'TWICE A YEAR') {
                $data['date_month'] = $valdidateData['annually_date_month'];
                $recurringEndDate = explode('-', $valdidateData['annually_recurring_end_date']);
            }
            $recurringEndDate = $recurringEndDate[2] . '-' . $recurringEndDate[0] . '-' . $recurringEndDate[1];
            $data['recurring_end_date'] = $recurringEndDate;

            /*
             * SAVING DATA
             */
            $ticketNumber = GenerateWorkOrder($valdidateData['property_id']);
            $prefix = $ticketNumber['prefix'];
            $woNumber = $ticketNumber['wo_number'];
            $data['work_order_id'] = $workOrderId;

            if ($valdidateData['recurring_type'] == 'MONTHLY') {
                $fromDate = $data['date_month'] . '-01';
                $toDate = $data['recurring_end_date'];
                $data['date_day'] = $request->date_day;

                if ($request->is_specific == "Yes" && !empty($request->date_day)) {
                    $data['schedule'] = 'SCHEDULED';
                    $data['or_each_day'] = '';
                    $data['week_day'] = '';
                }

                if ($request->is_specific == "No" && !empty($request->or_each) && !empty($request->week_day)) {
                    $data['schedule'] = 'SCHEDULED';
                    $data['date_day'] = '';
                }

                $occurs = $this->getListOfMonths($fromDate, $toDate);
                foreach ($occurs as $k => $occur):
                    $saveData[$k] = $data;
                    $saveData[$k]['work_order'] = $prefix . $woNumber;
                    $saveData[$k]['date_month'] = $occur['date_month'];
                    $saveData[$k]['scheduled_task'] = $occur['scheduled_task'];
                    $woNumber = str_pad(($woNumber + 1), 4, "0", STR_PAD_LEFT);
                endforeach;
            }

            if ($valdidateData['recurring_type'] == 'DAILY') {
                $fromDate = $data['date_month'];
                $toDate = $data['recurring_end_date'];
                $afterEvery = $data['every_days'];
                $occurs = dailyIntervel($fromDate, $toDate, $afterEvery . ' day');

                foreach ($occurs as $k => $occur):
                    $saveData[$k] = $data;
                    $saveData[$k]['work_order'] = $prefix . $woNumber;
                    $saveData[$k]['date_month'] = $occur;
                    $woNumber = str_pad(($woNumber + 1), 4, "0", STR_PAD_LEFT);
                endforeach;
            }

            if (in_array($valdidateData['recurring_type'], ['ANNUALLY', 'QUARTERLY', 'TWICE A YEAR'])) {
                $fromDate = $data['date_month'] . '-01';
                $toDate = $data['recurring_end_date'];
                if ($valdidateData['recurring_type'] == "ANNUALLY") {

                    $occurs = dailyIntervel($fromDate, $toDate, '1 year');
                }
                if ($valdidateData['recurring_type'] == "QUARTERLY") {
                    $occurs = dailyIntervel($fromDate, $toDate, '3 months');
                }
                if ($valdidateData['recurring_type'] == "TWICE A YEAR") {
                    $occurs = dailyIntervel($fromDate, $toDate, '6 months');
                }
                foreach ($occurs as $k => $occur):
                    $saveData[$k] = $data;
                    $saveData[$k]['work_order'] = $prefix . $woNumber;
                    $saveData[$k]['date_month'] = $occur;
                    $woNumber = str_pad(($woNumber + 1), 4, "0", STR_PAD_LEFT);
                endforeach;
            }
        }
        if (empty($saveData)) {
            $saveData = [$data];
        }
        try {
            DB::beginTransaction();
            $result = Task::insert($saveData);
            DB::commit();
            $message = 'Work Order has been added successfully.';
            return redirect()->route('admin.task.index', 'NOT_SCHEDULED')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {
        $message = '';
        try {
            DB::beginTransaction();

            $task = ContractTask::find($id);
            $task->delete();

            DB::commit();
            $message = "Work Order has been deleted successfully.";
            return redirect()->back()->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->route('admin.task.index')->with('message', $message);
    }

    public function scheduleEdit($id) {
        $task = Task::find($id);
        if (!$task) {
            return redirect()->route('admin.task.index');
        }

        $properties = Property::where('is_active', '=', 1)->get();

        return view('pages.task.schedule', compact('properties', 'task'));
    }

    public function ScheduleTaskStore(Request $request, $id) {
        $currentDateTime = date('Y-m-d H:i:s');
        $data = array();
        $record = Task::find($id);
//dd($request->all());
        $valdidateData = $request->validate([
            'work_order' => 'required | min:3 |max:255',
            'date_month' => 'nullable',
            'scheduled_task' => 'nullable',
            'escorted_name' => 'nullable',
            'schedule_time' => 'nullable',
            'schedule_to_time' => 'nullable',
        ]);

        if ($request->type == 'one_time') {
            $scheduledDate = $valdidateData['scheduled_task'];
            $scheduledDate = explode('-', $scheduledDate);
            $scheduledDate = $scheduledDate[2] . '-' . $scheduledDate[0] . '-' . $scheduledDate[1];

            $data = array(
                'work_order' => $valdidateData['work_order'],
                'scheduled_task' => $scheduledDate,
                'schedule' => 'SCHEDULED',
                'user_id' => Auth::user()->id,
            );

            if (isset($valdidateData['escorted_name']) && $request->escorted == 'Yes') {
                $data['escorted'] = $request->escorted_name;
            } else {
                $data['escorted'] = 'No';
            }
        }
        if ($request->type == 'one_time') {
            $scheduledDate = $valdidateData['scheduled_task'];
            $scheduledDate = explode('-', $scheduledDate);
            $scheduledDate = $scheduledDate[2] . '-' . $scheduledDate[0] . '-' . $scheduledDate[1];

            $data = array(
                'work_order' => $valdidateData['work_order'],
                'scheduled_task' => $scheduledDate,
                'schedule_time' => $valdidateData['schedule_time'],
                'schedule_to_time' => $valdidateData['schedule_to_time'],
                'schedule' => 'SCHEDULED',
                'user_id' => Auth::user()->id,
            );

            if (isset($valdidateData['escorted_name']) && $request->escorted == "Yes") {
                $data['escorted'] = $request->escorted_name;
            } else {
                $data['escorted'] = 'No';
            }
        }


        if ($request->type == 'recurring') {
            $scheduledDate = $valdidateData['scheduled_task'];
            $scheduledDate = explode('-', $scheduledDate);
            $scheduledDate = $scheduledDate[2] . '-' . $scheduledDate[0] . '-' . $scheduledDate[1];

            $data = array(
                'work_order' => $valdidateData['work_order'],
                'scheduled_task' => $scheduledDate,
                'schedule_time' => $valdidateData['schedule_time'],
                'schedule_to_time' => $valdidateData['schedule_to_time'],
                'schedule' => 'SCHEDULED',
                'user_id' => Auth::user()->id,
            );

            if ($record->recurring_type == 'MONTHLY') {
                $result = $this->monthly($record->or_each_day, $record->week_day, $record->date_month);
                $data['target_date'] = $result['tr_date'];
                $data['remaining_days'] = $result['remaining_days'];
            }
            if ($record->recurring_type == 'ANNUALLY') {
                $annuallyResult = $this->annually($record->date_month);
                $data['target_date'] = $annuallyResult['tr_date'];
                $data['remaining_days'] = $annuallyResult['remaining_days'];
            }

            if ($record->recurring_type == 'DAILY') {
                $result = $this->daily($record->every_days);
                $data['target_date'] = $result['tr_date'];
                $data['remaining_days'] = $result['remaining_days'];
            }

            if (isset($valdidateData['escorted_name']) && isset($request->escorted)) {
                $data['escorted'] = $request->escorted_name;
            } else {
                $data['escorted'] = 'No';
            }
        }


        $data['updated_at'] = $currentDateTime;

        try {
            DB::beginTransaction();

            Task::where('id', '=', $id)->update($data);
            DB::commit();
            $message = 'Work Order has been Scheduled successfully.';
            return redirect()->route('admin.task.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function GenerateWorkOrder(Request $request) {
        $clientProperty = ClientProperty::where('property_id', '=', $request->property_id)->first();
        if ($clientProperty != null) {
            $property = ClientProperty::where('property_id', '=', $request->property_id)->orderBy('id', 'desc')->first()->property;
            $client = ClientProperty::where('property_id', '=', $request->property_id)
                            ->orderBy('id')->first()->client;
//dd($request->property_id);
            $property = explode(" ", $property->property_name);

            $propertyCode = $property[0];
//            dump($client->last_name);
            $clientCode = substr($client->last_name, 0, 3);
//            dump($client->last_name);
//            dd($clientCode);
            $clientCode = strtoupper($clientCode);
            $task = Task::orderBy('id', 'desc')->first();

            if ($task != '') {
                $taskWorkOrder = explode("-", $task->work_order);
                $taskWorkOrder = $taskWorkOrder[2];
                $taskWorkOrder = $taskWorkOrder + 1;
            } else {
                $taskWorkOrder = 1;
            }
            $taskWorkOrder = str_pad($taskWorkOrder, 4, "0", STR_PAD_LEFT);

            $workOrder = $clientCode . '-' . $propertyCode . '-' . $taskWorkOrder;

            return json_encode(['client_id' => $client->id, 'work_order' => $workOrder]);
        } else {
            return json_encode(['client_id' => '', 'work_order' => 'Not-Assigned-client']);
        }
    }

    public function taskComplete($id) {
        try {
            DB::beginTransaction();
            $currentDateTime = date('Y-m-d H:i:s');
            ContractTask::where('id', '=', $id)->update(['is_completed' => 1, 'updated_at' => $currentDateTime]);
            DB::commit();
            $message = 'Work Order has been completed successfully.';
            return redirect()->back()->with('message', $message);
//            return redirect()->route('admin.task.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }
    public function taskInComplete($id) {
        try {
            DB::beginTransaction();
            $currentDateTime = date('Y-m-d H:i:s');
            ContractTask::where('id', '=', $id)->update(['is_completed' => 0, 'updated_at' => $currentDateTime]);
            DB::commit();
            $message = 'Work Order has been updated successfully.';
            return redirect()->back()->with('message', $message);
//            return redirect()->route('admin.task.index')->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }
    public function taskUnschedule($id) {
        try {
            DB::beginTransaction();
            $currentDateTime = date('Y-m-d H:i:s');
            ContractTask::where('id', '=', $id)->update(['is_completed' => 0,'task_date'=>NULL,'worker_id'=>NULL, 'updated_at' => $currentDateTime]);
            DB::commit();
            $message = 'Task has been Unschedule successfully.';
            return redirect()->back()->with('message', $message);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    // $futureYearMonth, $weekNumber, $weekDay
    public function monthly($week, $weekDay, $monthYear) {
        // decideComingDateAndNumberOfDays
        $weekNumber = $week;
        $weekDay = $weekDay;
        $yearyMonth = $monthYear;
        $yearyMonth = explode('-', $yearyMonth);
        $yearyMonth = $yearyMonth[1];

        $str = $yearyMonth . '' . $weekNumber . ' ' . $weekDay;

        // number of days remained between two date start
        // $currentDate = date('Y/m/d');
        // $startTimeStamp = strtotime($currentDate);
        // $endTimeStamp = strtotime('2022/08/01');
        // $timeDiff = abs($endTimeStamp - $startTimeStamp);
        // $numberDays = $timeDiff / 86400;  // 86400 seconds in one day
        // // and you might want to convert to integer
        // $numberDays = intval($numberDays);
        // dd($numberDays);
        // number of days remained between two date end
        // $currentDate = date('Y-m-d');
        // $futureYMArr = explode("-", $futureYearMonth);
        // $futureYMMysql = $futureYMArr[1] . '' . $futureYMArr[0];
        // $febMonth = 0;
        // if (1 * $futureYMArr[0] == 2) {
        //     $febMonth = 28;
        //     if ($futureYMArr[1] % 4 == 0) {
        //         $febMonth = 29;
        //     }
        // }
        // if ($febMonth > 0 && $weekNumber == "30") {
        //     //feb special case
        // }
        //SELECT  str_to_date('202241 Thursday', '%Y%U %W') as `mm`;
        //41 is the week number
        // yearWeekNumber Day name
        $qeury = "SELECT  str_to_date('$str', '%Y%U %W') as `date`;";
        $result = DB::select($qeury);

        $currentDate = date('Y/m/d');
        $trDate = $result[0]->date;

        $startTimeStamp = strtotime($currentDate);
        $endTimeStamp = strtotime($trDate);
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff / 86400;  // 86400 seconds in one day
        // and you might want to convert to integer
        $numberDays = intval($numberDays);

        $data = array(
            'remaining_days' => $numberDays,
            'tr_date' => $trDate,
        );

        return $data;
    }

    public function annually($monthYear) {
        $monthNumber = explode('-', $monthYear);
        $month_num = $monthNumber[0];
        $month_name = date("F", mktime(0, 0, 0, $month_num, 10));

        $startDateOfTheMonth = date("j, d-m-Y", strtotime("first day of $month_name"));
        $startDateOfTheMonth = explode(',', $startDateOfTheMonth);



        $startDateOfTheMonth = explode('-', $startDateOfTheMonth[1]);
        $startDateOfTheMonth = $startDateOfTheMonth[2] . '-' . $startDateOfTheMonth[1] . '-' . trim($startDateOfTheMonth[0]);
        $currentDate = date('Y/m/d');

        $startTimeStamp = strtotime($currentDate);
        $endTimeStamp = strtotime($startDateOfTheMonth);
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff / 86400;  // 86400 seconds in one day
        // and you might want to convert to integer
        $numberDays = intval($numberDays);

        $data = array(
            'remaining_days' => $numberDays,
            'tr_date' => $startDateOfTheMonth,
        );

        return $data;
    }

    public function daily($everyDay) {
        // dd($everyDay);
        $date = date("j, d-m-Y", strtotime("+$everyDay day"));
        $date = explode(',', $date);

        $date = explode('-', $date[1]);
        $date = $date[2] . '-' . $date[1] . '-' . trim($date[0]);

        $currentDate = date('Y/m/d');

        $startTimeStamp = strtotime($currentDate);
        $endTimeStamp = strtotime($date);
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff / 86400;  // 86400 seconds in one day
        // and you might want to convert to integer
        $numberDays = intval($numberDays);

        $data = array(
            'remaining_days' => $numberDays,
            'tr_date' => $date,
        );

        return $data;
    }

    /* public function getWeekNumbers(Request $request) {
      $month = $request->month;
      $monthNumber = explode('-', $month);
      $month_num = $monthNumber[0];
      $month_name = date("F", mktime(0, 0, 0, $month_num, 10));

      $startDateOfTheMonth = date("j, d-m-Y", strtotime("first day of $month_name"));
      $lastDateOfTheMonth = date("j, d-m-Y", strtotime("last day of $month_name"));

      $startDate = explode(',', $startDateOfTheMonth);
      $endDate = explode(',', $lastDateOfTheMonth);

      $monthDateStart = explode('-', $startDate[1]);
      $monthDateStart = $monthDateStart[2] . '-' . $monthDateStart[1] . '-' . trim($monthDateStart[0]);

      $monthDateEnd = explode('-', $endDate[1]);
      $monthDateEnd = $monthDateEnd[2] . '-' . $monthDateEnd[1] . '-' . trim($monthDateEnd[0]);


      $startTime = strtotime($monthDateStart);
      $endTime = strtotime($monthDateEnd);

      $weeks = array();

      while ($startTime < $endTime) {
      $weeks[] = date('W', $startTime);
      $startTime += strtotime('+1 week', 0);
      }

      return json_encode($weeks);
      } */

    public function workOrderRevert($id) {
        $task = Task::find($id);
        $task->update(['schedule' => 'SCHEDULED']);
        return redirect('backend/workOrder-list/COMPLETED')->with('message', 'Work Order reverted to scheduled successfully');
//        dd($task);
    }

}
