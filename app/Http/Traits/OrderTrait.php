<?php

namespace App\Http\Traits;

use App\Models\Task;
use App\Models\ContractTask;
use Illuminate\Support\Facades\DB;

trait OrderTrait {

    public function getTasks($type) {
        
        
        $tasks = [];

        if ($type == 'SCHEDULED') {
            $tasks = ContractTask::whereNotNull('task_date')->where('is_completed',0);
            $task_labels = [
                'status' => "",
                'issue' => "Issue",
                'task_name' => "Task Name",
                'task_description' => "Description",
                'first_name' => "Team Member F.Name",
                'last_name' => "Team Member L.Name",
                'task_date' => "Task Date",
                'id' => "Action",
            ];
            $tasks = $tasks->select('task_date as status','i.issue','task_name','task_description','w.first_name','w.last_name','task_date','contract_tasks.id')
                    ->leftJoin('users as w', 'w.id', 'contract_tasks.worker_id')
                    ->leftJoin('issues as i', 'i.id', 'contract_tasks.issue_id')
                    ->leftJoin('contracts as c', 'c.id', 'contract_tasks.contract_id')
                    ->leftJoin('clients as cl', 'cl.id', 'c.client_id');
        }



        if ($type == 'PENDING') {
            $tasks = ContractTask::whereNotNull('task_date')->where('is_completed',0);
            $task_labels = [
                'task_name' => "Task Name",
                'task_description' => "Description",
                'first_name' => "First Name",
                'last_name' => "Last Name",
                'task_date' => "Task Date",
                'id' => "Action",
            ];
            $tasks = $tasks->select('task_name','task_description','w.first_name','w.last_name','task_date','contract_tasks.id')
                    ->leftJoin('users as w', 'w.id', 'contract_tasks.worker_id');
        }

        if ($type == 'ARCHIVE') {
            $tasks = ContractTask::whereNotNull('task_date')->where('is_completed',0);
            $task_labels = [
                'task_name' => "Task Name",
                'task_description' => "Description",
                'first_name' => "First Name",
                'last_name' => "Last Name",
                'task_date' => "Task Date",
                'id' => "Action",
            ];
            $tasks = $tasks->select('task_name','task_description','w.first_name','w.last_name','task_date','contract_tasks.id')
                    ->leftJoin('users as w', 'w.id', 'contract_tasks.worker_id');
        }
        
        if ($type == 'COMPLETED') {
            $task_labels = [
                'updated_at' => 'Completed Date',
                'scheduled_task' => 'Scheduled Date',
                'recurring_type' => 'Freq',
                'work_order' => 'Work Order #',
                'issue' => 'Item',
                "description" => 'Work Order Notes',
                "id" => 'Action'
            ];
//            dd($task_labels);
            $tasks = $tasks->select(
                            DB::raw("DATE(tasks.updated_at) as updated_at"), 'scheduled_task', 'schedule_time', 'schedule_to_time', 'type', 'recurring_type', 'every_days', 'work_order', 'i.issue', 'tasks.description', 'tasks.id')
                    ->leftJoin('properties as p', 'p.id', 'tasks.property_id')
                    ->leftJoin('issues as i', 'i.id', 'tasks.issue_id');
        }
        if ($type == 'NOT_SCHEDULED') {

            $tasks = ContractTask::whereNull('task_date')->where('is_completed',0);
            $task_labels = [
                'task_name' => "Task Name",
                'first_name' => "Client First Name",
                'last_name' => "Last Name",
                'date_of_contract' => "Contract Date",
                'task_description' => "Description",
                'id' => "Action",
            ];
            $tasks = $tasks->select('task_name','cl.first_name','cl.last_name','c.date_of_contract','task_description','contract_tasks.id','c.client_id','c.id')
                    ->leftJoin('users as w', 'w.id', 'contract_tasks.worker_id')
                    ->leftJoin('contracts as c', 'c.id', 'contract_tasks.contract_id')
                    ->leftJoin('clients as cl', 'cl.id', 'c.client_id');

        }
        if ($type == "SCHEDULED") {
            $tasks = $tasks->orderBy('task_date');
        }
        if ($type == "COMPLETED") {
            $tasks = $tasks->orderBy('updated_at');
        }
        if ($type == "NOT_SCHEDULED") {
            $tasks = $tasks->orderBy('task_date');
        }
        if ($type == "ARCHIVE") {
            $tasks = $tasks->orderBy('task_date');
        }
        return ['tasks' => $tasks, 'task_labels' => $task_labels];
    }

    public function betweebFilter($query, $fromDate = null, $toDate = null) {
        if ($fromDate) {
            $query = $query->where('task_date', '>=', $fromDate);
        }
        if ($toDate) {
            $query = $query->where('task_date', '<=', $toDate);
        }
        return $query;
    }

    public function InFilter($query, $months) {
        return $query->whereIn('date_month', $months);
    }

    public function checkFromAndTo($query, $fromDate = null, $toDate = null) {
//        dd($fromDate);
        /*
        if ($fromDate) {
            $query->where('task_date', '>=', $fromDate);
        }
        if ($toDate) {
            $query->where('task_date', '<=', $toDate);
        }*/
        return $query;
    }

    public function getListOfMonths($fromDate, $toDate) {
        $start = new \DateTime($fromDate);
        $start->modify('first day of this month');
//        dd($fromDate);
        $end = new \DateTime($toDate);
        $end->modify('first day of next month');

        $interval = \DateInterval::createFromDateString('1 month');

        $period = new \DatePeriod($start, $interval, $end);

        $a = [];

        foreach ($period as $k => $dt) {
            $a[$k]['date_month'] = $dt->format("Y-m");
            $a[$k]['scheduled_task'] = '';
            if (request()->is_specific == "Yes") {
                $specificDate = request()->date_day;
                $formattedDate = $dt->format("Y-m-" . $specificDate);
                $endDayOfMonth = $dt->format("Y-m-t");
                if ($formattedDate <= $endDayOfMonth) {
                    $a[$k]['scheduled_task'] = $formattedDate;
                } else {
                    $a[$k]['scheduled_task'] = $endDayOfMonth;
                }
            }
            if (request()->is_specific == "No") {
                $onEvery=request()->or_each;
                $weekDay=request()->week_day;
                
                $string =$onEvery.' '.$weekDay;
                
                $startOfMonth = $dt->format("Y-m");
                $a[$k]['scheduled_task']=date("Y-m-d", strtotime($string." ".$startOfMonth));
            }
        }
//        dd($a);
        return $a;
    }

}
