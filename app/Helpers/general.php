<?php

use App\Models\ClientProperty;
use App\Models\Task;

function checkScheduledStatus($field) {
//    $field= $field;
    $legend = "bg-primary";
    $current = date('Y-m-d');
    $sevenDays = date('Y-m-d', strtotime('+7days'));

    if ($current > $field) {
        $legend = "bg-danger";
    }

    if ($current <= $field && $field < $sevenDays) {
        $legend = "bg-warning";
    }
    return $legend;
}

function checkNotScheduledStatus($field) {
    if ($field) {
//        $field = '01-' . $field;
//        $field = date('Y-m', strtotime($field));
    }
    $legend = "bg-primary";
    $current = date('Y-m');
    $sevenDays = date('Y-m', strtotime('+7days', strtotime(date('01-10-2022'))));

    if ($current > $field) {
        $legend = "bg-danger";
    }
    /*
     * Da ba hala conmpare kai che pa $field ki 7 ya da 7 na kami wrazi pati shi
     */
//    if ($current <= $field && $field < $sevenDays) {
    if ($current == $field) {
        $endOfMonth = date('Y-m-t');

//        $now = time(); // or your date as well
        $now = strtotime(date('Y-m-d'));
        $endOfMonth = strtotime(date('Y-m-t'));
        $datediff = $endOfMonth - $now;
        $datediff = round($datediff / (60 * 60 * 24));
//        return $datediff;
        if ($datediff > 0 && $datediff <= 7) {
            $legend = "bg-warning";
        }
    }
    return $legend;
//    return ($current .'=='. $field);
}

function formatDate($date, $type = 'u') {
    if (!$date || $date == '0000-00-00') {
        return '';
    }
    if ($type == 'u') {
//        dd(date('m-d-Y', strtotime($date)));
        return date('m/d/Y', strtotime($date));
    }
    if ($type == 'd') {
        return date("Y-m-d", strtotime($date));
    }
    if ($type == 'W') {
        return date("W", strtotime($date));
    }
}

function GenerateWorkOrder($pId) {
    $clientProperty = ClientProperty::where('property_id', '=', $pId)->first();

    if ($clientProperty != null) {
        $property = ClientProperty::where('property_id', '=', $pId)
                        ->orderBy('id', 'desc')
                        ->first()->property;

        $client = ClientProperty::where('property_id', '=', $pId)
                        ->orderBy('id')
                        ->first()->client;

        $property = explode(" ", $property->property_name);

        $propertyCode = $property[0];
//            dump($client->last_name);
        $clientCode = substr($client->last_name, 0, 3);

        $clientCode = strtoupper($clientCode);
        $task = Task::orderBy('id', 'desc')->first();

        if ($task != '') {
            $taskWorkOrder = explode("-", $task->work_order);
            $taskWorkOrder = $taskWorkOrder[2];
            $taskWorkOrder = $taskWorkOrder + 1;
        } else {
            $taskWorkOrder = 1;
        }
        $taskWorkOrder = str_pad($taskWorkOrder, 4, "0", STR_PAD_LEFT);


        $workOrderPrefix = $clientCode . '-' . $propertyCode . '-';
        $workOrderNumber = $taskWorkOrder;
        return ['prefix' => $workOrderPrefix, 'wo_number' => $workOrderNumber];
    } else {
//            $message = 'Not-Assigned-client';
        return json_encode(['client_id' => '', 'work_order' => 'Not-Assigned-client']);
//            return json_encode($message);
    }
}

function dailyIntervel($from, $to, $intervel) {
    $begin = new DateTime($from);
    $end = new DateTime($to);

    $interval = DateInterval::createFromDateString($intervel);
    $period = new DatePeriod($begin, $interval, $end);
    $a = [];
    foreach ($period as $dt) {
        $a[] = $dt->format("Y-m");
    }
    return $a;
}

function yymm($string) {
    if ($string) {
        $string = explode('-', $string);
        return $string[1] . '-' . $string[0];
    } else {
        return "";
    }
}

function mmyy($string) {
    if ($string) {

        $string = explode('-', $string);
        return $string[1] . '-' . $string[0];
    } else {
        return "";
    }
}

function DS() {
    return DIRECTORY_SEPARATOR;
}

function contractTypes() {
    return [
        1 => 'Counter Top',
        2 => 'Cabinet',
        3 => 'Flooring',
        4 => 'Others'
    ];
}

function setPaginationUrl($url) {
    return $url;
}

function convertToWOrd($num = '') {
    $num = (string) ( (int) $num );

    if ((int) ( $num ) && ctype_digit($num)) {
        $words = array();

        $num = str_replace(array(',', ' '), '', trim($num));

        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
            'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
            'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen');

        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty',
            'seventy', 'eighty', 'ninety', 'hundred');

        $list3 = array('', 'thousand', 'million', 'billion', 'trillion',
            'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion',
            'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion',
            'octodecillion', 'novemdecillion', 'vigintillion');

        $num_length = strlen($num);
        $levels = (int) ( ( $num_length + 2 ) / 3 );
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);

        foreach ($num_levels as $num_part) {
            $levels--;
            $hundreds = (int) ( $num_part / 100 );
            $hundreds = ( $hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ( $hundreds == 1 ? '' : 's' ) . ' ' : '' );
            $tens = (int) ( $num_part % 100 );
            $singles = '';

            if ($tens < 20) {
                $tens = ( $tens ? ' ' . $list1[$tens] . ' ' : '' );
            } else {
                $tens = (int) ( $tens / 10 );
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ( $num_part % 10 );
                $singles = ' ' . $list1[$singles] . ' ';
            } $words[] = $hundreds . $tens . $singles . ( ( $levels && (int) ( $num_part ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        } $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }

        $words = implode(', ', $words);

        $words = trim(str_replace(' ,', ',', ucwords($words)), ', ');
        if ($commas) {
            $words = str_replace(',', ' and', $words);
        }

        return $words.'/-';
    } else if (!( (int) $num )) {
        return 'Zero';
    }
    return '';
}
