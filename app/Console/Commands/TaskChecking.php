<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Task;
use Illuminate\Support\Facades\DB;

class TaskChecking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:checking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command will check task remaining day.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        $tasks = Task::where('schedule', '=', 'SCHEDULED')->get();
        $currentDate = date('Y-m-d');

        foreach ($tasks as $key => $task) {

            if ($task->recurring_end_date != $currentDate) {
                if ($task->target_date == $currentDate) {
                    if ($task->recurring_type == 'MONTHLY') {
                        $result = $this->monthly($task->or_each_day, $task->week_day, $task->date_month);
                        $data = array(
                            'target_date' => $result['tr_date'],
                            'remaining_days' => $result['remaining_days'],
                        );

                        Task::where('id', '=', $task->id)->update($data);
                    }
                    if ($task->recurring_type == 'DAILY') {
                        $result = $this->daily($task->every_days);
                        $data = array(
                            'target_date' => $result['tr_date'],
                            'remaining_days' => $result['remaining_days'],
                        );
                        Task::where('id', '=', $task->id)->update($data);
                    }
                    if ($task->recurring_type == 'ANNUALLY') {
                        $result = $this->annually($task->date_month);
                        $data = array(
                            'target_date' => $result['tr_date'],
                            'remaining_days' => $result['remaining_days'],
                        );
                        Task::where('id', '=', $task->id)->update($data);
                    }
                } else {
                    if ($task->remaining_days != 0) {
                        Task::where('id', '=', $task->id)->decrement('remaining_days');
                    }
                }
            }
        }
    }

    public function monthly($week, $weekDay, $monthYear)
    {
        // decideComingDateAndNumberOfDays
        $weekNumber = $week;
        $weekDay = $weekDay;
        $yearyMonth = $monthYear;
        $yearyMonth = explode('-', $yearyMonth);
        $yearyMonth = $yearyMonth[1];

        $str = $yearyMonth . '' . $weekNumber . ' ' . $weekDay;

        $qeury = "SELECT  str_to_date('$str', '%Y%U %W') as `date`;";
        $result  = DB::select($qeury);

        $currentDate = date('Y/m/d');
        $trDate = $result[0]->date;

        $startTimeStamp = strtotime($currentDate);
        $endTimeStamp = strtotime($trDate);
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff / 86400;  // 86400 seconds in one day
        // and you might want to convert to integer
        $numberDays = intval($numberDays);

        $data = array(
            'remaining_days' => $numberDays,
            'tr_date' => $trDate,
        );

        return $data;
    }

    public function annually($monthYear)
    {
        $monthNumber = explode('-', $monthYear);
        $month_num = $monthNumber[0];
        $month_name = date("F", mktime(0, 0, 0, $month_num, 10));

        $startDateOfTheMonth = date("j, d-m-Y", strtotime("first day of $month_name"));
        $startDateOfTheMonth = explode(',', $startDateOfTheMonth);



        $startDateOfTheMonth = explode('-', $startDateOfTheMonth[1]);
        $startDateOfTheMonth = $startDateOfTheMonth[2] . '-' . $startDateOfTheMonth[1] . '-' . trim($startDateOfTheMonth[0]);
        $currentDate = date('Y/m/d');

        $startTimeStamp = strtotime($currentDate);
        $endTimeStamp = strtotime($startDateOfTheMonth);
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff / 86400;  // 86400 seconds in one day
        // and you might want to convert to integer
        $numberDays = intval($numberDays);

        $data = array(
            'remaining_days' => $numberDays,
            'tr_date' => $startDateOfTheMonth,
        );

        return $data;
    }

    public function daily($everyDay)
    {
        // dd($everyDay);
        $date = date("j, d-m-Y", strtotime("+$everyDay day"));
        $date = explode(',', $date);

        $date = explode('-', $date[1]);
        $date = $date[2] . '-' . $date[1] . '-' . trim($date[0]);

        $currentDate = date('Y/m/d');

        $startTimeStamp = strtotime($currentDate);
        $endTimeStamp = strtotime($date);
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff / 86400;  // 86400 seconds in one day
        // and you might want to convert to integer
        $numberDays = intval($numberDays);

        $data = array(
            'remaining_days' => $numberDays,
            'tr_date' => $date,
        );

        return $data;
    }
}
