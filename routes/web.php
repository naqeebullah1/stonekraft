<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:clear');
    dd(Artisan::output());
});

// Route::get('/', 'CommonController@clientLoginIndex')->name('client-login-page');
// Route::post('/client-login', 'CommonController@clientLogin')->name('client.login');
// Route::get('/client-dashboard', 'CommonController@clientDashboard')->name('client.dashboard');
// Route::post('/logout', 'CommonController@clientLogout')->name('client.logout');
// Route::post('/store-ticket', 'CommonController@storeTicket')->name('store.ticket');
// Route::get('/pending-ticket', 'CommonController@getPendingTicket')->name('pending.ticket');
// Route::post('/set-password', 'CommonController@changePassword')->name('set-password');
//Before authentication
Route::group(['middleware' => 'beforeAuth'], function () {
    Route::get('/login', 'Admin\UsersController@login');
    Route::get('/backend', 'Admin\UsersController@login');
    Route::get('backend/login', 'Admin\UsersController@login')->name('backend.login');
    Route::get('backend/forgotPassword', 'Admin\UsersController@recoverPassword');
    Route::get('backend/resetPassword/{token}', 'Admin\UsersController@resetPassword');


    Route::post('login', 'Admin\UsersController@login');
    Route::post('recoverPassword', 'Admin\UsersController@recoverPassword');

    //client routes
    Route::get('/', 'CommonController@clientLoginIndex')->name('client-login-page');
    Route::post('/client-login', 'CommonController@clientLogin')->name('client.login');
    Route::get('/client-dashboard', 'CommonController@clientDashboard')->name('client.dashboard');
    Route::post('/logout', 'CommonController@clientLogout')->name('client.logout');
    Route::post('/store-ticket', 'CommonController@storeTicket')->name('store.ticket');
    Route::get('/pending-ticket', 'CommonController@getPendingTicket')->name('pending.ticket');

    Route::post('/set-password', 'CommonController@changePassword')->name('set-password');

    Route::get('change-password', 'CommonController@changeClientPassword');
    Route::post('changeClientOldPassword', 'CommonController@changeClientPassword');

    Route::get('resetPasswordClient/{token}', 'CommonController@resetPassword');
    Route::get('client-forgotPassword', 'CommonController@recoverPassword');
    Route::post('client-recoverPassword', 'CommonController@recoverPassword');

    Route::get('client-generate-work-order', 'TaskController@GenerateWorkOrder')->name('client.generate.work.order');
});


//After authentication with prefix:backend
Route::prefix('backend')->name('admin.')->middleware('checkAuth')->group(function () {
    Route::resource('users', 'Admin\UsersController');
    Route::match(['get', 'post'], '/users-list', 'Admin\UsersController@index')->name('users.index');

    Route::resource('roles', 'Admin\RolesController');
    Route::get('user-profile', 'Admin\UsersController@profileEdit');
    Route::get('reset-password-manually/{email}', 'Admin\UsersController@resetpasswordmanually');
    Route::get('change-password', 'Admin\UsersController@changePassword');
    Route::get('dashboard/{type?}', 'PagesController@dashboard');
    Route::post('logout', 'Admin\UsersController@logout')->name('logout');
    Route::patch('user-profile', 'Admin\UsersController@updateProfile')->name('user.profile');

    Route::resource('properties', 'PropertyController');
    Route::match(['get', 'post'], '/property-list', 'PropertyController@index')->name('property.index');

    Route::resource('clients', 'ClientController');
    Route::match(['get', 'post'], '/client-list', 'ClientController@index')->name('client.index');

    Route::resource('client_properties', 'ClientPropertyController');
    Route::match(['get', 'post'], '/client-property-list', 'ClientPropertyController@index')->name('client_properties.index');


    Route::resource('leads', 'LeadController');
    Route::get('/arcive-lead/{id?}', 'LeadController@archive_lead')->name('lead.archive');
    Route::match(['get', 'post'], '/lead-list', 'LeadController@index')->name('lead.index');


    // Route::get('tasks', 'TaskController@index')->name('task.index');
    // Route::post('tasks-check', 'TaskController@index')->name('task.check');
    Route::match(['get', 'post'], '/task-list/{type?}', 'TaskController@index')->name('task.index');
    Route::get('get-client', 'TaskController@getClientInfo')->name('get.client.info');
    Route::resource('tasks', 'TaskController');
    Route::get('tasks/{id}/edit/{completed?}', 'TaskController@edit')->name('tasks.edit');
    Route::get('workOrder-revert/{id}', 'TaskController@workOrderRevert');

    // Route::get('task-create', 'TaskController@taskCreate')->name('task.create');
    // Route::post('task-store', 'TaskController@taskStore')->name('task.store');
    Route::delete('task-delete/{id}', 'TaskController@destroy')->name('task.delete');
    // Route::get('task-edit/{id}', 'TaskController@edit')->name('task.edit');
    // Route::post('task-update/{id}', 'TaskController@update')->name('task.update');
    Route::get('schedule-edit/{id}/{completed?}', 'TaskController@scheduleEdit')->name('schedule.edit');
    Route::post('schedule-task-store/{id}', 'TaskController@ScheduleTaskStore')->name('schedule.task.store');
    Route::get('task-complete/{id}', 'TaskController@taskComplete')->name('task.complete');
    Route::get('task-incomplete/{id}', 'TaskController@taskInComplete')->name('task.incomplete');
    Route::get('task-unschedule/{id}', 'TaskController@taskUnschedule')->name('task.unschedule');
    Route::resource('items', 'ItemController');

    //Route::get('issue', 'IssueController@index')->name('issue.index');
    //Route::get('issue-create', 'IssueController@create')->name('issue.create');
    //Route::post('issue-store', 'IssueController@store')->name('issue.store');
    //Route::delete('issue-delete/{id}', 'IssueController@destroy')->name('issue.delete');
    //Route::get('issue-edit/{id}', 'IssueController@edit')->name('issue.edit');
    //Route::patch('issue-update/{id}', 'IssueController@update')->name('issue.update');

    Route::get('month-numbers', 'TaskController@getWeekNumbers')->name('get.month.numbers');

    Route::get('days-date', 'TaskController@decideComingDateAndNumberOfDays')->name('get.days.date');

    Route::get('reject-ticket', 'CommonController@ticketRejected')->name('ticket.reject');

    Route::get('generate-work-order', 'TaskController@GenerateWorkOrder')->name('generate.work.order');
    Route::get('delete-contract-file/{id}', 'PropertyController@deleteContractFile');

    Route::get('contracts/index/{client_id?}', 'ContractsController@index')->name('contracts.index');
    Route::get('contracts/allcontracts', 'ContractsController@allContracts')->name('contracts.allcontracts'); 
    Route::get('contracts/create/{client_id?}', 'ContractsController@create')->name('contracts.create');
    Route::post('contracts/store/{client_id?}', 'ContractsController@store')->name('contracts.store');

    Route::get('contracts/edit/{client_id?}/{id?}', 'ContractsController@edit')->name('contracts.edit');
    Route::put('contracts/update/{client_id?}/{id?}', 'ContractsController@update')->name('contracts.update');

    Route::get('retrieve_doc/{envelope_id?}', 'ContractsController@retrieveDocument')->name('retrieve_document');
    Route::get('update_contract_statuses', 'ContractsController@updateContractStatuses')->name('update_contract_statuses');

    Route::get('lists/index/{issue_id}', 'ItemController@listsIndex')->name('lists.index');

    Route::get('lists/create/{issue_id}', 'ItemController@listsCreate')->name('lists.create');
    Route::get('load/issue-list/{issue_id}', 'ItemController@issuesList');
    Route::get('lists/edit/{issue_id}/{id}', 'ItemController@listsEdit')->name('lists.edit');
    Route::post('lists/store/{issue_id}', 'ItemController@listsStore')->name('lists.store');
    Route::patch('lists/update/{issue_id}/{id}', 'ItemController@listsUpdate')->name('lists.update');
    Route::put('save-contract_tasks', 'ContractsController@contractTaskStore');
    Route::put('edit-contract_tasks/{contract_task_id}', 'ContractsController@contractTaskUpdate');
    Route::put('add-images-contract_tasks/{contract_task_id}', 'ContractsController@contractTaskAddImages');

    Route::get('contract_task/edit/{id}', 'ContractsController@contractTaskEdit');
    Route::get('contract_task/upload_images/{id}', 'ContractsController@contractTaskUploadImages');
    Route::get('contract_task/all/{id}', 'ContractsController@getContractTask');
  
    Route::get('to-words/{value?}', 'ContractsController@toWords');
});

Route::post('changePassword', 'Admin\UsersController@changePassword');
