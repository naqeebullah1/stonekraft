<?php

return [

    /**
     * The DocuSign Integrator's Key
     */

    'integrator_key' => 'c8946a03-05dd-41e4-b698-042193af0221',

    /**
     * The Docusign Account Email
     */
    'email' => 'waseemjustonline@gmail.com',

    /**
     * The Docusign Account Password
     */
    'password' => 'Error@1122',

    /**
     * The version of DocuSign API (Ex: v1, v2)
     */
    'version' => 'v2',

    /**
     * The DocuSign Environment (Ex: demo, test, www)
     */
    'environment' => 'demo',

    /**
     * The DocuSign Account Id
     */
    'account_id' => '003832e8-9ec6-4880-8777-05a32c2877a1',


    /**
     * Envelope Trait Configs 
     */


    /**
     * Envelope ID field 
     */
    'envelope_field' => 'envelopeId',

    /**
    * Recipient IDs to save tabs for upon creating the Envelope (false = Disabled)
    */
    'save_recipient_tabs' => [1],

    /**
    * Envelope Tabs field
    */
    'tabs_field' => 'envelopeTabs',

    /**
    * Envelope Documents field (false = Disabled)
    */
    'documents_field' => 'templateDocuments',
];

